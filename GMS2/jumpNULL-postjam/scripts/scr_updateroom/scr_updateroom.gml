var xflick = argument0;
var yflick = argument1;

visited[xflick/160,yflick/144] = true
	  
if yflick/144 >= 8
{
	__background_set( e__BG.Visible, 1, false ); 
	__background_set( e__BG.Visible, 2, true ); 
	__background_set( e__BG.Visible, 3, false ); 
	__background_set( e__BG.Visible, 4, false ); 
if !audio_is_playing(s_caves) && music {audio_stop_all(); audio_play_sound(s_caves,0,true)}
}
else if yflick/144 < 8
{
if (xflick/160 <= 2 && yflick/144 <= 5) || yflick/144 <= 3
{
__background_set( e__BG.Visible, 3, false ); 
__background_set( e__BG.Visible, 4, false );
__background_set( e__BG.Visible, 5, true ); 
__background_set( e__BG.Visible, 6, true );
if !audio_is_playing(s_mountains) && music {audio_stop_all(); audio_play_sound(s_mountains,0,true)}
}
else
{
__background_set( e__BG.Visible, 2, false ); 
__background_set( e__BG.Visible, 1, true ); 
__background_set( e__BG.Visible, 5, false ); 
__background_set( e__BG.Visible, 6, false );
if !audio_is_playing(s_plains) && music {audio_stop_all(); audio_play_sound(s_plains,0,true)}
if o_dude.upgrade[2] = true
{
	__background_set( e__BG.Visible, 3, true ); 
	__background_set( e__BG.Visible, 4, true );
}
}
}