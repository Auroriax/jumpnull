#macro c_gbblack make_color_rgb(30,63,44)
#macro c_gbwhite make_color_rgb(204,228,152)
#macro c_gbdarkgray make_color_rgb(159,192,77)
#macro c_gblightgray make_color_rgb(83,112,62)
#macro gspeed 1
#macro COOLMATH false
#macro CoolMath:COOLMATH true
