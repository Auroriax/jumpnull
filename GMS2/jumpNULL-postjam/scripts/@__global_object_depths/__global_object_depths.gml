// Initialise the global array that allows the lookup of the depth of a given object
// GM2.0 does not have a depth on objects so on import from 1.x a global array is created
// NOTE: MacroExpansion is used to insert the array initialisation at import time
gml_pragma( "global", "__global_object_depths()");

// insert the generated arrays here
global.__objectDepths[0] = 0; // o_dummy
global.__objectDepths[1] = -3; // o_dialog
global.__objectDepths[2] = 0; // o_interactable
global.__objectDepths[3] = -100; // o_debug
global.__objectDepths[4] = -101; // o_upgradeani
global.__objectDepths[5] = -10; // o_transition
global.__objectDepths[6] = -1; // o_dude
global.__objectDepths[7] = 0; // o_block
global.__objectDepths[8] = 0; // o_slope
global.__objectDepths[9] = 0; // o_slope2
global.__objectDepths[10] = 0; // o_slope3
global.__objectDepths[11] = 0; // o_slope4
global.__objectDepths[12] = 0; // o_lowslope
global.__objectDepths[13] = 0; // o_lowslope2
global.__objectDepths[14] = 0; // o_lowslope3
global.__objectDepths[15] = 0; // o_lowslope4
global.__objectDepths[16] = 0; // o_highslope
global.__objectDepths[17] = 0; // o_highslope2
global.__objectDepths[18] = 0; // o_highslope3
global.__objectDepths[19] = 0; // o_highslope4
global.__objectDepths[20] = 1; // o_house
global.__objectDepths[21] = 1; // o_house2
global.__objectDepths[22] = 1; // o_house3
global.__objectDepths[23] = 0; // o_ladder
global.__objectDepths[24] = 0; // o_checkpoint
global.__objectDepths[25] = 0; // o_sign
global.__objectDepths[26] = 0; // o_ufo
global.__objectDepths[27] = 0; // o_citicen
global.__objectDepths[28] = 0; // o_citicen2
global.__objectDepths[29] = 0; // o_citicen3
global.__objectDepths[30] = 0; // o_cameo
global.__objectDepths[31] = 0; // o_cameo2
global.__objectDepths[32] = 0; // o_final
global.__objectDepths[33] = 0; // o_intro
global.__objectDepths[34] = 0; // o_swapblockon
global.__objectDepths[35] = 0; // o_swapblockoff
global.__objectDepths[36] = 0; // o_switch
global.__objectDepths[37] = 1; // o_deco
global.__objectDepths[38] = -1; // o_shrubbery
global.__objectDepths[39] = -1; // o_secretwall
global.__objectDepths[40] = 0; // o_pickup
global.__objectDepths[41] = 0; // o_currentleft
global.__objectDepths[42] = 0; // o_currentleftstrong
global.__objectDepths[43] = 0; // o_currentup
global.__objectDepths[44] = 0; // o_currentupstrong
global.__objectDepths[45] = 0; // o_currentupmed
global.__objectDepths[46] = 0; // o_currentright
global.__objectDepths[47] = 0; // o_currentrightstrong
global.__objectDepths[48] = 0; // o_currentdown
global.__objectDepths[49] = 0; // o_jumpthroughoff
global.__objectDepths[50] = 0; // o_jumpthroughon
global.__objectDepths[51] = 0; // o_jumpthroughleftoff
global.__objectDepths[52] = 0; // o_jumpthroughlefton
global.__objectDepths[53] = 0; // o_menu
global.__objectDepths[54] = 0; // o_choicemenu
global.__objectDepths[55] = 0; // o_bonusmenu
global.__objectDepths[56] = 0; // o_jukebox
global.__objectDepths[57] = 0; // o_init
global.__objectDepths[58] = -2; // o_banner
global.__objectDepths[59] = 0; // o_bridge
global.__objectDepths[60] = 0; // o_breakable
global.__objectDepths[61] = 0; // o_broken
global.__objectDepths[62] = 0; // o_event
global.__objectDepths[63] = 0; // o_scrollspace
global.__objectDepths[64] = 0; // o_stayending
global.__objectDepths[65] = 0; // o_map
global.__objectDepths[66] = 0; // o_hat
global.__objectDepths[67] = 0; // o_arrowsign
global.__objectDepths[68] = 0; // o_fallinlight
global.__objectDepths[69] = 0; // o_coolMathSplash


global.__objectNames[0] = "o_dummy";
global.__objectNames[1] = "o_dialog";
global.__objectNames[2] = "o_interactable";
global.__objectNames[3] = "o_debug";
global.__objectNames[4] = "o_upgradeani";
global.__objectNames[5] = "o_transition";
global.__objectNames[6] = "o_dude";
global.__objectNames[7] = "o_block";
global.__objectNames[8] = "o_slope";
global.__objectNames[9] = "o_slope2";
global.__objectNames[10] = "o_slope3";
global.__objectNames[11] = "o_slope4";
global.__objectNames[12] = "o_lowslope";
global.__objectNames[13] = "o_lowslope2";
global.__objectNames[14] = "o_lowslope3";
global.__objectNames[15] = "o_lowslope4";
global.__objectNames[16] = "o_highslope";
global.__objectNames[17] = "o_highslope2";
global.__objectNames[18] = "o_highslope3";
global.__objectNames[19] = "o_highslope4";
global.__objectNames[20] = "o_house";
global.__objectNames[21] = "o_house2";
global.__objectNames[22] = "o_house3";
global.__objectNames[23] = "o_ladder";
global.__objectNames[24] = "o_checkpoint";
global.__objectNames[25] = "o_sign";
global.__objectNames[26] = "o_ufo";
global.__objectNames[27] = "o_citicen";
global.__objectNames[28] = "o_citicen2";
global.__objectNames[29] = "o_citicen3";
global.__objectNames[30] = "o_cameo";
global.__objectNames[31] = "o_cameo2";
global.__objectNames[32] = "o_final";
global.__objectNames[33] = "o_intro";
global.__objectNames[34] = "o_swapblockon";
global.__objectNames[35] = "o_swapblockoff";
global.__objectNames[36] = "o_switch";
global.__objectNames[37] = "o_deco";
global.__objectNames[38] = "o_shrubbery";
global.__objectNames[39] = "o_secretwall";
global.__objectNames[40] = "o_pickup";
global.__objectNames[41] = "o_currentleft";
global.__objectNames[42] = "o_currentleftstrong";
global.__objectNames[43] = "o_currentup";
global.__objectNames[44] = "o_currentupstrong";
global.__objectNames[45] = "o_currentupmed";
global.__objectNames[46] = "o_currentright";
global.__objectNames[47] = "o_currentrightstrong";
global.__objectNames[48] = "o_currentdown";
global.__objectNames[49] = "o_jumpthroughoff";
global.__objectNames[50] = "o_jumpthroughon";
global.__objectNames[51] = "o_jumpthroughleftoff";
global.__objectNames[52] = "o_jumpthroughlefton";
global.__objectNames[53] = "o_menu";
global.__objectNames[54] = "o_choicemenu";
global.__objectNames[55] = "o_bonusmenu";
global.__objectNames[56] = "o_jukebox";
global.__objectNames[57] = "o_init";
global.__objectNames[58] = "o_banner";
global.__objectNames[59] = "o_bridge";
global.__objectNames[60] = "o_breakable";
global.__objectNames[61] = "o_broken";
global.__objectNames[62] = "o_event";
global.__objectNames[63] = "o_scrollspace";
global.__objectNames[64] = "o_stayending";
global.__objectNames[65] = "o_map";
global.__objectNames[66] = "o_hat";
global.__objectNames[67] = "o_arrowsign";
global.__objectNames[68] = "o_fallinlight";
global.__objectNames[69] = "o_coolMathSplash";


// create another array that has the correct entries
var len = array_length_1d(global.__objectDepths);
global.__objectID2Depth = [];
for( var i=0; i<len; ++i ) {
	var objID = asset_get_index( global.__objectNames[i] );
	if (objID >= 0) {
		global.__objectID2Depth[ objID ] = global.__objectDepths[i];
	} // end if
} // end for