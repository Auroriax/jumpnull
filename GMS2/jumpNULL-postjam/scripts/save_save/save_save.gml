/// @description save_save()
show_debug_message("Save SAVE")

ini_open("jumpNULL.save")

ini_write_real("State","SaveX",o_dude.checkx+4)
ini_write_real("State","SaveY",o_dude.checky+9)

if o_dude.upgrade[1] = true {ini_write_real("State","UpgradeCount",1)}
if o_dude.upgrade[2] = true {ini_write_real("State","UpgradeCount",2)}
if o_dude.upgrade[3] = true {ini_write_real("State","UpgradeCount",3)}
if o_dude.upgrade[4] = true {ini_write_real("State","UpgradeCount",4)}

ini_write_real("State","Hat",o_dude.hashat)
ini_write_real("State","SpokeToLena",!instance_exists(o_event))
ini_write_real("State","Switch",o_dude.caveswitch)

for(var i = 0; i != 15; i += 1)
{
 for(var j = 0; j != 15; j += 1)
 {
  ini_write_real("Minimap",string(i)+"_"+string(j),o_dude.visited[i,j])
 }
}

ini_close()
