/// @description save_load()
show_debug_message("Save LOAD")

ini_open("jumpNULL.save")

var loadx = ini_read_real("State","SaveX",-1)
if loadx != -1 { o_dude.x = loadx; __view_set( e__VW.XView, 3, floor(o_dude.x/160)*160 ); o_dude.time = 360}
var loady = ini_read_real("State","SaveY",-1)
if loady != -1 { o_dude.y = loady; __view_set( e__VW.YView, 3, floor(o_dude.y/144)*144 ); o_dude.time = 360}

var upgrade = ini_read_real("State","UpgradeCount",0)
if upgrade >= 1 {o_dude.upgrade[1] = true; scr_progress_state(0); with o_pickup {if upgradenr == 1 {instance_destroy()}}}
if upgrade >= 2 {o_dude.upgrade[2] = true; scr_progress_state(1); with o_pickup {if upgradenr == 2 {instance_destroy()}}}
if upgrade >= 3 {o_dude.upgrade[3] = true; with o_pickup {if upgradenr == 3 {instance_destroy()}}}
if upgrade >= 4 {o_dude.upgrade[4] = true; with o_pickup {if upgradenr == 4 {instance_destroy()}}}

hashat = ini_read_real("State","Hat",0)
if ini_read_real("State","SpokeToLena",false) == true
{scr_progress_state(2); with o_event {instance_destroy()}}
if ini_read_real("State","Switch",0) {caveswitch = true; alarm[3] = 2;}

for(var i = 0; i != 15; i += 1)
{
 for(var j = 0; j != 15; j += 1)
 {
 visited[i,j] = ini_read_real("Minimap",string(i)+"_"+string(j),false)
 }
}

ini_close()

var xflick, yflick;
xflick = floor(x/160)*160
yflick = floor(y/144)*144
scr_updateroom(xflick,yflick);
