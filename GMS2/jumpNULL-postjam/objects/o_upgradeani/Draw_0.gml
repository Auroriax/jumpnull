if image_index != 10
{
draw_sprite_tiled_ext(sprite_index,image_index,__view_get( e__VW.XView, 3 ),__view_get( e__VW.YView, 3 ),1,1,-1,1-alph)
draw_sprite_tiled_ext(sprite_index,image_index+1,__view_get( e__VW.XView, 3 ),__view_get( e__VW.YView, 3 ),1,1,-1,alph)

alph += 1/50

if floor(alph) == 1
{alph = 0; image_index += 1}
}
else
{instance_destroy()}


__view_set( e__VW.XView, 3, (floor(o_dude.x/160)*160) )
__view_set( e__VW.YView, 3, (floor(o_dude.y/144)*144) )

if image_index < 9
{
__view_set( e__VW.XView, 3, __view_get( e__VW.XView, 3 ) + (irandom_range(-3,3)) )
__view_set( e__VW.YView, 3, __view_get( e__VW.YView, 3 ) + (irandom_range(-2,2)) )

if keyboard_check_pressed(vk_enter)
{
 image_index = 9; alph = 0; 
 if os_browser == browser_not_a_browser
 {
  audio_sound_gain(s_plains,1,100)
  audio_sound_gain(s_caves,1,100)
  audio_sound_gain(s_mountains,1,100)
 }
}
}

if image_index == 8 && os_browser == browser_not_a_browser
{
audio_sound_gain(s_plains,1,100)
audio_sound_gain(s_caves,1,100)
audio_sound_gain(s_mountains,1,100)
}

