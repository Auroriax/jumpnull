{
    "id": "755a2791-a0b9-4f6d-891b-eca30499ad7c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_upgradeani",
    "eventList": [
        {
            "id": "372482f9-66eb-4a7d-bac8-95c8e273ad2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "755a2791-a0b9-4f6d-891b-eca30499ad7c"
        },
        {
            "id": "d466b60f-0621-4dd0-a38d-1fd5cbf57a88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "755a2791-a0b9-4f6d-891b-eca30499ad7c"
        },
        {
            "id": "bdeec018-df38-423d-a009-4a8a76172d05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "755a2791-a0b9-4f6d-891b-eca30499ad7c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
    "visible": true
}