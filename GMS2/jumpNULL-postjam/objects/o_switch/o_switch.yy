{
    "id": "9c65f3e3-27b2-40f7-b58a-7e9ff7fbe0f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_switch",
    "eventList": [
        {
            "id": "47b5ae76-6a92-4fb9-85db-cfed0c68f91f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9c65f3e3-27b2-40f7-b58a-7e9ff7fbe0f3"
        },
        {
            "id": "50ca3556-259d-470a-8a8c-11a4192c1b59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "59f2e563-4f00-4f98-aca9-5a0810ddde1a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9c65f3e3-27b2-40f7-b58a-7e9ff7fbe0f3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d2b8078d-2a71-4376-ba24-9782a4c8ad1b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "55b9a308-cddd-4ba2-9c4f-5ac9fc14ce84",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "f5754bc4-c9e3-44ce-94ba-16b84333dfc7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0fce53ff-134c-4ea5-8e5c-769a7b88a24d",
    "visible": true
}