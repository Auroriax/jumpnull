{
    "id": "061ae2ee-fd72-4229-a330-8a7e3fc863fc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_breakable",
    "eventList": [
        {
            "id": "05e49096-4f7b-4106-9ebc-ec75d885ae33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "061ae2ee-fd72-4229-a330-8a7e3fc863fc"
        },
        {
            "id": "a984aa9d-c0c3-497a-add0-372d2e63df2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "061ae2ee-fd72-4229-a330-8a7e3fc863fc"
        },
        {
            "id": "a6bb91e1-0835-4010-9088-9b02567c1aaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "061ae2ee-fd72-4229-a330-8a7e3fc863fc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "954f928a-e110-49fd-bc14-a9eaf8c915f5",
    "visible": true
}