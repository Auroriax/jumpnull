{
    "id": "6c9efffc-52d8-4092-ba0f-efddfb464751",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_currentdown",
    "eventList": [
        {
            "id": "a16a9970-cd19-4413-8b39-aab610f373cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6c9efffc-52d8-4092-ba0f-efddfb464751"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d108915a-173d-4bc3-b711-b4c9541b8e83",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ded2c0ed-8316-474a-9bf5-52598f27dcfd",
    "visible": true
}