{
    "id": "7abe19b1-a36d-44aa-9b06-511daa83af5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_final",
    "eventList": [
        {
            "id": "51e2013e-2461-41f7-be01-87c0ffcbcc18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "59f2e563-4f00-4f98-aca9-5a0810ddde1a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7abe19b1-a36d-44aa-9b06-511daa83af5f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4e0abe81-c6a6-4813-9c99-9b07f567f327",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "3ba70666-1199-4e84-b0ee-d40e4ac58eba",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "3c41e4c1-935e-4d51-825f-ceb5218260af",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d077f629-0698-481f-8de0-8050dcfe55c4",
    "visible": true
}