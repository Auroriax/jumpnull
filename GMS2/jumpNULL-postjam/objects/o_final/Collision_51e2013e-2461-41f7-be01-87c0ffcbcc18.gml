if keyboard_check_released(vk_enter) && !instance_exists(o_dialog) && !instance_exists(o_choicemenu)
{
scr_dialog(
"LANA: Oh, there you are! You're cuter than I imagined!",
"You brought the orbs?",
"Yeah?! Aw, geez! You're a real life saver.",
"This orb goes here... and this one... over here...",
"Ta-Daah!!!",
"Well... seems this thing is operational again.",
"...",
"You know...",
"We would make a pretty good team. Probably.",
"I break stuff, and you go fix it! Heh.",
"Seriously: you'd be amazed at the rate I break stuff.",
"But still... I always wanted a sidekick.",
"So what do you think about it?",
"Wanna go to space with me?",
)

instance_create(__view_get( e__VW.XView, 3 ),__view_get( e__VW.YView, 3 ),o_choicemenu)
}

