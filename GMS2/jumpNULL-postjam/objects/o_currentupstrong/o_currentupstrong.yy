{
    "id": "5825a39a-2146-4ba9-9d16-e7b0e33bcaae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_currentupstrong",
    "eventList": [
        {
            "id": "6fe4a3c6-efc5-4056-8720-71781d0b6516",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5825a39a-2146-4ba9-9d16-e7b0e33bcaae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d108915a-173d-4bc3-b711-b4c9541b8e83",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ded2c0ed-8316-474a-9bf5-52598f27dcfd",
    "visible": true
}