{
    "id": "3f35ee9d-de72-407c-a082-ff1392af1e34",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_ufo",
    "eventList": [
        {
            "id": "95af3501-b2af-4e59-9cfd-f8117fdb3f00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3f35ee9d-de72-407c-a082-ff1392af1e34"
        },
        {
            "id": "24417147-02fa-4a02-bc74-9d4c09c55ebd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "59f2e563-4f00-4f98-aca9-5a0810ddde1a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3f35ee9d-de72-407c-a082-ff1392af1e34"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d2b8078d-2a71-4376-ba24-9782a4c8ad1b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "90e5426a-1206-406e-907a-ed02a896b2ac",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "9461ff8d-ea3e-4248-80c8-1d13e2654506",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f43921fb-ebe3-4df0-9fea-7bc54bfe44fe",
    "visible": true
}