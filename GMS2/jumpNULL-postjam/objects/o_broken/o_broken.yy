{
    "id": "4579868b-a844-4b3c-aed0-6ebedadd3aea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_broken",
    "eventList": [
        {
            "id": "a653e624-5415-4b3a-8637-02c66ad0a229",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4579868b-a844-4b3c-aed0-6ebedadd3aea"
        },
        {
            "id": "5109c246-d494-4efd-9f59-e3304d8fce07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4579868b-a844-4b3c-aed0-6ebedadd3aea"
        },
        {
            "id": "12b8657b-714f-4196-b1fc-df23d93dbe38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4579868b-a844-4b3c-aed0-6ebedadd3aea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4f0fa2d9-5ed0-4554-87bb-8bad0fa51279",
    "visible": true
}