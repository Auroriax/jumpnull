{
    "id": "125fe3dd-69c0-4302-8d31-8ed4f8f8bac6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_jumpthroughon",
    "eventList": [
        {
            "id": "2190a455-5757-4e21-9cb0-8a690e5870e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "125fe3dd-69c0-4302-8d31-8ed4f8f8bac6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "ff5b9a51-8591-4a26-83e4-5dcc912b61e6",
    "visible": true
}