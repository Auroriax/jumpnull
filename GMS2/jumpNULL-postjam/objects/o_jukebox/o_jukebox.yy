{
    "id": "0e9bf2c9-77e8-45b7-a113-ccdb72372b9b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_jukebox",
    "eventList": [
        {
            "id": "320a30ff-7791-484b-b8b6-da251eb1b310",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e9bf2c9-77e8-45b7-a113-ccdb72372b9b"
        },
        {
            "id": "249fa292-cb76-4aa9-991c-8cb175ba8a6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0e9bf2c9-77e8-45b7-a113-ccdb72372b9b"
        },
        {
            "id": "8dcc0bfb-fbf8-48d2-952c-d20ab3de85df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0e9bf2c9-77e8-45b7-a113-ccdb72372b9b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}