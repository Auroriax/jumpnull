{
    "id": "3d94169b-b99a-469b-b698-8801aab7cfcb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_intro",
    "eventList": [
        {
            "id": "14a0e6a7-2bf1-4ca0-a1b5-23e8284c89a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d94169b-b99a-469b-b698-8801aab7cfcb"
        },
        {
            "id": "9d738117-c1b7-425e-bc9b-da5b91314b7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3d94169b-b99a-469b-b698-8801aab7cfcb"
        },
        {
            "id": "b6aaf7d2-e7b0-4da7-83e5-8c51618c9856",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3d94169b-b99a-469b-b698-8801aab7cfcb"
        },
        {
            "id": "319e998c-0ca8-4b2e-a77f-f47a2576d57d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3d94169b-b99a-469b-b698-8801aab7cfcb"
        },
        {
            "id": "f7a9204a-ec5e-4d21-a2e8-091da10aa66c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3d94169b-b99a-469b-b698-8801aab7cfcb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "2066e8e2-9858-487a-a730-a7d1685a4807",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "f8f7f80b-4c5f-43e6-9c50-a9589714064b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 80
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5ea54253-35e7-4435-9a44-899afc69eb92",
    "visible": true
}