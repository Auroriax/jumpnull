{
    "id": "6cdd1fef-a4f1-4453-b6ad-8067963f63f6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_checkpoint",
    "eventList": [
        {
            "id": "cd2856b1-7ff3-411c-8d5a-4611879937dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6cdd1fef-a4f1-4453-b6ad-8067963f63f6"
        },
        {
            "id": "c472ec99-f197-45ad-a95a-0efa94415beb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "59f2e563-4f00-4f98-aca9-5a0810ddde1a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6cdd1fef-a4f1-4453-b6ad-8067963f63f6"
        },
        {
            "id": "9beb8a56-6986-4bca-aade-38be60f069e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "6cdd1fef-a4f1-4453-b6ad-8067963f63f6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d2b8078d-2a71-4376-ba24-9782a4c8ad1b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "1651ee79-d3cb-44df-981d-e79e7b9bfb40",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "bb88890d-8819-40d6-8d26-d9ead5d83bc0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 8
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8301f30b-3913-4ba9-b993-229fda449539",
    "visible": true
}