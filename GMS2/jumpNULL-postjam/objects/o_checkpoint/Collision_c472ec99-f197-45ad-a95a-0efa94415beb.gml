if sprite_index == s_checkinactive
{
sprite_index = s_checkactive; 
image_index = 0
audio_play_sound(s_checkpoint,0,false)

//scr_gaevent("Checkpoint",string(x)+","+string(y))

with o_checkpoint
{
if id != other.id
{sprite_index = s_checkinactive; image_index = 0; image_speed = 1/6*gspeed}
}

o_dude.checkx = x
o_dude.checky = y

save_save()
}

if keyboard_check_released(vk_enter) && !instance_exists(o_dialog) && !instance_exists(o_transition)
{
save_save()
scr_dialog(
"Your game has been saved!"
)
}

