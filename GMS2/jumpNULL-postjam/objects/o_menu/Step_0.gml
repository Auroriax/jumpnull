var menulength = array_length_1d(menu)-1;

if keyboard_check_pressed(vk_down) || keyboard_check_pressed(vk_right)
{ if cursor == menulength {cursor = 0} else {cursor += 1; if menu[cursor] = "" {cursor += 1}}}

if keyboard_check_pressed(vk_up) || keyboard_check_pressed(vk_left)
{ if cursor == 0 {cursor = menulength} else {cursor -= 1; if menu[cursor] = "" {cursor -= 1}}}

if keyboard_check_pressed(vk_enter)
{
switch cursor{
case 0: 
	o_init.alarm[1] = 30; 
	scr_gascreen("New Game");
	instance_destroy(); 
	break;
case 1: 
	if file_exists("jumpNULL.save") 
	{
		o_init.alarm[0] = 30; 
		scr_gascreen("Continue");
		instance_destroy();
	} else {
		scr_dialog("No save data found.")
	} 
	break;
case 3: 
	instance_create(x,y,o_bonusmenu); 
	scr_gascreen("Bonus menu");
	io_clear(); 
	instance_destroy(); 
	break;
case 5: 
	o_init.alarm[2] = 30; 
	instance_destroy(); 
	break;
}
}

