{
    "id": "05e1b272-a0b6-43d4-a51a-0b59e56da7fe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_menu",
    "eventList": [
        {
            "id": "0adeb199-07ac-4340-a3ce-6aa1f9fc4504",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "05e1b272-a0b6-43d4-a51a-0b59e56da7fe"
        },
        {
            "id": "bd4dc66a-0d23-4fdb-b05d-16be1368d905",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "05e1b272-a0b6-43d4-a51a-0b59e56da7fe"
        },
        {
            "id": "c7e2f7f2-20e9-49a8-bdb9-73e1665efe46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "05e1b272-a0b6-43d4-a51a-0b59e56da7fe"
        },
        {
            "id": "dae1b2ff-3d1e-4077-adba-237c70bfec9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "05e1b272-a0b6-43d4-a51a-0b59e56da7fe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}