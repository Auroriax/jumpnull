{
    "id": "4862d25a-560b-4f62-a85f-18a5171fb429",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_highslope2",
    "eventList": [
        
    ],
    "maskSpriteId": "23812d4d-aa85-4c1f-b0fe-dc4db1f05e67",
    "overriddenProperties": null,
    "parentObjectId": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "131d48c4-7b5c-4bf8-93dc-c5dc4f5226c0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "b116c0f1-73e0-420c-a638-0a9ce337d351",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "edeab854-d7c4-44a7-b363-7b9b2ed5a200",
    "visible": true
}