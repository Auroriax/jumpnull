/// @description Grunt work
lookdir = 1
controlable = true

var xflick, yflick;
xflick = floor(x/160)*160
yflick = floor(y/144)*144

__view_set( e__VW.XView, 3, xflick )
__view_set( e__VW.YView, 3, yflick )

/*var lay = layer_get_id("Compatibility_Foreground_0_bg_grid")
var bg = layer_background_get_id(lay)
layer_background_visible(bg,false)*/
//__background_set( e__BG.Visible, 0, false )
__background_set( e__BG.HSpeed, 1, 0.125*gspeed )

visited[15,15] = false

for(i=0;i!=15;i+=1)
for(j=0;j!=15;j+=1)
{
	visited[i,j] = false
}

mapcolor = c_gblightgray
alarm[0] = 60/gspeed

checkx = xstart
checky = ystart

interactable = false
interact_imgindex = 0
alarm[1] = 20/gspeed

scr_resetcurrents()

__background_set( e__BG.VSpeed, 2, 0.5*gspeed )
__background_set( e__BG.HSpeed, 5, 0.125*gspeed )
__background_set( e__BG.HSpeed, 6, 0.25*gspeed )

falltime = 0

time = 0

music = true

hashat = 0

//Optimalisation
xprevview = __view_get( e__VW.XView, 3 )
yprevview = __view_get( e__VW.YView, 3 )

//The game normally intends to be run at 60fps.
//But that's too much for Android.
//gspeed will adjust some values for better play.

room_speed /= gspeed;

///Upgrades

upgrade[1] = false; //The float power
upgrade[2] = false; //Strong shoes
upgrade[3] = false; //Ground pound
upgrade[4] = false; //Spiky shoes
upgrade[5] = false; //Better float power

airscroll = 0;

scr_touchcontrols();

//scr_gascreen("WorldStart");

caveswitch = false;

alarm[2] = 1

floating = false;
	 
gpu_set_alphatestenable(false);