{
    "id": "59f2e563-4f00-4f98-aca9-5a0810ddde1a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_dude",
    "eventList": [
        {
            "id": "dc4b7c0d-3beb-4a76-a361-d13a5116443d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        },
        {
            "id": "88713cc7-aab5-4827-8836-d1d36c4c290b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        },
        {
            "id": "c737b90c-7798-4bf1-aa28-b46fb5ef5d74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        },
        {
            "id": "73aa81b4-cbfb-48a7-9292-3bb94882c5a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        },
        {
            "id": "68775b12-3131-45b2-adef-ed673ba418ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        },
        {
            "id": "8fc29fe9-e19a-4488-80e4-432c601b9b66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        },
        {
            "id": "6d7e366d-5098-403d-bdaf-22371d833af8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        },
        {
            "id": "4f05eecc-bf6f-4f5f-8b82-58e6abe52cac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d108915a-173d-4bc3-b711-b4c9541b8e83",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        },
        {
            "id": "5ee445e2-99b0-4138-b470-f8ba824116c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        },
        {
            "id": "0675dec7-0943-4fdd-8ea2-4f737f1dd2e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        },
        {
            "id": "52a87613-e3ed-4e08-adf0-6bbfecc57b59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 123,
            "eventtype": 9,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        },
        {
            "id": "9334eb74-c60b-4666-a4eb-930f6681c682",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 120,
            "eventtype": 9,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        },
        {
            "id": "3c3877bc-e081-4a42-9ce1-7cb0b540bfc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        },
        {
            "id": "55bd1b6b-eff6-4882-97a1-07d8e663a3ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 9,
            "m_owner": "59f2e563-4f00-4f98-aca9-5a0810ddde1a"
        }
    ],
    "maskSpriteId": "b8bef6d1-70d6-45d3-b7b4-5e9234156d63",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "e50d0b43-2c7b-49ea-b1ed-71e24991ab64",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 5
        },
        {
            "id": "b2443afa-05a2-4e32-ba6f-d7a3c9a377a7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 6,
            "y": 6
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b8bef6d1-70d6-45d3-b7b4-5e9234156d63",
    "visible": true
}