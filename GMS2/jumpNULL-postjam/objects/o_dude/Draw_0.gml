//Ground pound
if falltime >= 15 && upgrade[3] && keyboard_check(vk_down)
{draw_sprite(s_groundpound,interact_imgindex,x,y)}
else if upgrade[1] && keyboard_check(vk_up) && floating && y != yprevious
{draw_sprite(s_float,interact_imgindex,x,y)}

draw_sprite_ext(sprite_index,image_index,round(x),round(y),lookdir,1,0,-1,1)

//Minimap
var i,j, size;
 size = 1
for(i=0;i!=7;i+=1) //Horizontal
 for(j=1;j!=12;j+=1) //Vertical
 {
  if visited[i,j] == true
  draw_sprite_ext(s_mapdot,0,__view_get( e__VW.XView, 3 )+i+1,__view_get( e__VW.YView, 3 )+j+1,1,1,0,c_gblightgray,1)
}

draw_sprite_ext(s_mapdot,0,__view_get( e__VW.XView, 3 )+round(__view_get( e__VW.XView, 3 )/160)+1,__view_get( e__VW.YView, 3 )+round(__view_get( e__VW.YView, 3 )/144)+1,1,1,0,mapcolor,1);

//Upgrades
for (i = 1; i != 6; i+=1)
{
if upgrade[i] = true
draw_rectangle_colour(__view_get( e__VW.XView, 3 )-1+i*3,__view_get( e__VW.YView, 3 )+16,__view_get( e__VW.XView, 3 )+i*3,__view_get( e__VW.YView, 3 )+17,c_gblightgray,c_gblightgray,c_gblightgray,c_gblightgray,0)
}

//Cool hat
if hashat != 0
{
if lookdir = 1
draw_sprite_ext(s_coolhat,hashat,round(x)+1,round(y)-18,lookdir,1,0,-1,1)
else
draw_sprite_ext(s_coolhat,hashat,round(x)-1,round(y)-18,lookdir,1,0,-1,1)
}

//Interactables
if interactable
draw_sprite(s_interactable,interact_imgindex,x,y-8)

// DEBUG FPS count
if instance_exists(o_debug)
{
var txt = string(fps)+" "+string(instance_count) + " " + string(x) + " " + string(y);
draw_text_colour(__view_get( e__VW.XView, 3 )+24,__view_get( e__VW.YView, 3 )+24,string_hash_to_newline(txt),c_gbblack,c_gbblack,c_gbblack,c_gbblack,1)
draw_text_colour(__view_get( e__VW.XView, 3 )+22,__view_get( e__VW.YView, 3 )+22,string_hash_to_newline(txt),c_gbblack,c_gbblack,c_gbblack,c_gbblack,1)
draw_text_colour(__view_get( e__VW.XView, 3 )+24,__view_get( e__VW.YView, 3 )+22,string_hash_to_newline(txt),c_gbblack,c_gbblack,c_gbblack,c_gbblack,1)
draw_text_colour(__view_get( e__VW.XView, 3 )+22,__view_get( e__VW.YView, 3 )+24,string_hash_to_newline(txt),c_gbblack,c_gbblack,c_gbblack,c_gbblack,1)
draw_text_colour(__view_get( e__VW.XView, 3 )+23,__view_get( e__VW.YView, 3 )+23,string_hash_to_newline(txt),c_gbdarkgray,c_gbdarkgray,c_gbdarkgray,c_gbdarkgray,1)
}

