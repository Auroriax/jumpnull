if controlable
{//Horizontal
floating = false;
 var dir;
 dir = keyboard_check(vk_right) - keyboard_check(vk_left)

 if dir == 0
 {image_speed = 0; if !collision_rectangle(x-3,y-4,x+3,y+5,o_ladder,1,1) {image_index = 0}}
 else
 {image_speed = 0.25*gspeed; lookdir = dir}

 var j;
 if upgrade[4] = true {j = 4}
 else if upgrade[2] = true {j = 2}
 else {j = 1}
 
 for (i=0;i<=j;i+=1)
 {
  if place_free(x+dir,y-i)
  {
	  x += dir*gspeed; y -= i; 
	  if place_meeting(x,y,o_block) 
		{x -= dir}; 
	  break;
	}
 }

 if !collision_rectangle(x-3,y-4,x+3,y+5,o_ladder,1,1) //Ladders
 {
  if !keyboard_check(vk_up) && !keyboard_check(vk_down) && place_free(x,y+1) 
  {
    y += 1*gspeed; if !place_free(x,y) {y -= 1} sprite_index = s_dudefall;
  }
  else if keyboard_check(vk_up) //Floating
  {
      if !upgrade[1] && place_free(x,y+1) {y += 1*gspeed; if !place_free(x,y) {y -= 1} sprite_index = s_dudefall;}
      else if upgrade[1] && !upgrade[5] && place_free(x,y+0.5) {y += 0.5*gspeed; if !place_free(x,y) {y -= 0.5*gspeed} else {floating = true} sprite_index = s_dudefall; }
      else if upgrade[1] && upgrade[5] && place_free(x,y+0.25) {y += 0.25*gspeed; if !place_free(x,y) {y -= 0.25*gspeed} else {floating = true} sprite_index = s_dudefall; }
  }
  else if keyboard_check(vk_down) && place_free(x,y+1) //Ground pound
  {
      if !upgrade[3] {y += 1*gspeed; if !place_free(x,y) {y -= 1}}
      else if upgrade[3] {y += 1*gspeed; if place_free(x,y+1*gspeed) {y += 1*gspeed}}
  }
  
  if !place_free(x,y+1) //Ground pound counter
  {
    sprite_index = s_dude; falltime  = 0; y = floor(y); 
  }
  else
  {if keyboard_check(vk_down) {falltime += 1*gspeed} else {falltime = 0}}
 }
 else
 {
 image_speed = 0; sprite_index = s_dude;
  if keyboard_check(vk_up) && !place_meeting(x,y-2,o_block) && place_meeting(x,y-2,o_ladder)
  {y -= 1*gspeed; image_speed = 0.25*gspeed}
  else if keyboard_check(vk_down) && !place_meeting(x,y+2,o_block) && place_meeting(x,y+2,o_ladder)
  {y += 1*gspeed; image_speed = 0.25*gspeed}
  
  falltime = 0
 } 
 
 //Camera control
 var xflick, yflick, changed;
 xflick = floor(x/160)*160
 yflick = floor(y/144)*144
 changed = false

 if xflick > __view_get( e__VW.XView, 3 ) 
 {
	 __view_set( e__VW.XView, 3, __view_get( e__VW.XView, 3 ) + (20*gspeed) ); changed = true
 }
 else if xflick < __view_get( e__VW.XView, 3 ) 
 {
	 __view_set( e__VW.XView, 3, __view_get( e__VW.XView, 3 ) - (20*gspeed) ); changed = true
	 }

 if yflick > __view_get( e__VW.YView, 3 ) {__view_set( e__VW.YView, 3, __view_get( e__VW.YView, 3 ) + (18*gspeed) ); changed = true}
 else if yflick < __view_get( e__VW.YView, 3 ) {__view_set( e__VW.YView, 3, __view_get( e__VW.YView, 3 ) - (18*gspeed) ); changed = true}
 
 //Audio/Background changing based on the location
 if changed
 {
	 scr_updateroom(xflick,yflick);
 }
 
 xprevview  = __view_get( e__VW.XView, 3 ); yprevview  = __view_get( e__VW.YView, 3 )
}

if place_meeting(x,y,o_interactable)
{interactable = true}
else
{interactable = false}