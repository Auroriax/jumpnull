{
    "id": "18bb52b2-843a-42ac-b9ba-3a7de3a86d80",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_jumpthroughlefton",
    "eventList": [
        {
            "id": "0846570e-1746-4a98-993f-aaa363aa868a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "18bb52b2-843a-42ac-b9ba-3a7de3a86d80"
        },
        {
            "id": "f9c59c6e-1582-4696-8225-8e41b904fdf5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "18bb52b2-843a-42ac-b9ba-3a7de3a86d80"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "7a3c3db0-b8ac-4248-ba52-fc385e1a6ff0",
    "visible": true
}