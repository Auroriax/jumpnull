{
    "id": "6e2f1cf7-a1c4-4f8e-99c3-246bdfd15544",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_secretwall",
    "eventList": [
        {
            "id": "2371dadf-b750-4d09-9bc9-75f510c5e58b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6e2f1cf7-a1c4-4f8e-99c3-246bdfd15544"
        },
        {
            "id": "617fb760-84dd-47e1-b48b-55e76767be4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6e2f1cf7-a1c4-4f8e-99c3-246bdfd15544"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "43e5d492-b13c-4934-8c07-ef23f952ea33",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "66518e7d-75ef-4f9a-999b-6b17c372564e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a43e1f13-76b7-444f-81d9-204d90a28700",
    "visible": true
}