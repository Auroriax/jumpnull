{
    "id": "7a5c269e-c27b-432e-8291-86a2e8b7cf55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_highslope4",
    "eventList": [
        
    ],
    "maskSpriteId": "857d01e5-c10c-40c0-8b15-a00297d62cac",
    "overriddenProperties": null,
    "parentObjectId": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "5e5e3ea2-3330-404d-86c8-d8f66f0489bc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "d6841cf6-a9ba-4910-81cb-cf0465ba01c9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "8291771e-186a-4aad-b62a-8c944a1b33ea",
    "visible": true
}