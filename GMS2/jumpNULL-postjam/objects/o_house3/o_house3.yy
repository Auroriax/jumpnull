{
    "id": "8e2be9fa-2712-46a3-a17d-0323fceeef2d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_house3",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "018b1bc8-f67c-4f88-9829-d70e928e1756",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "ea5d17b0-97ec-4231-9928-d96604da869b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 12
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "ba527913-56aa-40a2-9f93-cbe02993b179",
    "visible": true
}