{
    "id": "ea330173-9be0-4483-9dc2-43ee0cb24398",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_jumpthroughoff",
    "eventList": [
        {
            "id": "b86570a8-b926-4222-be4a-38699568739f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ea330173-9be0-4483-9dc2-43ee0cb24398"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff5b9a51-8591-4a26-83e4-5dcc912b61e6",
    "visible": true
}