{
    "id": "558ea21d-599e-4c99-9c5c-aec9eff9051d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_jumpthroughleftoff",
    "eventList": [
        {
            "id": "70e33032-cc07-44d8-8122-59d7f31541f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "558ea21d-599e-4c99-9c5c-aec9eff9051d"
        },
        {
            "id": "a199bf6a-7f84-478e-a193-c31b3486ab9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "558ea21d-599e-4c99-9c5c-aec9eff9051d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7a3c3db0-b8ac-4248-ba52-fc385e1a6ff0",
    "visible": true
}