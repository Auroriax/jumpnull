{
    "id": "e0b31cc2-e306-4632-860f-b5ef6820dec7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_highslope",
    "eventList": [
        
    ],
    "maskSpriteId": "f68c9586-77b0-4c81-8a47-1302fae75103",
    "overriddenProperties": null,
    "parentObjectId": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "a8e74a13-a154-470b-8667-7746b8b1b4ab",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "f84971f3-776c-436f-84ad-8c3573ff1e0d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "73e26c9a-e0fd-438b-9c8a-6b61d57469e1",
    "visible": true
}