{
    "id": "9226f82f-f8f5-4e01-9463-450822c38161",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_cameo2",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4e0abe81-c6a6-4813-9c99-9b07f567f327",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "1d062aa3-ec3a-4b6c-afac-ebfa93ae3daf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "f9d80966-a807-406c-985b-d545e1f8059e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a869b61-9f2e-4c50-a69f-ddf19657b2e4",
    "visible": true
}