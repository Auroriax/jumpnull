scr_dialog(
"VOICE IN YOUR MIND: Hey there.",
"Don't be alarmed: I'm talking to you via your mind.",
"I'm Lana. Your species would call me an 'alien'. Heh.",
"LANA: But enough about me.",
"I see you have been collecting those energy orbs.",
"You know, the ones that grant you those powers.",
"I need them to reboot my spaceship. To get outta here.",
"Would you kindly deliver them to me?",
"I'm on top of the mountain. Please bring them there!",
"That's where my ship crashed, as well.",
"All right! See you soon!",
"Lana's voice fades..."
)

scr_progress_state(2)

instance_destroy()

