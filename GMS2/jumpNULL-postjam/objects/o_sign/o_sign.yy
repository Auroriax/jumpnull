{
    "id": "dbfb2c04-b249-4fae-933b-09cc21714d67",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_sign",
    "eventList": [
        {
            "id": "f77dc718-48cd-4bf4-a580-b8e709bd46c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dbfb2c04-b249-4fae-933b-09cc21714d67"
        },
        {
            "id": "ddc868de-aae9-4136-a585-2796b387e620",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "59f2e563-4f00-4f98-aca9-5a0810ddde1a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dbfb2c04-b249-4fae-933b-09cc21714d67"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d2b8078d-2a71-4376-ba24-9782a4c8ad1b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "ff054af3-d0cb-4d10-9d54-2b18e78b2a2e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "13fb8df0-c8bb-442b-812f-c59d598626b3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5c792f4c-ad76-4120-8196-691a758eae68",
    "visible": true
}