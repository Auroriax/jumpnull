/// @description Key mapping
keyboard_set_map(vk_space,vk_enter)
keyboard_set_map(ord("E"),vk_enter)
keyboard_set_map(ord("A"),vk_left)
keyboard_set_map(ord("W"),vk_up)
keyboard_set_map(ord("D"),vk_right)
keyboard_set_map(ord("S"),vk_down)

//Azerty
keyboard_set_map(ord("Q"),vk_left)
keyboard_set_map(ord("Z"),vk_up)

/// Fonts

draw_set_font(f_GameBoy)

display_set_gui_maximise()

//application_surface_enable(false)
//surface_resize(application_surface,display_get_width(),display_get_width())

/*application_surface_draw_enable(false);
min_width = room_width
min_height= room_height
scr_resize();
*/


if os_type == os_android
{
	global.gainited = true
	//GoogleAnalytics_Init( "UA-36092603-8", false)
	//scr_gascreen("Game Start")
}
else
{global.gainited = false}

/* */
/*  */
