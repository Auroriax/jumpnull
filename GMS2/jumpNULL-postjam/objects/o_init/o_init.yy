{
    "id": "79ad56c3-c30a-4922-bb40-e0140a0d908e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_init",
    "eventList": [
        {
            "id": "b3d3a674-5aec-487a-a252-578314562f7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "79ad56c3-c30a-4922-bb40-e0140a0d908e"
        },
        {
            "id": "3e412291-918a-4992-86e1-3645712dddf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "79ad56c3-c30a-4922-bb40-e0140a0d908e"
        },
        {
            "id": "4a27d1e5-94fe-4f3c-8f44-6fcaedb55b0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "79ad56c3-c30a-4922-bb40-e0140a0d908e"
        },
        {
            "id": "4f9e0928-ff8e-44f1-86fd-6d6e414f2e8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "79ad56c3-c30a-4922-bb40-e0140a0d908e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}