{
    "id": "92f726e7-5711-484b-a86d-714458e8b19b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_swapblockoff",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "00f842d6-ca39-4c3e-bc03-9ca422e52c00",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "f980c72b-830f-4e41-a69c-3e47f5a8f1cc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "56ea0c30-4f03-4943-9935-bd626f70042d",
    "visible": true
}