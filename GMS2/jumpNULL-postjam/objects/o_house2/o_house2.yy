{
    "id": "5149ec59-878d-432a-bf65-f63191ebcc38",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_house2",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "952cfc63-de73-4467-a9bf-c616129112ec",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "4141a0aa-43c2-475c-a024-a3a3d85adbb2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 12
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "64df0d4d-d811-4e72-99d8-ee8c01629f57",
    "visible": true
}