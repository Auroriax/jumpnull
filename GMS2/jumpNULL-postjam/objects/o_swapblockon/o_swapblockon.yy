{
    "id": "16e59408-3244-4101-bb9e-1e95074c43e7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_swapblockon",
    "eventList": [
        {
            "id": "1a8f27f1-4f82-4aee-9501-effe72858051",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "16e59408-3244-4101-bb9e-1e95074c43e7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "82ee8a6a-5135-4cb3-b4b6-9a4b9e1311f7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "1bb25fbf-301f-46de-bf10-31d9160e6ad7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "35a31cf3-fe31-4a6b-b9c0-cc4c76a7fc51",
    "visible": true
}