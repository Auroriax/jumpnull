{
    "id": "f5f2041d-aea0-4284-b1b1-f276e309f8a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_cameo",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4e0abe81-c6a6-4813-9c99-9b07f567f327",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "d05066dc-a34c-44a3-9bf8-0ca3ff315f7d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "5358bde2-01e8-4dda-ae0f-a5b27ece6fc9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "492f7350-d041-47e9-90da-21063e38fcb9",
    "visible": true
}