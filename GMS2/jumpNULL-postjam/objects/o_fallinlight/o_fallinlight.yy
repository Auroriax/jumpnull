{
    "id": "1ec94351-58a4-47de-abfc-ae04cf7d3710",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_fallinlight",
    "eventList": [
        {
            "id": "3bc9726b-639d-4db6-8775-9b977e64dfba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1ec94351-58a4-47de-abfc-ae04cf7d3710"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "dbfb2c04-b249-4fae-933b-09cc21714d67",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3366eb4a-8795-48ef-b24f-4fb8921c2c44",
    "visible": true
}