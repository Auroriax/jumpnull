{
    "id": "06c284da-7d7f-4e1b-936b-f4151e98c777",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_longladder",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f0bcf8e1-ad29-4ebf-a241-4fd7241664df",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "a73b378b-e265-4005-b58a-1d456f7d052e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "41fff79e-3a3b-49bb-9f7c-eb5832d02884",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bbd79735-068f-4a82-ba73-10ca370d2a97",
    "visible": true
}