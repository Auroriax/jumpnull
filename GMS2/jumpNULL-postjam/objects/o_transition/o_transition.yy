{
    "id": "b95fb698-4093-4ac0-b94e-da0861b6e301",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_transition",
    "eventList": [
        {
            "id": "00212c03-8614-41f8-ac94-a9202d4b25b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b95fb698-4093-4ac0-b94e-da0861b6e301"
        },
        {
            "id": "29d6176f-70a3-4967-9397-d0d92e8126ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b95fb698-4093-4ac0-b94e-da0861b6e301"
        },
        {
            "id": "76286f9b-b658-4aca-b391-13511465dbb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "b95fb698-4093-4ac0-b94e-da0861b6e301"
        },
        {
            "id": "9c0bcd3c-0cce-4984-92e3-66692532a28a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b95fb698-4093-4ac0-b94e-da0861b6e301"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
    "visible": true
}