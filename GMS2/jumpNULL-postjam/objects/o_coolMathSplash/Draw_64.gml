var w = display_get_gui_width();
var h = display_get_gui_height();

draw_sprite(s_auroriaxlogo,0,w/2,h/2-50);
draw_set_color(c_white);
draw_set_font(f_GameBoyBig);
draw_set_halign(1);

var txt = "Click anywhere to play!"
if alarm[0] == -1 {txt = "Sorry, this game#is sitelocked!"}
if alarm[1] != -1 {txt = ""}
draw_text(w/2,h*0.75,string_hash_to_newline(txt))

