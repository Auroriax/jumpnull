{
    "id": "dcb2b51f-21ec-47e4-88f0-27a4121b098b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_coolMathSplash",
    "eventList": [
        {
            "id": "0dcfa711-cb72-4506-9f43-1f64d25f3160",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dcb2b51f-21ec-47e4-88f0-27a4121b098b"
        },
        {
            "id": "f23bc0dc-e6a1-47b5-9548-b54eade13c2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "dcb2b51f-21ec-47e4-88f0-27a4121b098b"
        },
        {
            "id": "bdb171c9-858d-4a0a-abb0-744e60e7a4c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "dcb2b51f-21ec-47e4-88f0-27a4121b098b"
        },
        {
            "id": "eef8fb85-1c0c-4b50-8443-7a89fae20745",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "dcb2b51f-21ec-47e4-88f0-27a4121b098b"
        },
        {
            "id": "6d366883-aded-49ba-917f-f20b03fce8e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dcb2b51f-21ec-47e4-88f0-27a4121b098b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "29ad09f0-814b-4cdf-9a82-bc18a3a79a06",
    "visible": true
}