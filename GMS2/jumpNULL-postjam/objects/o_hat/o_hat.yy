{
    "id": "218cc471-876e-42dd-87e6-12c7b7e5787b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_hat",
    "eventList": [
        {
            "id": "80f0d156-a974-44e7-bfe9-9306addb8ebd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "218cc471-876e-42dd-87e6-12c7b7e5787b"
        },
        {
            "id": "b4c95e6c-8fd0-4c96-80c0-4a192296d6b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "218cc471-876e-42dd-87e6-12c7b7e5787b"
        },
        {
            "id": "48d533e3-1a8c-48d0-9d0d-56c0058a6bf2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "59f2e563-4f00-4f98-aca9-5a0810ddde1a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "218cc471-876e-42dd-87e6-12c7b7e5787b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d2b8078d-2a71-4376-ba24-9782a4c8ad1b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b85ee409-efd4-4216-8609-52421dcb521f",
    "visible": true
}