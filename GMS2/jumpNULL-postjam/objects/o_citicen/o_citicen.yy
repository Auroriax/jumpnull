{
    "id": "4e0abe81-c6a6-4813-9c99-9b07f567f327",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_citicen",
    "eventList": [
        {
            "id": "d2704025-a6ee-4eb4-8c36-cf024eae8187",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4e0abe81-c6a6-4813-9c99-9b07f567f327"
        },
        {
            "id": "0b697afd-c933-4252-a78a-634d8c494158",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4e0abe81-c6a6-4813-9c99-9b07f567f327"
        },
        {
            "id": "e5b0e8cc-11b0-45ad-b045-3d9f8cc306d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4e0abe81-c6a6-4813-9c99-9b07f567f327"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "dbfb2c04-b249-4fae-933b-09cc21714d67",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "e9feeb7b-02e1-4e16-9f3d-df6297474fa5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "7e3e6b23-ebb5-4aba-b7d4-864c553cded2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4995296-b91e-4178-9c64-3271045d530a",
    "visible": true
}