{
    "id": "c93ca11a-ee31-4a9a-a404-8b428c72245d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_deco",
    "eventList": [
        {
            "id": "b7baacdc-1f90-4be3-b06a-463bfff9f8e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c93ca11a-ee31-4a9a-a404-8b428c72245d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f4fe8ac7-5ef9-4a49-883c-3679cc32ebb9",
    "visible": true
}