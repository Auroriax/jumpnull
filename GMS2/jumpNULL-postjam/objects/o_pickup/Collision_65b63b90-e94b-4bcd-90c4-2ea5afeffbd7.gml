o_dude.upgrade[upgradenr] = true

var ani;
ani = instance_create(x,y,o_upgradeani)

if upgradenr == 1
{
ani.d[0] = "You feel a strange sensation as you touch the orb..."
ani.d[1] = "You can now hold -UP- to fall slower!"

scr_progress_state(0)
}

if upgradenr == 2
{
ani.d[0] = "You got the steel boots!"
ani.d[1] = "You can now climb those diagonal hills!"

scr_progress_state(1)
}

if upgradenr == 3
{
ani.d[0] = "You feel a power welling inside you..."
ani.d[1] = "You can now hold -DOWN- to ground pound!"
}

if upgradenr == 4
{
ani.d[0] = "Power moves through your arms towards your heart..."
ani.d[1] = "Oh, wait."
ani.d[2] = "You don't have hands, kid."
ani.d[3] = "...never mind, then."
ani.d[4] = "You can now climb super steep ledges!"
}

instance_destroy()

