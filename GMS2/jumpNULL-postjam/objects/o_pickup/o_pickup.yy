{
    "id": "15d18d37-fa03-4312-8dc8-0c6b1eb02158",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_pickup",
    "eventList": [
        {
            "id": "d44141fa-16ee-452a-9305-b1112db315f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "15d18d37-fa03-4312-8dc8-0c6b1eb02158"
        },
        {
            "id": "65b63b90-e94b-4bcd-90c4-2ea5afeffbd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "59f2e563-4f00-4f98-aca9-5a0810ddde1a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "15d18d37-fa03-4312-8dc8-0c6b1eb02158"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
    "visible": true
}