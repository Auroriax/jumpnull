{
    "id": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_block",
    "eventList": [
        {
            "id": "0f38aa03-113d-4c1b-a563-7df665b26419",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "22c6d676-a5db-4247-afec-3faf8039dbe5"
        },
        {
            "id": "cee9e824-728c-4226-93c1-81346d319b95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "22c6d676-a5db-4247-afec-3faf8039dbe5"
        },
        {
            "id": "a75da7c9-8f54-4b1d-8bb3-64eb1d1225e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "22c6d676-a5db-4247-afec-3faf8039dbe5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "bd4c2def-e0a8-4c96-b7a0-51d95e075dfa",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "5bada615-d128-4538-b720-fc265d925a54",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "4f0fa2d9-5ed0-4554-87bb-8bad0fa51279",
    "visible": true
}