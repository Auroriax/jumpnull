/// @description QQQ

if !object_get_parent(object_index)
{
	gpu_set_blendenable(false);
	draw_self();
	gpu_set_blendenable(true);
}
else
{
	draw_self();
}

//For profiling