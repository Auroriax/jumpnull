{
    "id": "9085619f-3fab-4eb5-b3b1-eb530eadd10b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_slope4",
    "eventList": [
        
    ],
    "maskSpriteId": "334a592b-f366-4e3c-b4ac-22b403c431b2",
    "overriddenProperties": null,
    "parentObjectId": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "958da0b8-9b73-453f-a23b-7d44fa5cf76a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "0688a492-bb4f-45ec-bce0-531142475a5f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "732b3d32-1d1c-40e4-b9ff-43479833248c",
    "visible": true
}