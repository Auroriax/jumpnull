{
    "id": "8a4ce7db-3a47-4bbf-a9b3-905487dc12e7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bridge",
    "eventList": [
        {
            "id": "cf9efa72-567d-48f3-b299-594257bcf0af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8a4ce7db-3a47-4bbf-a9b3-905487dc12e7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "3946dd51-0cb9-4065-8afe-e983642b86fb",
    "visible": true
}