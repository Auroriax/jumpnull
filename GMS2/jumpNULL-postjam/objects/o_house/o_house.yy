{
    "id": "9eea89da-95a2-4ecc-a006-bb0aefd33828",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_house",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "154b726a-6a1b-48b9-bbca-be8d89e0f71a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "b5a343be-f9ac-4c44-a4dc-a2c6c48f9eda",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 12
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "4f29b622-c8d4-4e43-898d-973a11e96f7d",
    "visible": true
}