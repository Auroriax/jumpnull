{
    "id": "4d5f2ccb-8abf-48c0-b8d0-519f4ce03bac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_highslope3",
    "eventList": [
        
    ],
    "maskSpriteId": "04b66eca-9bca-4888-a1c2-7208a9230b67",
    "overriddenProperties": null,
    "parentObjectId": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "a39ae189-c8a7-43a9-9344-687a9c302bcb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "98248c5b-cd22-4e29-a4e6-97ff52c2e69b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "74222e42-0fbc-4843-9ee1-1dcc59bbab77",
    "visible": true
}