{
    "id": "3c2bb3f5-0f5e-4d15-842d-aec58ddfe4df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_slope",
    "eventList": [
        
    ],
    "maskSpriteId": "3fd793f1-e893-4724-a8b3-93755f9c372d",
    "overriddenProperties": null,
    "parentObjectId": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "02e9113a-d2f6-4a4b-bd16-bdf5168247ff",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "0d9d29b7-da0f-4c79-bdcc-6bc2925d1f92",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "9c7cf525-e78c-43e9-886a-c7e0cd745c62",
    "visible": true
}