{
    "id": "d108915a-173d-4bc3-b711-b4c9541b8e83",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_currentleft",
    "eventList": [
        {
            "id": "856db92e-672d-497d-872b-75542df1ce1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d108915a-173d-4bc3-b711-b4c9541b8e83"
        },
        {
            "id": "833c79f8-35cf-455c-8f38-034e52b444c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d108915a-173d-4bc3-b711-b4c9541b8e83"
        },
        {
            "id": "0588a497-cf7d-4053-87f1-6ab09ec2cd89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d108915a-173d-4bc3-b711-b4c9541b8e83"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ded2c0ed-8316-474a-9bf5-52598f27dcfd",
    "visible": true
}