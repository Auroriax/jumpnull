{
    "id": "8906e333-708b-405b-8efb-a435927a2e45",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_map",
    "eventList": [
        {
            "id": "d1998f34-70f0-4111-a524-fe6b987636f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8906e333-708b-405b-8efb-a435927a2e45"
        },
        {
            "id": "8a6d47a8-d65f-46c1-afad-5de0a3f4d741",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "8906e333-708b-405b-8efb-a435927a2e45"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aab94747-f814-4e72-af6c-7ee20fcc101f",
    "visible": true
}