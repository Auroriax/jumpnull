{
    "id": "eb0e6a00-3f79-4080-bb4a-e1e2246cff2a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_banner",
    "eventList": [
        {
            "id": "5657c976-9eda-4cd1-b36b-41deb66542c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "eb0e6a00-3f79-4080-bb4a-e1e2246cff2a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "86105852-8ac0-494e-9ac0-e73d5189d756",
    "visible": true
}