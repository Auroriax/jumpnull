{
    "id": "60817c07-b6f4-43bb-a0ad-0f13b9da7dd7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_slope2",
    "eventList": [
        
    ],
    "maskSpriteId": "42dce8db-ad34-488e-9d64-aa3a29a0f8e1",
    "overriddenProperties": null,
    "parentObjectId": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "e9c434d7-cde7-472e-8f23-0eeffb447c51",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "afdf5b4c-46a8-4f15-9d25-42c0082383bd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "340c732b-c817-48df-bd69-edfb48b42d32",
    "visible": true
}