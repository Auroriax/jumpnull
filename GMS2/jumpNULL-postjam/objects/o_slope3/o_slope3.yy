{
    "id": "5522eee7-16f2-49ae-a4a7-0ed751b47a59",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_slope3",
    "eventList": [
        
    ],
    "maskSpriteId": "008ae45a-f8fb-4a4f-ac4f-241261ce37d0",
    "overriddenProperties": null,
    "parentObjectId": "22c6d676-a5db-4247-afec-3faf8039dbe5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "c640b092-91e0-4b78-939a-8ce79b42fa6a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "c06fb1e4-44ed-4c16-a44e-2148109bc651",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "e6a857ac-c6ec-4318-93f0-8f2e363703cc",
    "visible": true
}