{
    "id": "c105531d-1c71-4350-a0d7-81908aef6d87",
    "modelName": "GMExtension",
    "mvc": "1.2",
    "name": "coolmath",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 32,
    "date": "2019-57-17 10:11:39",
    "description": "",
    "exportToGame": true,
    "extensionName": "",
    "files": [
        {
            "id": "9f4b6a26-f7fd-4e3a-ab25-d237e86b6e32",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 9223372036854775807,
            "filename": "coolmath_functions.js",
            "final": "",
            "functions": [
                {
                    "id": "5206e127-da00-4640-a1e5-aa09c5433df8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "coolmathCallStart",
                    "help": "Whenever a \"Play\" button is pressed.",
                    "hidden": false,
                    "kind": 11,
                    "name": "coolmathCallStart",
                    "returnType": 2
                },
                {
                    "id": "6c1fce65-3122-49d4-aaa5-727fee3788de",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "coolmathCallLevelStart",
                    "help": "Whenever a new level loads, passing the level number",
                    "hidden": false,
                    "kind": 11,
                    "name": "coolmathCallLevelStart",
                    "returnType": 2
                },
                {
                    "id": "efe3c5fe-91a3-405f-bb93-72c80312e75a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "coolmathCallLevelRestart",
                    "help": "Whenever the level is manually (not automatically) restarted, passing level number",
                    "hidden": false,
                    "kind": 11,
                    "name": "coolmathCallLevelRestart",
                    "returnType": 2
                },
                {
                    "id": "ab5c11aa-87c1-4427-bca2-74b9d60f2b72",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "coolMathShouldUnlockAll",
                    "help": "Check regularily whether a subscriber wants to unlock all levels",
                    "hidden": false,
                    "kind": 11,
                    "name": "coolMathShouldUnlockAll",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 5,
            "order": [
                
            ],
            "origname": "extensions\\coolmath_functions.js",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "options": null,
    "optionsFile": "options.json",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "supportedTargets": 9223372036854775807,
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "1.0.0"
}