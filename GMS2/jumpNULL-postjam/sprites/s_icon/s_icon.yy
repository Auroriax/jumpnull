{
    "id": "5065d7d4-c022-4b11-b7af-40aa951923ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 139,
    "bbox_left": 28,
    "bbox_right": 111,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3b3884e-42e2-4cbe-9a56-3b12487d3eea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5065d7d4-c022-4b11-b7af-40aa951923ef",
            "compositeImage": {
                "id": "80a3bd45-7689-4bbc-bff2-d35d1f91a115",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3b3884e-42e2-4cbe-9a56-3b12487d3eea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f0d65af-edd3-4884-9999-3257b5b5899c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3b3884e-42e2-4cbe-9a56-3b12487d3eea",
                    "LayerId": "04781c35-e9db-4682-baf4-80ebca0af236"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "04781c35-e9db-4682-baf4-80ebca0af236",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5065d7d4-c022-4b11-b7af-40aa951923ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 4,
    "yorig": 5
}