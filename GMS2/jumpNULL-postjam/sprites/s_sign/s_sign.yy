{
    "id": "5c792f4c-ad76-4120-8196-691a758eae68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_sign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f77ac95-1ff1-4b5d-9edc-1b419facdc88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c792f4c-ad76-4120-8196-691a758eae68",
            "compositeImage": {
                "id": "07b02605-ae14-411c-a8cc-4052807e2233",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f77ac95-1ff1-4b5d-9edc-1b419facdc88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e13091f3-a3dc-4140-933f-a057b46ef4e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f77ac95-1ff1-4b5d-9edc-1b419facdc88",
                    "LayerId": "dabb7844-8f4f-48c3-a33a-3430951ee7ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "dabb7844-8f4f-48c3-a33a-3430951ee7ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c792f4c-ad76-4120-8196-691a758eae68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}