{
    "id": "b7879554-f8a6-484b-8688-aa634f842e3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_checkactive",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "390a4cb0-e994-42e3-8d12-3b2859570f4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7879554-f8a6-484b-8688-aa634f842e3e",
            "compositeImage": {
                "id": "3e2e924f-1f36-4ae5-b6ed-bd7833168229",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "390a4cb0-e994-42e3-8d12-3b2859570f4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df4a3e4c-f2ac-46e2-b4f5-7e1b6f143139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "390a4cb0-e994-42e3-8d12-3b2859570f4d",
                    "LayerId": "74d8e744-7618-40c9-bf01-f75ca9a76f1f"
                }
            ]
        },
        {
            "id": "7e8e38ef-8709-41f8-8983-b6223861118f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7879554-f8a6-484b-8688-aa634f842e3e",
            "compositeImage": {
                "id": "ea50f025-d49e-44ad-ad0c-19b2a1254ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e8e38ef-8709-41f8-8983-b6223861118f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c3ef61b-e0f5-42bc-aba7-3e337206b90c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e8e38ef-8709-41f8-8983-b6223861118f",
                    "LayerId": "74d8e744-7618-40c9-bf01-f75ca9a76f1f"
                }
            ]
        },
        {
            "id": "7ccb5022-74fb-481a-9b03-613912691ca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7879554-f8a6-484b-8688-aa634f842e3e",
            "compositeImage": {
                "id": "8a20fe4f-dafc-4dbf-b7ff-e11ed40b6706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ccb5022-74fb-481a-9b03-613912691ca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59d059c3-3c04-4704-a61c-0e6c1dd4c63e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ccb5022-74fb-481a-9b03-613912691ca1",
                    "LayerId": "74d8e744-7618-40c9-bf01-f75ca9a76f1f"
                }
            ]
        },
        {
            "id": "294a9bfe-bc5a-4af5-aa0c-113500aa53ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7879554-f8a6-484b-8688-aa634f842e3e",
            "compositeImage": {
                "id": "8697d744-65b4-4fbd-885a-b229137562cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "294a9bfe-bc5a-4af5-aa0c-113500aa53ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f01c525e-67a3-450c-b9d5-87410af0282e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "294a9bfe-bc5a-4af5-aa0c-113500aa53ed",
                    "LayerId": "74d8e744-7618-40c9-bf01-f75ca9a76f1f"
                }
            ]
        },
        {
            "id": "53afb4be-d561-4c01-af70-92214fd24218",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7879554-f8a6-484b-8688-aa634f842e3e",
            "compositeImage": {
                "id": "661bbe89-4cab-4729-b147-5c1f4519cd47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53afb4be-d561-4c01-af70-92214fd24218",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d980cc6-bfe6-4b4a-8942-9c181c637b1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53afb4be-d561-4c01-af70-92214fd24218",
                    "LayerId": "74d8e744-7618-40c9-bf01-f75ca9a76f1f"
                }
            ]
        },
        {
            "id": "de1bfe00-d3e9-4907-b329-a47c56f68148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7879554-f8a6-484b-8688-aa634f842e3e",
            "compositeImage": {
                "id": "8266c822-d1f9-435a-bef7-6970c238ecf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de1bfe00-d3e9-4907-b329-a47c56f68148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "381c84e5-f884-4562-933c-b5f22732b9e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de1bfe00-d3e9-4907-b329-a47c56f68148",
                    "LayerId": "74d8e744-7618-40c9-bf01-f75ca9a76f1f"
                }
            ]
        },
        {
            "id": "5195b2b0-3c72-427b-8b85-f405dfa3e00b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7879554-f8a6-484b-8688-aa634f842e3e",
            "compositeImage": {
                "id": "86187a60-58b8-4f72-a5c6-d36c0cd4a31c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5195b2b0-3c72-427b-8b85-f405dfa3e00b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd558589-8adf-47c1-a6ab-bfffd4cedbc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5195b2b0-3c72-427b-8b85-f405dfa3e00b",
                    "LayerId": "74d8e744-7618-40c9-bf01-f75ca9a76f1f"
                }
            ]
        },
        {
            "id": "de1baa20-ab67-4e43-98a7-967cf9aae12d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7879554-f8a6-484b-8688-aa634f842e3e",
            "compositeImage": {
                "id": "f36fb124-4454-459e-affc-78079889c800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de1baa20-ab67-4e43-98a7-967cf9aae12d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bd8257d-161b-4ede-9c05-927794ee2feb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de1baa20-ab67-4e43-98a7-967cf9aae12d",
                    "LayerId": "74d8e744-7618-40c9-bf01-f75ca9a76f1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "74d8e744-7618-40c9-bf01-f75ca9a76f1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7879554-f8a6-484b-8688-aa634f842e3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}