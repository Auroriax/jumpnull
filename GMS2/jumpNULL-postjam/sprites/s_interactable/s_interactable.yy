{
    "id": "84c4bb48-54e9-4c4a-950b-65e2a155be78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_interactable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d1f0044-3775-48a1-9351-da52a076dcc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84c4bb48-54e9-4c4a-950b-65e2a155be78",
            "compositeImage": {
                "id": "3028a10d-2163-4de7-b947-1a3c2340a683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d1f0044-3775-48a1-9351-da52a076dcc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d531b7db-a381-48d2-8846-9ab7ee4b6acb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d1f0044-3775-48a1-9351-da52a076dcc1",
                    "LayerId": "0d8e2642-fea6-440e-b09c-d853c9c82fa3"
                }
            ]
        },
        {
            "id": "56a9af69-7271-4f7d-8cb8-e5c09f919e95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84c4bb48-54e9-4c4a-950b-65e2a155be78",
            "compositeImage": {
                "id": "d218b4ce-4cfa-46e3-bb17-0d80c436b8f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56a9af69-7271-4f7d-8cb8-e5c09f919e95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68539db1-2bd3-418d-a20c-221d669d2373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56a9af69-7271-4f7d-8cb8-e5c09f919e95",
                    "LayerId": "0d8e2642-fea6-440e-b09c-d853c9c82fa3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "0d8e2642-fea6-440e-b09c-d853c9c82fa3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84c4bb48-54e9-4c4a-950b-65e2a155be78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 2,
    "yorig": 8
}