{
    "id": "c4995296-b91e-4178-9c64-3271045d530a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_citicen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01d2b359-e8d1-4f61-917d-e570bf54b5ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4995296-b91e-4178-9c64-3271045d530a",
            "compositeImage": {
                "id": "f69593af-32f2-480a-b497-c60f17e11f2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01d2b359-e8d1-4f61-917d-e570bf54b5ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c511f305-8a87-4d0b-9a20-1ba9960747be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01d2b359-e8d1-4f61-917d-e570bf54b5ad",
                    "LayerId": "605e9d68-00e1-45e2-8895-c6df20f58b04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "605e9d68-00e1-45e2-8895-c6df20f58b04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4995296-b91e-4178-9c64-3271045d530a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 0
}