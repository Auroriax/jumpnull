{
    "id": "e72955de-81c8-4203-9f6b-151c157c94d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_float",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 11,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43696fc9-296c-4db9-b383-9b010fa562b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e72955de-81c8-4203-9f6b-151c157c94d4",
            "compositeImage": {
                "id": "7a7dd447-b341-4a95-bd1c-835af1e5adf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43696fc9-296c-4db9-b383-9b010fa562b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d85a3744-d1ae-499e-9e09-67bfd9fabf15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43696fc9-296c-4db9-b383-9b010fa562b5",
                    "LayerId": "6a2e3094-9119-4de0-ab54-dec096893f5f"
                }
            ]
        },
        {
            "id": "4147ca67-915b-44d8-b04e-ba0f3344d3f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e72955de-81c8-4203-9f6b-151c157c94d4",
            "compositeImage": {
                "id": "6ca1a489-e679-4b73-9773-7f32f0e2c831",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4147ca67-915b-44d8-b04e-ba0f3344d3f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df075179-213a-4645-8a42-e386522bd67c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4147ca67-915b-44d8-b04e-ba0f3344d3f3",
                    "LayerId": "6a2e3094-9119-4de0-ab54-dec096893f5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6a2e3094-9119-4de0-ab54-dec096893f5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e72955de-81c8-4203-9f6b-151c157c94d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 7
}