{
    "id": "ba527913-56aa-40a2-9f93-cbe02993b179",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_house3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a1d0c84-38d5-48a9-b62a-4429bfe4e4a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba527913-56aa-40a2-9f93-cbe02993b179",
            "compositeImage": {
                "id": "cb0812b2-b422-443d-bbda-7cb51f08bec6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a1d0c84-38d5-48a9-b62a-4429bfe4e4a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34b2301c-b37d-4ddc-87e1-58c0de562b28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a1d0c84-38d5-48a9-b62a-4429bfe4e4a8",
                    "LayerId": "41c36c6c-b067-4471-a707-5e7197ab18f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "41c36c6c-b067-4471-a707-5e7197ab18f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba527913-56aa-40a2-9f93-cbe02993b179",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}