{
    "id": "a43e1f13-76b7-444f-81d9-204d90a28700",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_secretwall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99d128ad-85c2-450e-ae89-6b1272f26f6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a43e1f13-76b7-444f-81d9-204d90a28700",
            "compositeImage": {
                "id": "80fc2079-07ff-4743-85a8-21112c4c310e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99d128ad-85c2-450e-ae89-6b1272f26f6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d873ea6f-189f-4641-a91a-6d786139c210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99d128ad-85c2-450e-ae89-6b1272f26f6d",
                    "LayerId": "64010327-f3dc-4346-a193-bace2a85107d"
                }
            ]
        },
        {
            "id": "11bb67a2-a573-49ca-95e0-f7351e78f5c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a43e1f13-76b7-444f-81d9-204d90a28700",
            "compositeImage": {
                "id": "6b63db5c-f6df-4f18-806c-af2055094749",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11bb67a2-a573-49ca-95e0-f7351e78f5c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd0a6670-edac-419b-b505-3482e63b8d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11bb67a2-a573-49ca-95e0-f7351e78f5c4",
                    "LayerId": "64010327-f3dc-4346-a193-bace2a85107d"
                }
            ]
        },
        {
            "id": "dda83897-d7e8-4c73-97cd-13e4af4d0bcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a43e1f13-76b7-444f-81d9-204d90a28700",
            "compositeImage": {
                "id": "102934b0-cb64-4147-9465-b712b93365aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dda83897-d7e8-4c73-97cd-13e4af4d0bcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d79949b2-c854-4d1c-ba92-9dd156686590",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dda83897-d7e8-4c73-97cd-13e4af4d0bcc",
                    "LayerId": "64010327-f3dc-4346-a193-bace2a85107d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "64010327-f3dc-4346-a193-bace2a85107d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a43e1f13-76b7-444f-81d9-204d90a28700",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}