{
    "id": "3fd793f1-e893-4724-a8b3-93755f9c372d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_slopemask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6dd0760a-3a80-4e45-8d84-4d6a3501e01f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fd793f1-e893-4724-a8b3-93755f9c372d",
            "compositeImage": {
                "id": "8338f174-794b-48ef-9850-11eaabc769da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dd0760a-3a80-4e45-8d84-4d6a3501e01f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beaaf488-e3c0-4e7c-8c95-853efe564854",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dd0760a-3a80-4e45-8d84-4d6a3501e01f",
                    "LayerId": "5065ae6a-7282-4ff2-9c4d-d58c72df2514"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "5065ae6a-7282-4ff2-9c4d-d58c72df2514",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fd793f1-e893-4724-a8b3-93755f9c372d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}