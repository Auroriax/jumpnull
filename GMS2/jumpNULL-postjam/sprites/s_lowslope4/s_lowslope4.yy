{
    "id": "a144e1a8-a6f0-4497-b325-6afc054cfb89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_lowslope4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d8fe60e-b976-4a8a-a32f-e8a1e6d79548",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a144e1a8-a6f0-4497-b325-6afc054cfb89",
            "compositeImage": {
                "id": "5a2dae65-4351-47ec-bc23-c9663243ac10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d8fe60e-b976-4a8a-a32f-e8a1e6d79548",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c68f6821-03d2-41c0-816c-d4ccf879969a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d8fe60e-b976-4a8a-a32f-e8a1e6d79548",
                    "LayerId": "2f254c10-1df5-4d6b-88a3-db73c3b2ee9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "2f254c10-1df5-4d6b-88a3-db73c3b2ee9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a144e1a8-a6f0-4497-b325-6afc054cfb89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}