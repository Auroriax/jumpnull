{
    "id": "aab94747-f814-4e72-af6c-7ee20fcc101f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_map",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11d9bd7d-89c3-4989-ba5f-a408d4836225",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aab94747-f814-4e72-af6c-7ee20fcc101f",
            "compositeImage": {
                "id": "9a5ba3c7-c5b4-44c2-ab32-8c23b921778f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11d9bd7d-89c3-4989-ba5f-a408d4836225",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16c83dde-6a99-4766-bfb3-cb7378ddafac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11d9bd7d-89c3-4989-ba5f-a408d4836225",
                    "LayerId": "da99617c-2b5e-4bb5-b506-42968c54f27c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 89,
    "layers": [
        {
            "id": "da99617c-2b5e-4bb5-b506-42968c54f27c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aab94747-f814-4e72-af6c-7ee20fcc101f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}