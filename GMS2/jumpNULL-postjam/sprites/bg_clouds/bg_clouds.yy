{
    "id": "935ef585-cc89-461e-bf1d-17caeaa40a06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_clouds",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 143,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "186620c3-6145-4b90-a868-de9a5e2b9747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935ef585-cc89-461e-bf1d-17caeaa40a06",
            "compositeImage": {
                "id": "d2e357a9-f110-40a7-be55-2cbed67a9a25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "186620c3-6145-4b90-a868-de9a5e2b9747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01cf9d1d-e1dc-487e-97eb-7f51b56c3b86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "186620c3-6145-4b90-a868-de9a5e2b9747",
                    "LayerId": "23c683d6-58cb-42c2-9fd7-195eef70b223"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "23c683d6-58cb-42c2-9fd7-195eef70b223",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "935ef585-cc89-461e-bf1d-17caeaa40a06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}