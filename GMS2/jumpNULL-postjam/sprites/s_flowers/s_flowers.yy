{
    "id": "865f91d9-2520-473e-b94d-5902cd10306d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_flowers",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d808f47-3103-4e9c-803e-7970d55835b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "865f91d9-2520-473e-b94d-5902cd10306d",
            "compositeImage": {
                "id": "10be1407-01ff-4e89-a05e-1658dce35567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d808f47-3103-4e9c-803e-7970d55835b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91afb612-4398-4aa5-a7cb-0392b6ab95da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d808f47-3103-4e9c-803e-7970d55835b6",
                    "LayerId": "c31cbd0d-7f8c-4738-8dcf-1b49ed9a926c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "c31cbd0d-7f8c-4738-8dcf-1b49ed9a926c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "865f91d9-2520-473e-b94d-5902cd10306d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 85,
    "xorig": 0,
    "yorig": 0
}