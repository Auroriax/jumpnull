{
    "id": "972608d2-038e-4e36-b6a1-d370759963fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_spacebg",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 143,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "baac918c-fc61-489b-ad35-104fcf0aa7f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972608d2-038e-4e36-b6a1-d370759963fc",
            "compositeImage": {
                "id": "67fb57bb-1c90-470d-a344-4136f2072644",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baac918c-fc61-489b-ad35-104fcf0aa7f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d8d2557-c8c1-4d89-9a73-e940c80f0925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baac918c-fc61-489b-ad35-104fcf0aa7f1",
                    "LayerId": "b6458fe3-10fc-4cdb-b844-214ddd8d74e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "b6458fe3-10fc-4cdb-b844-214ddd8d74e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "972608d2-038e-4e36-b6a1-d370759963fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}