{
    "id": "2eed9e6f-feec-495e-be7e-3225b1058dbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dudefall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22236d38-ec5a-403c-a0e5-b580877e3f6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eed9e6f-feec-495e-be7e-3225b1058dbb",
            "compositeImage": {
                "id": "67149a88-d9ed-4a13-8ea8-c56f22d40741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22236d38-ec5a-403c-a0e5-b580877e3f6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c0ed20a-5076-4eb2-86c9-bc1924c46fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22236d38-ec5a-403c-a0e5-b580877e3f6d",
                    "LayerId": "d4d14b82-a53c-428a-bb1c-8baad8c71a81"
                }
            ]
        },
        {
            "id": "c0f5c631-cf56-496a-a94d-1a8a63233502",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eed9e6f-feec-495e-be7e-3225b1058dbb",
            "compositeImage": {
                "id": "8948883f-b26f-4e94-9f90-91ea3c0258cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0f5c631-cf56-496a-a94d-1a8a63233502",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f95aba85-e965-4fe5-9851-96b82fc03764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0f5c631-cf56-496a-a94d-1a8a63233502",
                    "LayerId": "d4d14b82-a53c-428a-bb1c-8baad8c71a81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "d4d14b82-a53c-428a-bb1c-8baad8c71a81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2eed9e6f-feec-495e-be7e-3225b1058dbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 5
}