{
    "id": "b58a168c-31d4-49ef-8072-4792b58b8af8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_waterfall",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32583c08-9e6e-4647-af8e-24a0fc88b8b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b58a168c-31d4-49ef-8072-4792b58b8af8",
            "compositeImage": {
                "id": "d6b9a8ee-a7b1-49a3-a4e1-632656eb79b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32583c08-9e6e-4647-af8e-24a0fc88b8b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11584d05-5cf0-47c4-bd25-d8b55dd6b013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32583c08-9e6e-4647-af8e-24a0fc88b8b1",
                    "LayerId": "c4f63b28-2432-431b-8c64-813cf2689b68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "c4f63b28-2432-431b-8c64-813cf2689b68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b58a168c-31d4-49ef-8072-4792b58b8af8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 0,
    "yorig": 0
}