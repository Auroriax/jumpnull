{
    "id": "b85ee409-efd4-4216-8609-52421dcb521f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_coolhat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ba27405-c2aa-42b4-9623-a79245385d96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b85ee409-efd4-4216-8609-52421dcb521f",
            "compositeImage": {
                "id": "672edf19-74cf-4500-a655-39f0d3973b45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba27405-c2aa-42b4-9623-a79245385d96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbf025ce-9ee1-492c-b6a2-003c4ed7c167",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba27405-c2aa-42b4-9623-a79245385d96",
                    "LayerId": "03f59244-14b0-49ce-ae4e-e3eddbda6ccd"
                }
            ]
        },
        {
            "id": "08680a9c-6896-48f7-bc41-62be57eea4b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b85ee409-efd4-4216-8609-52421dcb521f",
            "compositeImage": {
                "id": "8e746fc5-cb03-4f14-a8aa-54b3f755ae5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08680a9c-6896-48f7-bc41-62be57eea4b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e55d064-091b-4665-8d32-3dad5ad5140a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08680a9c-6896-48f7-bc41-62be57eea4b9",
                    "LayerId": "03f59244-14b0-49ce-ae4e-e3eddbda6ccd"
                }
            ]
        },
        {
            "id": "b2ac4f06-3d77-4c46-b103-e5996f5374ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b85ee409-efd4-4216-8609-52421dcb521f",
            "compositeImage": {
                "id": "6e4d56ea-97eb-4db4-ad0a-11f3269a3c6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2ac4f06-3d77-4c46-b103-e5996f5374ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85857d42-6ac2-4250-b891-452b1aba5d37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2ac4f06-3d77-4c46-b103-e5996f5374ee",
                    "LayerId": "03f59244-14b0-49ce-ae4e-e3eddbda6ccd"
                }
            ]
        },
        {
            "id": "da974a1a-6ead-4055-b400-7b508cb2fd70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b85ee409-efd4-4216-8609-52421dcb521f",
            "compositeImage": {
                "id": "41bd48f8-90b8-4682-b576-7ff7df5db2b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da974a1a-6ead-4055-b400-7b508cb2fd70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20ae9de-1cdf-457b-8e43-029b5a0099e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da974a1a-6ead-4055-b400-7b508cb2fd70",
                    "LayerId": "03f59244-14b0-49ce-ae4e-e3eddbda6ccd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "03f59244-14b0-49ce-ae4e-e3eddbda6ccd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b85ee409-efd4-4216-8609-52421dcb521f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 4,
    "yorig": -6
}