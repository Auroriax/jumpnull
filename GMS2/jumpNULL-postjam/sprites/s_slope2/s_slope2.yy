{
    "id": "340c732b-c817-48df-bd69-edfb48b42d32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_slope2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc16d1c5-0dbd-4974-a59d-7dd71e68cddc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "340c732b-c817-48df-bd69-edfb48b42d32",
            "compositeImage": {
                "id": "f7073acb-2916-4888-9e18-f93ce33fd093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc16d1c5-0dbd-4974-a59d-7dd71e68cddc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dcefa9c-8685-4146-bf0e-2c12b0697128",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc16d1c5-0dbd-4974-a59d-7dd71e68cddc",
                    "LayerId": "21fff654-3796-4c94-9872-a42ff914515c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "21fff654-3796-4c94-9872-a42ff914515c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "340c732b-c817-48df-bd69-edfb48b42d32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}