{
    "id": "732b3d32-1d1c-40e4-b9ff-43479833248c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_slope4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "edbbd338-dd13-40a0-9233-36b33fcf5acd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "732b3d32-1d1c-40e4-b9ff-43479833248c",
            "compositeImage": {
                "id": "34693034-41b6-41a2-a323-b909b961458a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edbbd338-dd13-40a0-9233-36b33fcf5acd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0247a5de-00d7-4e54-89f5-08b5ccd65698",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edbbd338-dd13-40a0-9233-36b33fcf5acd",
                    "LayerId": "97ab1c6e-41ba-47e3-aefe-daeba814929a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "97ab1c6e-41ba-47e3-aefe-daeba814929a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "732b3d32-1d1c-40e4-b9ff-43479833248c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}