{
    "id": "0fce53ff-134c-4ea5-8e5c-769a7b88a24d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_switch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9368c8dd-ff51-47f6-a907-8567479e4380",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fce53ff-134c-4ea5-8e5c-769a7b88a24d",
            "compositeImage": {
                "id": "66f16916-53ec-4668-9221-c2e7c5d97462",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9368c8dd-ff51-47f6-a907-8567479e4380",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3bd7ffe-c9e6-4761-a74a-fe6002991f4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9368c8dd-ff51-47f6-a907-8567479e4380",
                    "LayerId": "1c5aaf74-08ac-4eab-9d53-e7c0caf8c09a"
                }
            ]
        },
        {
            "id": "7de3fd95-be0a-450c-a392-b315228b3beb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fce53ff-134c-4ea5-8e5c-769a7b88a24d",
            "compositeImage": {
                "id": "12454736-1cc6-4b0d-a47e-df5089ea847e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7de3fd95-be0a-450c-a392-b315228b3beb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "535355cf-af7d-447d-9123-e502de02d3d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7de3fd95-be0a-450c-a392-b315228b3beb",
                    "LayerId": "1c5aaf74-08ac-4eab-9d53-e7c0caf8c09a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "1c5aaf74-08ac-4eab-9d53-e7c0caf8c09a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fce53ff-134c-4ea5-8e5c-769a7b88a24d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}