{
    "id": "954f928a-e110-49fd-bc14-a9eaf8c915f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_breakable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d407becd-c611-4f1c-816a-a5b7a42265d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "954f928a-e110-49fd-bc14-a9eaf8c915f5",
            "compositeImage": {
                "id": "3f4bd686-e59b-47a3-a989-8a1ad2b01a82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d407becd-c611-4f1c-816a-a5b7a42265d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5ccbe45-9029-40c6-88a1-30b4dd0d3224",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d407becd-c611-4f1c-816a-a5b7a42265d4",
                    "LayerId": "62664cfe-2244-493c-9bc8-d990ca27833a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "62664cfe-2244-493c-9bc8-d990ca27833a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "954f928a-e110-49fd-bc14-a9eaf8c915f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}