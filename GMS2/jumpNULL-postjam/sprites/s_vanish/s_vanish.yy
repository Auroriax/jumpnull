{
    "id": "b19dc31f-a1c3-4bcb-be9c-fa7efac47996",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_vanish",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd7553ee-d948-4adc-a8c6-788122035b2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b19dc31f-a1c3-4bcb-be9c-fa7efac47996",
            "compositeImage": {
                "id": "3aaca82a-d0c6-4e15-b4b4-e02241067124",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd7553ee-d948-4adc-a8c6-788122035b2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0db1f57d-b496-42ae-87ec-906fd8ef13fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd7553ee-d948-4adc-a8c6-788122035b2a",
                    "LayerId": "eeba0dc2-3809-430e-b3c4-c613635bc07d"
                }
            ]
        },
        {
            "id": "ee66ca49-5a82-4b9f-8a09-21bc15b6b268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b19dc31f-a1c3-4bcb-be9c-fa7efac47996",
            "compositeImage": {
                "id": "60eb5057-028f-408d-a7d5-4d0e1c814edd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee66ca49-5a82-4b9f-8a09-21bc15b6b268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cdc8a1b-8a09-45e8-98f9-1710d7d2a92b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee66ca49-5a82-4b9f-8a09-21bc15b6b268",
                    "LayerId": "eeba0dc2-3809-430e-b3c4-c613635bc07d"
                }
            ]
        },
        {
            "id": "b4e07740-e98a-4ce4-9f6f-570c35eca852",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b19dc31f-a1c3-4bcb-be9c-fa7efac47996",
            "compositeImage": {
                "id": "13f36bb8-7d97-49ac-91f6-f7cdc4c46195",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4e07740-e98a-4ce4-9f6f-570c35eca852",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e80880a3-97eb-47c0-a78c-271511736db3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4e07740-e98a-4ce4-9f6f-570c35eca852",
                    "LayerId": "eeba0dc2-3809-430e-b3c4-c613635bc07d"
                }
            ]
        },
        {
            "id": "7dd8f921-f55f-4eae-b0bd-b3f9252a75ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b19dc31f-a1c3-4bcb-be9c-fa7efac47996",
            "compositeImage": {
                "id": "fd28c789-acb4-45a7-b917-798a35da9b1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dd8f921-f55f-4eae-b0bd-b3f9252a75ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09e810f8-7423-4896-848b-dd9f902a6b05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dd8f921-f55f-4eae-b0bd-b3f9252a75ef",
                    "LayerId": "eeba0dc2-3809-430e-b3c4-c613635bc07d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "eeba0dc2-3809-430e-b3c4-c613635bc07d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b19dc31f-a1c3-4bcb-be9c-fa7efac47996",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 5
}