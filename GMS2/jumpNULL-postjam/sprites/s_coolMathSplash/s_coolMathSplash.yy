{
    "id": "02f2f22f-1d6d-49c0-bcad-613e6c8d36a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_coolMathSplash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4698d30f-dced-4d56-90d3-3f19b6ca36e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02f2f22f-1d6d-49c0-bcad-613e6c8d36a2",
            "compositeImage": {
                "id": "304977de-c887-49ca-9399-a06ab75ce47b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4698d30f-dced-4d56-90d3-3f19b6ca36e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aa03b81-c1e0-441c-8f99-8b2b778e89b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4698d30f-dced-4d56-90d3-3f19b6ca36e4",
                    "LayerId": "21a27861-3c41-42b6-a7f6-3f0958fa6c2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "21a27861-3c41-42b6-a7f6-3f0958fa6c2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02f2f22f-1d6d-49c0-bcad-613e6c8d36a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 300
}