{
    "id": "f43921fb-ebe3-4df0-9fea-7bc54bfe44fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_ufo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85628f2e-803c-4856-95fe-9015d71284a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f43921fb-ebe3-4df0-9fea-7bc54bfe44fe",
            "compositeImage": {
                "id": "23620ffb-c3a6-4a0e-bfd1-679beb5869a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85628f2e-803c-4856-95fe-9015d71284a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a10f5758-81c6-48ea-868b-4a5fef62e7f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85628f2e-803c-4856-95fe-9015d71284a3",
                    "LayerId": "3da34e76-28a3-4275-8598-934d80c569ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3da34e76-28a3-4275-8598-934d80c569ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f43921fb-ebe3-4df0-9fea-7bc54bfe44fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}