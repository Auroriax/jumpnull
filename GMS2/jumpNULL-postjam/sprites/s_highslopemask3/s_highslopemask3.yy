{
    "id": "04b66eca-9bca-4888-a1c2-7208a9230b67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_highslopemask3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b2982b4-d868-41ff-95d6-94883894370d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04b66eca-9bca-4888-a1c2-7208a9230b67",
            "compositeImage": {
                "id": "20f6ee19-bf6a-46b4-958a-509a089c066e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b2982b4-d868-41ff-95d6-94883894370d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f62fbee8-9aca-4dca-9ce6-c6594610523a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b2982b4-d868-41ff-95d6-94883894370d",
                    "LayerId": "8f69ce32-62e7-47cc-b165-fb68f4f2b3c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "8f69ce32-62e7-47cc-b165-fb68f4f2b3c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04b66eca-9bca-4888-a1c2-7208a9230b67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}