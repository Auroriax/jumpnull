{
    "id": "23812d4d-aa85-4c1f-b0fe-dc4db1f05e67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_highslopemask2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "081550b9-9aaa-410e-8bfb-d3f16d62e55e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23812d4d-aa85-4c1f-b0fe-dc4db1f05e67",
            "compositeImage": {
                "id": "a03f151b-a7b4-473c-9b04-1fb92a6bcf40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "081550b9-9aaa-410e-8bfb-d3f16d62e55e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55f7f1a9-7c0e-476d-8ab4-c7c64c2a789f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "081550b9-9aaa-410e-8bfb-d3f16d62e55e",
                    "LayerId": "7deb3a70-0269-4e22-bc0d-a07fad7a5e35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "7deb3a70-0269-4e22-bc0d-a07fad7a5e35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23812d4d-aa85-4c1f-b0fe-dc4db1f05e67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}