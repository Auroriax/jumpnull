{
    "id": "8dfd326c-7137-40da-ab9f-9e0823eab3bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_groundpound",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9623d7a3-1752-42fe-b895-d9dab926f893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dfd326c-7137-40da-ab9f-9e0823eab3bb",
            "compositeImage": {
                "id": "cf0dc57d-1b98-459a-a265-ba24c67587bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9623d7a3-1752-42fe-b895-d9dab926f893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84cc0b41-5372-488a-b13a-ca3370134e2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9623d7a3-1752-42fe-b895-d9dab926f893",
                    "LayerId": "a2d1b9a6-d514-4367-bfde-e7317a42522c"
                }
            ]
        },
        {
            "id": "b7a55a48-9ee4-4541-a9ab-30d852e9d396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dfd326c-7137-40da-ab9f-9e0823eab3bb",
            "compositeImage": {
                "id": "1cd03f3a-005a-483f-9f96-91aa6de64e51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7a55a48-9ee4-4541-a9ab-30d852e9d396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d3694a2-cf5d-4935-a02d-d25f1171653e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7a55a48-9ee4-4541-a9ab-30d852e9d396",
                    "LayerId": "a2d1b9a6-d514-4367-bfde-e7317a42522c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a2d1b9a6-d514-4367-bfde-e7317a42522c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8dfd326c-7137-40da-ab9f-9e0823eab3bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 7
}