{
    "id": "29ad09f0-814b-4cdf-9a82-bc18a3a79a06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_auroriaxlogo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 294,
    "bbox_left": 0,
    "bbox_right": 294,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a490c518-2612-40a3-b6ee-7d01d669f57d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ad09f0-814b-4cdf-9a82-bc18a3a79a06",
            "compositeImage": {
                "id": "1675f90b-2735-4e47-9ccf-e2dea9bc28af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a490c518-2612-40a3-b6ee-7d01d669f57d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baf09b7b-6bb4-4821-8bff-df5c472be355",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a490c518-2612-40a3-b6ee-7d01d669f57d",
                    "LayerId": "27b1a9fa-3060-4730-a88b-95ab9ca02cdd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 295,
    "layers": [
        {
            "id": "27b1a9fa-3060-4730-a88b-95ab9ca02cdd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29ad09f0-814b-4cdf-9a82-bc18a3a79a06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 295,
    "xorig": 147,
    "yorig": 147
}