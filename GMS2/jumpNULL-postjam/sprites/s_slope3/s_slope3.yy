{
    "id": "e6a857ac-c6ec-4318-93f0-8f2e363703cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_slope3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a7cf3bd-158a-4cde-87b0-1a76ba0a568d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6a857ac-c6ec-4318-93f0-8f2e363703cc",
            "compositeImage": {
                "id": "f68abc03-de1f-4b26-b379-7c4923f93b83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a7cf3bd-158a-4cde-87b0-1a76ba0a568d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d81fd65d-983e-4c4d-89d9-5172eb049c2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a7cf3bd-158a-4cde-87b0-1a76ba0a568d",
                    "LayerId": "38ea464f-ddf3-4cc1-96dd-cd02d91733a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "38ea464f-ddf3-4cc1-96dd-cd02d91733a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6a857ac-c6ec-4318-93f0-8f2e363703cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}