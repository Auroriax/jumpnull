{
    "id": "0dbe67c4-8f93-4b1b-baab-cce4990be13a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_arrowsign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "069988f7-7f52-45ac-a616-57883b7207a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dbe67c4-8f93-4b1b-baab-cce4990be13a",
            "compositeImage": {
                "id": "a44262a4-31e5-411f-b256-d89da2d50061",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "069988f7-7f52-45ac-a616-57883b7207a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "133e26ad-3076-4683-aaee-c28f20910cc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "069988f7-7f52-45ac-a616-57883b7207a1",
                    "LayerId": "152026fe-cccb-4b87-964e-391b80bf33d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "152026fe-cccb-4b87-964e-391b80bf33d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0dbe67c4-8f93-4b1b-baab-cce4990be13a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}