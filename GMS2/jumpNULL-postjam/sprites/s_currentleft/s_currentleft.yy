{
    "id": "ded2c0ed-8316-474a-9bf5-52598f27dcfd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_currentleft",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4788d4fa-388e-4757-9bd5-0928da7cc132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ded2c0ed-8316-474a-9bf5-52598f27dcfd",
            "compositeImage": {
                "id": "8b2144ff-ee81-4424-8aa4-7c986c72008b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4788d4fa-388e-4757-9bd5-0928da7cc132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4e61df8-9b41-4c64-b8a2-0731c836a830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4788d4fa-388e-4757-9bd5-0928da7cc132",
                    "LayerId": "dcb8222f-4871-4ee4-9f8c-1a1a865fea47"
                }
            ]
        },
        {
            "id": "e05fdb72-0b77-4d54-900d-f9d0b860776f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ded2c0ed-8316-474a-9bf5-52598f27dcfd",
            "compositeImage": {
                "id": "6aac3af5-6ed7-4dde-9e09-bf498109f065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e05fdb72-0b77-4d54-900d-f9d0b860776f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e96b91b-4182-4d4e-b629-ca0364cb5ad3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e05fdb72-0b77-4d54-900d-f9d0b860776f",
                    "LayerId": "dcb8222f-4871-4ee4-9f8c-1a1a865fea47"
                }
            ]
        },
        {
            "id": "b75ed681-b2fc-4924-a4f9-7f26e6208d49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ded2c0ed-8316-474a-9bf5-52598f27dcfd",
            "compositeImage": {
                "id": "74544fae-3566-4fc9-9931-1bff6a1f8023",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b75ed681-b2fc-4924-a4f9-7f26e6208d49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc3604f7-08c5-4dfa-979b-1f2cf5fb4e44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b75ed681-b2fc-4924-a4f9-7f26e6208d49",
                    "LayerId": "dcb8222f-4871-4ee4-9f8c-1a1a865fea47"
                }
            ]
        },
        {
            "id": "8f27f68a-d344-4164-9c42-2fdbc2c53dd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ded2c0ed-8316-474a-9bf5-52598f27dcfd",
            "compositeImage": {
                "id": "524496fc-af5d-436c-951d-53eb37cce33f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f27f68a-d344-4164-9c42-2fdbc2c53dd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d08b342-a9c5-4446-a06a-8354e0d7524f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f27f68a-d344-4164-9c42-2fdbc2c53dd0",
                    "LayerId": "dcb8222f-4871-4ee4-9f8c-1a1a865fea47"
                }
            ]
        },
        {
            "id": "ee0fcd75-8ed9-43cd-a11e-fd5da52c4876",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ded2c0ed-8316-474a-9bf5-52598f27dcfd",
            "compositeImage": {
                "id": "71a2406f-5773-41f7-a68a-484b69ac519a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee0fcd75-8ed9-43cd-a11e-fd5da52c4876",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1feda96d-35d0-4f15-9217-51d58c514974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee0fcd75-8ed9-43cd-a11e-fd5da52c4876",
                    "LayerId": "dcb8222f-4871-4ee4-9f8c-1a1a865fea47"
                }
            ]
        },
        {
            "id": "ccaf0aeb-9b37-4cc8-a363-1f22bb03968d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ded2c0ed-8316-474a-9bf5-52598f27dcfd",
            "compositeImage": {
                "id": "89c17d43-c430-40a7-b56a-7acdc9d9d1f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccaf0aeb-9b37-4cc8-a363-1f22bb03968d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e59ffb14-ae95-4ad9-9821-6a9cd34782ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccaf0aeb-9b37-4cc8-a363-1f22bb03968d",
                    "LayerId": "dcb8222f-4871-4ee4-9f8c-1a1a865fea47"
                }
            ]
        },
        {
            "id": "b69e95a1-3722-447b-8dd1-457a5c230696",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ded2c0ed-8316-474a-9bf5-52598f27dcfd",
            "compositeImage": {
                "id": "619c21c7-71a2-4fde-9dae-002ffe90412b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b69e95a1-3722-447b-8dd1-457a5c230696",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "923aa571-e756-48e7-b559-291266426adb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b69e95a1-3722-447b-8dd1-457a5c230696",
                    "LayerId": "dcb8222f-4871-4ee4-9f8c-1a1a865fea47"
                }
            ]
        },
        {
            "id": "fb70acd8-9e77-4361-8a01-988d4c4d24d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ded2c0ed-8316-474a-9bf5-52598f27dcfd",
            "compositeImage": {
                "id": "dd8691c8-43b7-4a88-9f8d-4dd9a8b2d854",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb70acd8-9e77-4361-8a01-988d4c4d24d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86318899-59b5-4563-b123-a163b3ae3352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb70acd8-9e77-4361-8a01-988d4c4d24d2",
                    "LayerId": "dcb8222f-4871-4ee4-9f8c-1a1a865fea47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "dcb8222f-4871-4ee4-9f8c-1a1a865fea47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ded2c0ed-8316-474a-9bf5-52598f27dcfd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}