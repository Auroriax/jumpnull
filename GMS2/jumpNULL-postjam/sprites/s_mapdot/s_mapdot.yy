{
    "id": "9aae1be5-607a-47d0-8ba1-8db344d93ddb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_mapdot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "deec0017-2ada-488c-bdd0-965ddcfc48f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9aae1be5-607a-47d0-8ba1-8db344d93ddb",
            "compositeImage": {
                "id": "d507e92f-157c-44cd-a0b5-9cf22e1b5ad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deec0017-2ada-488c-bdd0-965ddcfc48f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "802e1e9c-ad22-4ab0-9d4c-877efff9eab6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deec0017-2ada-488c-bdd0-965ddcfc48f8",
                    "LayerId": "49f758e6-06df-4f20-855e-662cb17623f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "49f758e6-06df-4f20-855e-662cb17623f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9aae1be5-607a-47d0-8ba1-8db344d93ddb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}