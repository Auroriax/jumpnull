{
    "id": "4c077f61-89f0-453d-9d52-45998f5a8063",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_ladder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63087613-2be9-421c-af2d-d6854adf7e59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c077f61-89f0-453d-9d52-45998f5a8063",
            "compositeImage": {
                "id": "a415c317-ffe7-43b7-a934-ccfa97fd876f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63087613-2be9-421c-af2d-d6854adf7e59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67ef220a-cde6-4ff7-b239-969bdfa9f7c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63087613-2be9-421c-af2d-d6854adf7e59",
                    "LayerId": "ee868dff-d620-424a-b0a1-be5c34433d9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ee868dff-d620-424a-b0a1-be5c34433d9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c077f61-89f0-453d-9d52-45998f5a8063",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}