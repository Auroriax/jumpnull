{
    "id": "9c7cf525-e78c-43e9-886a-c7e0cd745c62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_slope",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40c84826-176f-4039-93f5-dff41909d9f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c7cf525-e78c-43e9-886a-c7e0cd745c62",
            "compositeImage": {
                "id": "8b8d679b-806e-418a-a3bf-b310ab8ac614",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40c84826-176f-4039-93f5-dff41909d9f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de0eef07-12e1-4ff9-810f-0f93ea2747dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40c84826-176f-4039-93f5-dff41909d9f4",
                    "LayerId": "b64bad87-682a-4c96-9c5f-a6b6e4135833"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "b64bad87-682a-4c96-9c5f-a6b6e4135833",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c7cf525-e78c-43e9-886a-c7e0cd745c62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}