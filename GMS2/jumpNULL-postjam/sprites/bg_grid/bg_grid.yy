{
    "id": "074edae3-efe7-4201-bd3e-00da2217fd2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_grid",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 143,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29530a78-f717-43bc-9665-694bfcb89bfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "074edae3-efe7-4201-bd3e-00da2217fd2b",
            "compositeImage": {
                "id": "2406707c-00f9-48d9-9141-84638e8409e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29530a78-f717-43bc-9665-694bfcb89bfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d6de453-da6e-45c4-ba4c-2f9a31bda13e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29530a78-f717-43bc-9665-694bfcb89bfd",
                    "LayerId": "21a0c525-bd8d-4150-8185-07696e1c202d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "21a0c525-bd8d-4150-8185-07696e1c202d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "074edae3-efe7-4201-bd3e-00da2217fd2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}