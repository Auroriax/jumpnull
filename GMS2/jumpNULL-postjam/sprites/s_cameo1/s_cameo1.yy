{
    "id": "492f7350-d041-47e9-90da-21063e38fcb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_cameo1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bc44a61-435b-4050-9668-fee4dd9f16cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "492f7350-d041-47e9-90da-21063e38fcb9",
            "compositeImage": {
                "id": "81fdd828-c78c-48d8-91a7-872bd873bb32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bc44a61-435b-4050-9668-fee4dd9f16cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f76bbf24-68b9-416f-b9f5-dc9a1edf1523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bc44a61-435b-4050-9668-fee4dd9f16cc",
                    "LayerId": "e4e18200-f1f9-47a8-a459-d9f1b7ce24d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "e4e18200-f1f9-47a8-a459-d9f1b7ce24d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "492f7350-d041-47e9-90da-21063e38fcb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 0
}