{
    "id": "5fb8bd98-7310-4200-9012-dae43b7f1a80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_gui",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 2,
    "bbox_right": 57,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5641f4d7-799f-42de-a577-249999eb8fcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fb8bd98-7310-4200-9012-dae43b7f1a80",
            "compositeImage": {
                "id": "a7706771-ff95-4c95-b5c3-436ece5c7a59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5641f4d7-799f-42de-a577-249999eb8fcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd8f0e42-99e3-4945-a2ae-81dcf8b6f13f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5641f4d7-799f-42de-a577-249999eb8fcf",
                    "LayerId": "c5a73a40-dc31-4e6e-acc2-6d2a82b236f6"
                }
            ]
        },
        {
            "id": "bc61f359-cee4-4baa-816b-74493a3ca84f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fb8bd98-7310-4200-9012-dae43b7f1a80",
            "compositeImage": {
                "id": "3a733dcb-17ed-4f13-b0c2-457c96d1c093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc61f359-cee4-4baa-816b-74493a3ca84f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1037f76d-840b-4c88-b6d1-68aa22f6ffa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc61f359-cee4-4baa-816b-74493a3ca84f",
                    "LayerId": "c5a73a40-dc31-4e6e-acc2-6d2a82b236f6"
                }
            ]
        },
        {
            "id": "18d9555b-2249-4978-8996-eb21aa3fc80d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fb8bd98-7310-4200-9012-dae43b7f1a80",
            "compositeImage": {
                "id": "ba61d777-623c-40d0-8321-e2ba3af28d0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18d9555b-2249-4978-8996-eb21aa3fc80d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f261879-cfb7-4dab-b220-92aa510b5a20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18d9555b-2249-4978-8996-eb21aa3fc80d",
                    "LayerId": "c5a73a40-dc31-4e6e-acc2-6d2a82b236f6"
                }
            ]
        },
        {
            "id": "6b2ba01a-e7fa-463f-bcb8-0668e7b7ed39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fb8bd98-7310-4200-9012-dae43b7f1a80",
            "compositeImage": {
                "id": "b88c8773-f8ea-47c7-9b70-a985fd77d576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b2ba01a-e7fa-463f-bcb8-0668e7b7ed39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc7a9176-e964-41c1-98a1-94b47916041a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b2ba01a-e7fa-463f-bcb8-0668e7b7ed39",
                    "LayerId": "c5a73a40-dc31-4e6e-acc2-6d2a82b236f6"
                }
            ]
        },
        {
            "id": "d3ed648c-e8f8-4f1e-884a-b53bd9cc06da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fb8bd98-7310-4200-9012-dae43b7f1a80",
            "compositeImage": {
                "id": "c8eab4ec-3439-42ec-8e81-69da602b143f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3ed648c-e8f8-4f1e-884a-b53bd9cc06da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ac43028-054a-4e01-b34e-f2a0065c89dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3ed648c-e8f8-4f1e-884a-b53bd9cc06da",
                    "LayerId": "c5a73a40-dc31-4e6e-acc2-6d2a82b236f6"
                }
            ]
        },
        {
            "id": "e3de783e-d974-4465-8ade-ffad7961c8a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fb8bd98-7310-4200-9012-dae43b7f1a80",
            "compositeImage": {
                "id": "21fe6611-3647-41fd-8d7d-59075ceef491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3de783e-d974-4465-8ade-ffad7961c8a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f7a8b93-29dd-4e05-8e7c-1fe7ac8a2012",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3de783e-d974-4465-8ade-ffad7961c8a6",
                    "LayerId": "c5a73a40-dc31-4e6e-acc2-6d2a82b236f6"
                }
            ]
        },
        {
            "id": "9b85f219-7890-418e-9b03-66db2c2fe37f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fb8bd98-7310-4200-9012-dae43b7f1a80",
            "compositeImage": {
                "id": "93b315ce-ebc0-4585-9b16-32783eb92444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b85f219-7890-418e-9b03-66db2c2fe37f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4147d4ad-f46f-468a-83ad-83cf151dd5ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b85f219-7890-418e-9b03-66db2c2fe37f",
                    "LayerId": "c5a73a40-dc31-4e6e-acc2-6d2a82b236f6"
                }
            ]
        },
        {
            "id": "6f02832c-03ba-4cd6-a904-d41ddc31b4ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fb8bd98-7310-4200-9012-dae43b7f1a80",
            "compositeImage": {
                "id": "17ed9f4e-f9b6-4069-9bf0-cb87eb832d16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f02832c-03ba-4cd6-a904-d41ddc31b4ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bd94a89-297a-4912-8bc0-be02c6489d35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f02832c-03ba-4cd6-a904-d41ddc31b4ab",
                    "LayerId": "c5a73a40-dc31-4e6e-acc2-6d2a82b236f6"
                }
            ]
        },
        {
            "id": "95b4c04b-09ae-4443-93f0-4f7574728198",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fb8bd98-7310-4200-9012-dae43b7f1a80",
            "compositeImage": {
                "id": "a5644fcd-546e-499c-8660-321ce1a2b66f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95b4c04b-09ae-4443-93f0-4f7574728198",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f0e2468-ce61-4243-bd04-ade4a09fdfcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95b4c04b-09ae-4443-93f0-4f7574728198",
                    "LayerId": "c5a73a40-dc31-4e6e-acc2-6d2a82b236f6"
                }
            ]
        },
        {
            "id": "44939528-fdc1-4bf2-888c-6280a44f5aed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fb8bd98-7310-4200-9012-dae43b7f1a80",
            "compositeImage": {
                "id": "94cb96d3-6a13-4881-ace6-b0152ad61c93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44939528-fdc1-4bf2-888c-6280a44f5aed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bce36df-1ca3-4300-a3f9-4c69f9affec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44939528-fdc1-4bf2-888c-6280a44f5aed",
                    "LayerId": "c5a73a40-dc31-4e6e-acc2-6d2a82b236f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c5a73a40-dc31-4e6e-acc2-6d2a82b236f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5fb8bd98-7310-4200-9012-dae43b7f1a80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}