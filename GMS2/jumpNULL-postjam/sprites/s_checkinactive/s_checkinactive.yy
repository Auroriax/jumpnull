{
    "id": "8301f30b-3913-4ba9-b993-229fda449539",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_checkinactive",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 5,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72bc7a5c-4612-4786-9fd5-c59cea44d32d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8301f30b-3913-4ba9-b993-229fda449539",
            "compositeImage": {
                "id": "c872b55c-0ea3-4d3e-9a74-49a899ec4b8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72bc7a5c-4612-4786-9fd5-c59cea44d32d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f07b0ac6-b2a8-4acd-8947-c5028663927f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72bc7a5c-4612-4786-9fd5-c59cea44d32d",
                    "LayerId": "bc5823a4-5daf-4712-af6d-e7e5c31860ad"
                }
            ]
        },
        {
            "id": "f76cf378-5cf9-4ce2-af28-85d91ddf8031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8301f30b-3913-4ba9-b993-229fda449539",
            "compositeImage": {
                "id": "28b80d70-b35f-44ae-a28e-2725cc16d4bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f76cf378-5cf9-4ce2-af28-85d91ddf8031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b985639-211c-495d-993f-fa6382eeef96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76cf378-5cf9-4ce2-af28-85d91ddf8031",
                    "LayerId": "bc5823a4-5daf-4712-af6d-e7e5c31860ad"
                }
            ]
        },
        {
            "id": "02172926-f06a-40c4-bfc8-12e7760b1920",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8301f30b-3913-4ba9-b993-229fda449539",
            "compositeImage": {
                "id": "3d07079f-d928-460f-8835-ca81c0a2dd80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02172926-f06a-40c4-bfc8-12e7760b1920",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a25b481-0610-4989-8b92-6363d0951e9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02172926-f06a-40c4-bfc8-12e7760b1920",
                    "LayerId": "bc5823a4-5daf-4712-af6d-e7e5c31860ad"
                }
            ]
        },
        {
            "id": "2a30e4f1-f4ab-4465-bd60-1b6ff14cea30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8301f30b-3913-4ba9-b993-229fda449539",
            "compositeImage": {
                "id": "016e2b59-5345-4906-913b-7c4a11070943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a30e4f1-f4ab-4465-bd60-1b6ff14cea30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "811df4a3-1df9-4d68-a61d-c79656275a5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a30e4f1-f4ab-4465-bd60-1b6ff14cea30",
                    "LayerId": "bc5823a4-5daf-4712-af6d-e7e5c31860ad"
                }
            ]
        },
        {
            "id": "7435a541-481a-4f36-a31f-d010af5d38e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8301f30b-3913-4ba9-b993-229fda449539",
            "compositeImage": {
                "id": "e1c8257f-ed0e-47eb-866a-a779b481a61c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7435a541-481a-4f36-a31f-d010af5d38e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50d20a13-27a0-4bc5-abe5-556b621cf590",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7435a541-481a-4f36-a31f-d010af5d38e3",
                    "LayerId": "bc5823a4-5daf-4712-af6d-e7e5c31860ad"
                }
            ]
        },
        {
            "id": "3d99f692-e7fd-4000-b15a-9c6f6e84c280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8301f30b-3913-4ba9-b993-229fda449539",
            "compositeImage": {
                "id": "5210c81c-135c-4d8b-84d4-b13e95983332",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d99f692-e7fd-4000-b15a-9c6f6e84c280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6def9a4-ce48-4c82-97b8-a4102fb2e197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d99f692-e7fd-4000-b15a-9c6f6e84c280",
                    "LayerId": "bc5823a4-5daf-4712-af6d-e7e5c31860ad"
                }
            ]
        },
        {
            "id": "e7dc3910-1dcb-4842-8a25-061da639ea1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8301f30b-3913-4ba9-b993-229fda449539",
            "compositeImage": {
                "id": "efdbc9a4-336e-4610-832c-728228f24e41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7dc3910-1dcb-4842-8a25-061da639ea1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "951fb171-a3bf-42d3-941c-5feeba5516c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7dc3910-1dcb-4842-8a25-061da639ea1f",
                    "LayerId": "bc5823a4-5daf-4712-af6d-e7e5c31860ad"
                }
            ]
        },
        {
            "id": "9855768c-fc12-4567-b52c-b4fac1fd74fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8301f30b-3913-4ba9-b993-229fda449539",
            "compositeImage": {
                "id": "8efb1006-df44-4e0a-a951-56eac7dbb64a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9855768c-fc12-4567-b52c-b4fac1fd74fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16b2725c-674f-4069-a55a-9e2835aef54c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9855768c-fc12-4567-b52c-b4fac1fd74fb",
                    "LayerId": "bc5823a4-5daf-4712-af6d-e7e5c31860ad"
                }
            ]
        },
        {
            "id": "d87c28ad-1f23-4afb-b975-a5d5bedb86e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8301f30b-3913-4ba9-b993-229fda449539",
            "compositeImage": {
                "id": "c9ef47db-4b2f-49c2-8326-d682d77c7468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d87c28ad-1f23-4afb-b975-a5d5bedb86e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "329e0538-fa10-4bf8-b5e1-22e77503f7de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d87c28ad-1f23-4afb-b975-a5d5bedb86e1",
                    "LayerId": "bc5823a4-5daf-4712-af6d-e7e5c31860ad"
                }
            ]
        },
        {
            "id": "6702d446-40b9-4565-95d6-3b49451942fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8301f30b-3913-4ba9-b993-229fda449539",
            "compositeImage": {
                "id": "36061432-5487-49d6-9093-325f26abe5ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6702d446-40b9-4565-95d6-3b49451942fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec5a9c2a-762e-43fc-b868-655112ed0ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6702d446-40b9-4565-95d6-3b49451942fe",
                    "LayerId": "bc5823a4-5daf-4712-af6d-e7e5c31860ad"
                }
            ]
        },
        {
            "id": "44c23c24-7271-4aeb-afb4-8771c2573442",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8301f30b-3913-4ba9-b993-229fda449539",
            "compositeImage": {
                "id": "7e7b8985-0b75-4302-b1f3-8759e09fb1ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44c23c24-7271-4aeb-afb4-8771c2573442",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9322a84f-825f-4b39-84f9-63c896620607",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44c23c24-7271-4aeb-afb4-8771c2573442",
                    "LayerId": "bc5823a4-5daf-4712-af6d-e7e5c31860ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bc5823a4-5daf-4712-af6d-e7e5c31860ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8301f30b-3913-4ba9-b993-229fda449539",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}