{
    "id": "1f3ed043-734a-4843-ad00-d19799a058b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dialog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80000602-01f8-4855-8898-9e239c839f0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f3ed043-734a-4843-ad00-d19799a058b8",
            "compositeImage": {
                "id": "6da40c7f-48f7-4c96-8eb7-6831c5a310b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80000602-01f8-4855-8898-9e239c839f0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43d97458-0f2c-48b5-a6f5-f0c935383cec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80000602-01f8-4855-8898-9e239c839f0e",
                    "LayerId": "d1c3b1dc-0438-43fc-b44c-64784ce111f1"
                }
            ]
        },
        {
            "id": "be0f7ad4-cc9f-4680-9894-87d2a5f7c042",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f3ed043-734a-4843-ad00-d19799a058b8",
            "compositeImage": {
                "id": "c8998e08-8aa2-4ee0-ac4d-1898b12971bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be0f7ad4-cc9f-4680-9894-87d2a5f7c042",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a5406f2-f08c-4b71-968e-32863ee10a27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be0f7ad4-cc9f-4680-9894-87d2a5f7c042",
                    "LayerId": "d1c3b1dc-0438-43fc-b44c-64784ce111f1"
                }
            ]
        },
        {
            "id": "4af63849-f02c-4a92-8862-ee1c2c47720a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f3ed043-734a-4843-ad00-d19799a058b8",
            "compositeImage": {
                "id": "17e38a11-651e-46f5-8139-f0d2b1432fbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4af63849-f02c-4a92-8862-ee1c2c47720a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a242c9f-cfc9-4e9d-bba9-52306fb90a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4af63849-f02c-4a92-8862-ee1c2c47720a",
                    "LayerId": "d1c3b1dc-0438-43fc-b44c-64784ce111f1"
                }
            ]
        },
        {
            "id": "d905453c-052f-490a-8a3a-dd9f2fd682cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f3ed043-734a-4843-ad00-d19799a058b8",
            "compositeImage": {
                "id": "fdcb5fd1-ad9b-47df-989a-0917ddfeffcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d905453c-052f-490a-8a3a-dd9f2fd682cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8e9ea42-c1dd-401e-98f6-c33f074318d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d905453c-052f-490a-8a3a-dd9f2fd682cf",
                    "LayerId": "d1c3b1dc-0438-43fc-b44c-64784ce111f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d1c3b1dc-0438-43fc-b44c-64784ce111f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f3ed043-734a-4843-ad00-d19799a058b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}