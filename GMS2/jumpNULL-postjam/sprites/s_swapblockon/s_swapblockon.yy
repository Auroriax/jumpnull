{
    "id": "35a31cf3-fe31-4a6b-b9c0-cc4c76a7fc51",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_swapblockon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ce56560-b641-47ff-883c-44375c628589",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35a31cf3-fe31-4a6b-b9c0-cc4c76a7fc51",
            "compositeImage": {
                "id": "935adb98-94fc-49f3-8a60-ec07acac0234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ce56560-b641-47ff-883c-44375c628589",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69d906d5-b1ce-481f-a4cc-6b97b66faf4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ce56560-b641-47ff-883c-44375c628589",
                    "LayerId": "ed026f35-4d31-47d5-a5f4-8fbd70f6815d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ed026f35-4d31-47d5-a5f4-8fbd70f6815d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35a31cf3-fe31-4a6b-b9c0-cc4c76a7fc51",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}