{
    "id": "f68c9586-77b0-4c81-8a47-1302fae75103",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_highslopemask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 4,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0f03d7e-cc62-42f5-ae3a-a5e356ed20da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f68c9586-77b0-4c81-8a47-1302fae75103",
            "compositeImage": {
                "id": "948d0ab4-f05f-4f1d-8140-d78d6d88e541",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0f03d7e-cc62-42f5-ae3a-a5e356ed20da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32d15442-fad6-4ae5-b75f-aecc49a9c8a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0f03d7e-cc62-42f5-ae3a-a5e356ed20da",
                    "LayerId": "a778218d-8c74-4e46-af41-0e8cdc89748f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "a778218d-8c74-4e46-af41-0e8cdc89748f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f68c9586-77b0-4c81-8a47-1302fae75103",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}