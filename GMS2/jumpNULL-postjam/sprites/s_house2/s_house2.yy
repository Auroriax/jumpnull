{
    "id": "64df0d4d-d811-4e72-99d8-ee8c01629f57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_house2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 71,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c528a74e-fd8f-4973-bb12-e35e4a781f77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64df0d4d-d811-4e72-99d8-ee8c01629f57",
            "compositeImage": {
                "id": "4c806700-ed21-414b-9ddb-fba5e4bde6f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c528a74e-fd8f-4973-bb12-e35e4a781f77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "702f649e-f304-474c-b599-b1162929ee11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c528a74e-fd8f-4973-bb12-e35e4a781f77",
                    "LayerId": "52f8c20f-9e74-4277-8b24-47991afccf4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "52f8c20f-9e74-4277-8b24-47991afccf4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64df0d4d-d811-4e72-99d8-ee8c01629f57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}