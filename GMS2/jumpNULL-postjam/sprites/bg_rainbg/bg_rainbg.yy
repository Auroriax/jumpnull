{
    "id": "7171d930-bee6-4920-9de8-7a0ae2e48cc1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_rainbg",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 139,
    "bbox_left": 9,
    "bbox_right": 152,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ccf7af8-3213-4026-8eaf-1d1ac687e733",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7171d930-bee6-4920-9de8-7a0ae2e48cc1",
            "compositeImage": {
                "id": "b11d13d1-1667-4f55-8227-b965dabbf0f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ccf7af8-3213-4026-8eaf-1d1ac687e733",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "085307d4-b646-4af9-b377-807bde02b913",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ccf7af8-3213-4026-8eaf-1d1ac687e733",
                    "LayerId": "5d4644b7-d8c8-4431-bed5-c27e04bd2da0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 140,
    "layers": [
        {
            "id": "5d4644b7-d8c8-4431-bed5-c27e04bd2da0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7171d930-bee6-4920-9de8-7a0ae2e48cc1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}