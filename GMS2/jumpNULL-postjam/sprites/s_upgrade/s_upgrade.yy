{
    "id": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_upgrade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "880eab87-2fbf-4e2a-a1d4-d0a36064118f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
            "compositeImage": {
                "id": "6f38562f-7c12-425d-bf64-061740a2f4b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "880eab87-2fbf-4e2a-a1d4-d0a36064118f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca593709-c34d-4964-a5a0-f763b4924492",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "880eab87-2fbf-4e2a-a1d4-d0a36064118f",
                    "LayerId": "81ada369-fe8a-4295-804d-ac181b457797"
                }
            ]
        },
        {
            "id": "37a0d690-e0be-4cf8-b919-e7fb550a4827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
            "compositeImage": {
                "id": "d6cbf7c5-4a55-4afd-815b-51cfb43d8f01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37a0d690-e0be-4cf8-b919-e7fb550a4827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68d22d7f-c200-4e00-b216-706b8aa4f51a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37a0d690-e0be-4cf8-b919-e7fb550a4827",
                    "LayerId": "81ada369-fe8a-4295-804d-ac181b457797"
                }
            ]
        },
        {
            "id": "49300c95-4183-47d4-9f67-843318dd40fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
            "compositeImage": {
                "id": "500f22c9-7076-495f-949d-312b2ac5461b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49300c95-4183-47d4-9f67-843318dd40fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "750ae8b1-b0f8-4748-9729-f4c5e8637471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49300c95-4183-47d4-9f67-843318dd40fd",
                    "LayerId": "81ada369-fe8a-4295-804d-ac181b457797"
                }
            ]
        },
        {
            "id": "e05f5693-c655-4e08-b869-95c7d84eb35a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
            "compositeImage": {
                "id": "38664c0a-6b73-424a-b511-ea56831d8b7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e05f5693-c655-4e08-b869-95c7d84eb35a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0abf76c8-6e78-4598-ad59-d2735740115b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e05f5693-c655-4e08-b869-95c7d84eb35a",
                    "LayerId": "81ada369-fe8a-4295-804d-ac181b457797"
                }
            ]
        },
        {
            "id": "874ce40e-7513-4b8f-8502-19ec69c5c180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
            "compositeImage": {
                "id": "c0ce3e87-d84b-415f-95cd-a0721462b946",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "874ce40e-7513-4b8f-8502-19ec69c5c180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56627a3c-6ea3-4f9c-b5cc-1485a8b9ae1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "874ce40e-7513-4b8f-8502-19ec69c5c180",
                    "LayerId": "81ada369-fe8a-4295-804d-ac181b457797"
                }
            ]
        },
        {
            "id": "2a921180-4063-4a39-b6fd-a4d29fb63aec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
            "compositeImage": {
                "id": "efc5dee5-d81a-401d-9f95-bcdd227ef5af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a921180-4063-4a39-b6fd-a4d29fb63aec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48481fca-4ab0-41a5-9522-b7fd85d23e8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a921180-4063-4a39-b6fd-a4d29fb63aec",
                    "LayerId": "81ada369-fe8a-4295-804d-ac181b457797"
                }
            ]
        },
        {
            "id": "467b69ec-655f-4bc4-b4f8-ea9ef13ad306",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
            "compositeImage": {
                "id": "26b0b2b1-ffb5-4ea3-87dd-9f4bdc6477ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "467b69ec-655f-4bc4-b4f8-ea9ef13ad306",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e94faff8-b29b-4a53-976f-1948c781be44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "467b69ec-655f-4bc4-b4f8-ea9ef13ad306",
                    "LayerId": "81ada369-fe8a-4295-804d-ac181b457797"
                }
            ]
        },
        {
            "id": "9fe32fae-53c8-429e-9823-ff2b7c9f5691",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
            "compositeImage": {
                "id": "198e4ed3-7b08-4633-b51c-8823bc7f866a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fe32fae-53c8-429e-9823-ff2b7c9f5691",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cedcb0f1-5f40-4f00-a767-0ec83a9636fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fe32fae-53c8-429e-9823-ff2b7c9f5691",
                    "LayerId": "81ada369-fe8a-4295-804d-ac181b457797"
                }
            ]
        },
        {
            "id": "ba2ae3a5-9787-467e-ba62-02abe45835b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
            "compositeImage": {
                "id": "4d770e02-be64-4a1a-af94-ea4dbde82f83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba2ae3a5-9787-467e-ba62-02abe45835b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "419cdfd5-625d-4c5b-b270-9f1fc37a0e5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba2ae3a5-9787-467e-ba62-02abe45835b3",
                    "LayerId": "81ada369-fe8a-4295-804d-ac181b457797"
                }
            ]
        },
        {
            "id": "e6857b7c-cd2d-4af5-b8a5-759707fc0fa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
            "compositeImage": {
                "id": "2ad8fa72-2703-4215-af34-8154eb2cd154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6857b7c-cd2d-4af5-b8a5-759707fc0fa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dff6a0a6-4fba-4cac-84b3-0d893d25475e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6857b7c-cd2d-4af5-b8a5-759707fc0fa6",
                    "LayerId": "81ada369-fe8a-4295-804d-ac181b457797"
                }
            ]
        },
        {
            "id": "9e18bccc-d78f-44b9-81ce-d2d6833305da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
            "compositeImage": {
                "id": "b976a2d7-0441-4077-b0a8-0f3fc723fe3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e18bccc-d78f-44b9-81ce-d2d6833305da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0625367b-e210-4289-9892-752f97c2a4ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e18bccc-d78f-44b9-81ce-d2d6833305da",
                    "LayerId": "81ada369-fe8a-4295-804d-ac181b457797"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "81ada369-fe8a-4295-804d-ac181b457797",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57cea7e2-cb7f-4e3f-9659-a25acbe7f59a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}