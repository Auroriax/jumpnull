{
    "id": "eb1b716c-6a75-404c-9751-237c6cde5597",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_lowslope3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e3e9d97-a44e-4dd3-8afd-2618d5d2b4c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb1b716c-6a75-404c-9751-237c6cde5597",
            "compositeImage": {
                "id": "46ece159-fc34-4de9-8706-495af864875d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e3e9d97-a44e-4dd3-8afd-2618d5d2b4c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "958144d1-b7b5-4882-8f80-4d0d6a4192ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e3e9d97-a44e-4dd3-8afd-2618d5d2b4c8",
                    "LayerId": "4edb172d-4d1b-4da6-832e-f405398663e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "4edb172d-4d1b-4da6-832e-f405398663e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb1b716c-6a75-404c-9751-237c6cde5597",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}