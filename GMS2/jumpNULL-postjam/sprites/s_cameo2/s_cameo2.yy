{
    "id": "2a869b61-9f2e-4c50-a69f-ddf19657b2e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_cameo2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7490351f-bf5b-4758-b1ed-835df6f18f75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a869b61-9f2e-4c50-a69f-ddf19657b2e4",
            "compositeImage": {
                "id": "2ce8b4d1-7be4-485a-ba9b-5f2c57a8c149",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7490351f-bf5b-4758-b1ed-835df6f18f75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8076ed40-22a2-4a5c-aaf4-24997d162eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7490351f-bf5b-4758-b1ed-835df6f18f75",
                    "LayerId": "aacbfff5-53f5-4df0-b268-ed8379b3a4c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "aacbfff5-53f5-4df0-b268-ed8379b3a4c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a869b61-9f2e-4c50-a69f-ddf19657b2e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 0
}