{
    "id": "521fc2e2-1d30-4cbb-93f7-45bc94744df8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_lowslope2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a120db72-63f9-4a77-b948-1016cdcd9084",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "521fc2e2-1d30-4cbb-93f7-45bc94744df8",
            "compositeImage": {
                "id": "89ee14e6-5796-430c-b680-d51921ed1eb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a120db72-63f9-4a77-b948-1016cdcd9084",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c42ec4f-60d0-4e21-9844-e8cce2beaf68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a120db72-63f9-4a77-b948-1016cdcd9084",
                    "LayerId": "e7200a94-1a4b-496d-88fe-634c7266399d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "e7200a94-1a4b-496d-88fe-634c7266399d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "521fc2e2-1d30-4cbb-93f7-45bc94744df8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}