{
    "id": "d077f629-0698-481f-8de0-8050dcfe55c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hologramgirl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e015356-f5e2-4dcd-85ba-bb4a75c36ea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d077f629-0698-481f-8de0-8050dcfe55c4",
            "compositeImage": {
                "id": "3ad93517-035e-4694-a7d5-b0314b9a8f5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e015356-f5e2-4dcd-85ba-bb4a75c36ea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "809add3f-7afa-4d25-b65b-82b43284f661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e015356-f5e2-4dcd-85ba-bb4a75c36ea0",
                    "LayerId": "18e14197-2ba3-4be4-9cc5-4218851b055f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "18e14197-2ba3-4be4-9cc5-4218851b055f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d077f629-0698-481f-8de0-8050dcfe55c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 0
}