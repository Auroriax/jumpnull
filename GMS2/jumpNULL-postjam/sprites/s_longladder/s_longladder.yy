{
    "id": "bbd79735-068f-4a82-ba73-10ca370d2a97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_longladder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0740ca6f-ada9-4d6a-901c-4ce76e8ebdb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbd79735-068f-4a82-ba73-10ca370d2a97",
            "compositeImage": {
                "id": "aeed25f6-4b25-443b-8baf-6585dc7c265f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0740ca6f-ada9-4d6a-901c-4ce76e8ebdb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e040bead-f058-4d1e-b064-508aa25b5f3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0740ca6f-ada9-4d6a-901c-4ce76e8ebdb7",
                    "LayerId": "3557e8d2-d930-4bf3-9570-ca73757aebf4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3557e8d2-d930-4bf3-9570-ca73757aebf4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbd79735-068f-4a82-ba73-10ca370d2a97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}