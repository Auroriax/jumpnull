{
    "id": "edeab854-d7c4-44a7-b363-7b9b2ed5a200",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_highslope2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "205534cc-7b13-4080-a879-ae0aaea03e6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "edeab854-d7c4-44a7-b363-7b9b2ed5a200",
            "compositeImage": {
                "id": "3fe8beb8-5138-432a-8570-c34d964dcab6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "205534cc-7b13-4080-a879-ae0aaea03e6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1549229-cd17-4b6d-ba50-1887d4d4e9ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "205534cc-7b13-4080-a879-ae0aaea03e6d",
                    "LayerId": "a9e8d58f-5ca4-4bb6-9d7b-f9362073dd31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "a9e8d58f-5ca4-4bb6-9d7b-f9362073dd31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "edeab854-d7c4-44a7-b363-7b9b2ed5a200",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}