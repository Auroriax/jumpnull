{
    "id": "56ea0c30-4f03-4943-9935-bd626f70042d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_swapblockoff",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b8e2afb-a729-410f-8214-ddcb548bf99f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56ea0c30-4f03-4943-9935-bd626f70042d",
            "compositeImage": {
                "id": "7730fe2d-9e77-4a94-81a8-42dd2346dd98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b8e2afb-a729-410f-8214-ddcb548bf99f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fae4b84c-f935-4390-92b7-eb927cae8d23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b8e2afb-a729-410f-8214-ddcb548bf99f",
                    "LayerId": "419aeae1-a15c-4413-bfda-bb6234c28a7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "419aeae1-a15c-4413-bfda-bb6234c28a7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56ea0c30-4f03-4943-9935-bd626f70042d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}