{
    "id": "3946dd51-0cb9-4065-8afe-e983642b86fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bridge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e26d1c16-5f21-4bd3-b0de-87501aafe560",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3946dd51-0cb9-4065-8afe-e983642b86fb",
            "compositeImage": {
                "id": "19b88415-1699-4e05-9328-ccc60d02b1f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e26d1c16-5f21-4bd3-b0de-87501aafe560",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0b8fab7-b6a9-47d1-9102-3e8e6aa031c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e26d1c16-5f21-4bd3-b0de-87501aafe560",
                    "LayerId": "e8829377-d158-4fe7-896a-ded928aa15f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "e8829377-d158-4fe7-896a-ded928aa15f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3946dd51-0cb9-4065-8afe-e983642b86fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}