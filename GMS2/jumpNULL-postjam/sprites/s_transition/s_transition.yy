{
    "id": "df197064-afed-45b5-9d7e-4170c0189ba4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_transition",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4b1dbe6-a124-421f-a10e-a05bbf9527af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "f45137cb-f4c0-4d70-acf0-6cb83874949b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4b1dbe6-a124-421f-a10e-a05bbf9527af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b638030-be70-43ba-a92f-ddf1ca7023cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4b1dbe6-a124-421f-a10e-a05bbf9527af",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "af167850-d3fe-42be-bebe-c9f5b1ba3823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "07c3e3dc-8168-43b9-8983-d6e28dbc992a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af167850-d3fe-42be-bebe-c9f5b1ba3823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14290b68-c4c4-4b61-a713-30ebc930cb52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af167850-d3fe-42be-bebe-c9f5b1ba3823",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "e97add46-0c63-4c37-90a2-7dd6c9dca0d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "c3630cf4-17db-4305-a55c-cc5ee2fc9ecf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e97add46-0c63-4c37-90a2-7dd6c9dca0d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4add4609-fede-4f8e-a6d2-e4bb8e461b0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e97add46-0c63-4c37-90a2-7dd6c9dca0d6",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "5f78ad3d-b8b4-4b0c-b55a-e5212937721f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "066d6df5-a756-4d62-aade-7b473ccc14eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f78ad3d-b8b4-4b0c-b55a-e5212937721f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e01a8be-d40f-4729-90db-e1682c44099b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f78ad3d-b8b4-4b0c-b55a-e5212937721f",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "2c528d81-d61d-4db7-a8cd-36319c7864f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "8d33b506-347e-46ce-9858-89e8b01a21db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c528d81-d61d-4db7-a8cd-36319c7864f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dce7454-52da-4723-9c71-2efa3619e5bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c528d81-d61d-4db7-a8cd-36319c7864f8",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "73faf990-9b41-46da-bf95-feac78e2dfe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "2c78815a-71d7-469f-84e3-8737e5557116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73faf990-9b41-46da-bf95-feac78e2dfe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e79db486-170c-495d-8605-bcf118ce1d63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73faf990-9b41-46da-bf95-feac78e2dfe3",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "1d6a8c8a-765e-4f60-842b-06ed3f54f382",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "311d30a8-e363-4365-b59f-1605b45e8f4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d6a8c8a-765e-4f60-842b-06ed3f54f382",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b9e5a1-5049-4e8a-9e0f-c496372adedc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d6a8c8a-765e-4f60-842b-06ed3f54f382",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "c3d811d8-fd84-4dc1-9ef4-4ee7757ac596",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "21b92133-e34f-46dd-bb53-5d499eb4e62e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3d811d8-fd84-4dc1-9ef4-4ee7757ac596",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc30799b-631a-48f8-80c6-22673507a2d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3d811d8-fd84-4dc1-9ef4-4ee7757ac596",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "53454a84-6173-4374-b46f-00ebaa8ce280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "419d9948-f678-472d-956a-034ff888dc7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53454a84-6173-4374-b46f-00ebaa8ce280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "784330d3-90db-4053-9f40-3be96fd8d6bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53454a84-6173-4374-b46f-00ebaa8ce280",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "e922a344-73a4-4e44-a564-6d8d038ec126",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "e43b7893-7ea8-444e-a57f-3680e0158974",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e922a344-73a4-4e44-a564-6d8d038ec126",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f796c26-4e23-4d6f-bdfc-c2bd3bba7a22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e922a344-73a4-4e44-a564-6d8d038ec126",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "ba2a0083-7236-4c08-a863-7178e4cf19d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "5b48923c-7747-4cad-bdbb-9c45d436ca3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba2a0083-7236-4c08-a863-7178e4cf19d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8b350a7-26da-4628-8700-545b6fd8045c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba2a0083-7236-4c08-a863-7178e4cf19d3",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "8b6a41aa-e9dd-4eea-9435-4976d5c728eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "d29f02b0-70c8-4d83-9f02-680f35cfb7bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b6a41aa-e9dd-4eea-9435-4976d5c728eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fbacaab-2058-4d18-be6f-1db9749fad91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b6a41aa-e9dd-4eea-9435-4976d5c728eb",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "b5db823b-0ba4-4bdb-9817-c48f08b16a6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "210cfde3-e086-4522-8743-87cdd0c759d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5db823b-0ba4-4bdb-9817-c48f08b16a6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2039829e-5902-4ec4-87f6-eb9530e2cf15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5db823b-0ba4-4bdb-9817-c48f08b16a6a",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "19b540f6-b6f3-4f45-80a4-c6e541c7eea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "5ebfed98-d5d2-4f89-befa-8443ccb60b01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19b540f6-b6f3-4f45-80a4-c6e541c7eea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4403211-6e6d-40df-9afc-d66846f90a88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19b540f6-b6f3-4f45-80a4-c6e541c7eea4",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "73a7713c-12aa-4c05-87de-82c4206a805f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "b734e225-c762-4294-87c3-3d08a4610f3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73a7713c-12aa-4c05-87de-82c4206a805f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d8ff91c-bc70-4ae9-a432-d7b91c9b0d3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73a7713c-12aa-4c05-87de-82c4206a805f",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "5f2ff509-8281-4241-affe-51d924057549",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "df886dec-a286-43e3-8c45-833bc3146f1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f2ff509-8281-4241-affe-51d924057549",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33fbe405-8171-49cf-a6f7-1d9f986a4597",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f2ff509-8281-4241-affe-51d924057549",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "4982b29e-9e39-4a61-b4f1-9a855c1f5c4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "180a0118-33d3-43b8-8aee-82279adb48eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4982b29e-9e39-4a61-b4f1-9a855c1f5c4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b8ef29f-5db1-4381-bb3b-101ab95cb82d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4982b29e-9e39-4a61-b4f1-9a855c1f5c4e",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "be35a606-dc60-4ab7-ac9e-b26fd0536955",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "5717c12b-cd22-4739-a00e-d8eb88981829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be35a606-dc60-4ab7-ac9e-b26fd0536955",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a4a358c-578e-4663-bc93-a37a4cf625ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be35a606-dc60-4ab7-ac9e-b26fd0536955",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "0fcf4b2b-7dde-4247-877e-5ace7989d835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "e370612c-b587-4d6e-9683-388b354406e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fcf4b2b-7dde-4247-877e-5ace7989d835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01009643-1195-4413-82ab-aee45616893a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fcf4b2b-7dde-4247-877e-5ace7989d835",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "dc9a3f77-50d1-42d7-bd7e-97466e64cefd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "d52dcfbd-4a9c-4e8c-a883-c074cea56244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc9a3f77-50d1-42d7-bd7e-97466e64cefd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f730ca7d-83f6-4d77-a324-6f5a4f0f4542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc9a3f77-50d1-42d7-bd7e-97466e64cefd",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "9a3bc5fb-4769-41f4-89ed-c61e685b780b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "e853023c-5541-49fa-b08b-daae12dd389d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a3bc5fb-4769-41f4-89ed-c61e685b780b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf969fdb-aecf-484b-8378-603b021b31e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a3bc5fb-4769-41f4-89ed-c61e685b780b",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        },
        {
            "id": "a3d9f8e5-3a53-48cc-b464-43c63ecf7be9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "compositeImage": {
                "id": "7bca9268-8f0f-4b6f-bca2-e50f418d6309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3d9f8e5-3a53-48cc-b464-43c63ecf7be9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00c8ba47-f09b-4b0a-b6f8-c54dfe2dc257",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3d9f8e5-3a53-48cc-b464-43c63ecf7be9",
                    "LayerId": "59a23d24-de86-453a-a21c-c1aa777675a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "59a23d24-de86-453a-a21c-c1aa777675a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df197064-afed-45b5-9d7e-4170c0189ba4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}