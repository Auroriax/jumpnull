{
    "id": "3366eb4a-8795-48ef-b24f-4fb8921c2c44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_fallinlight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 0,
    "bbox_right": 94,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17e22b30-987b-4654-a26f-9f5e398a158c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3366eb4a-8795-48ef-b24f-4fb8921c2c44",
            "compositeImage": {
                "id": "3fe56be1-4ad1-4610-867f-66f3a2258b11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17e22b30-987b-4654-a26f-9f5e398a158c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edbe2596-1893-4dbf-9fdd-253fad73da52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17e22b30-987b-4654-a26f-9f5e398a158c",
                    "LayerId": "65c29888-63c2-496b-bcc2-524fa758e0a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 118,
    "layers": [
        {
            "id": "65c29888-63c2-496b-bcc2-524fa758e0a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3366eb4a-8795-48ef-b24f-4fb8921c2c44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 95,
    "xorig": -23,
    "yorig": 0
}