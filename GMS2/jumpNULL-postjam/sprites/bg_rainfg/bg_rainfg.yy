{
    "id": "51e7f4c2-5fde-4487-bd5b-274019d7ba22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_rainfg",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 139,
    "bbox_left": 6,
    "bbox_right": 128,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3298c8b5-3410-4293-89c2-a0b0f32f28ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51e7f4c2-5fde-4487-bd5b-274019d7ba22",
            "compositeImage": {
                "id": "ca1fc2ac-5db4-44c7-9921-f86cff5736f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3298c8b5-3410-4293-89c2-a0b0f32f28ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a068a0bd-d87b-4784-b3f2-cfe0f45a8c95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3298c8b5-3410-4293-89c2-a0b0f32f28ea",
                    "LayerId": "1d833a3b-3839-4f78-9854-5f40d94fe4fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 140,
    "layers": [
        {
            "id": "1d833a3b-3839-4f78-9854-5f40d94fe4fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51e7f4c2-5fde-4487-bd5b-274019d7ba22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}