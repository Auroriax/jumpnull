{
    "id": "8bac8438-fb4b-48d1-b264-8a85563185a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_shrubbery",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d67ce521-e3a1-4be4-b435-064ef3a23f4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bac8438-fb4b-48d1-b264-8a85563185a8",
            "compositeImage": {
                "id": "76a98944-602f-4f02-a01a-d425af93254b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d67ce521-e3a1-4be4-b435-064ef3a23f4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0ddd3f8-a0ff-47bc-a519-d4b876982124",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d67ce521-e3a1-4be4-b435-064ef3a23f4f",
                    "LayerId": "bf583b33-43f4-4c98-9716-1b9a7654c90e"
                }
            ]
        },
        {
            "id": "04d90268-5401-4612-879c-ec652398e58e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bac8438-fb4b-48d1-b264-8a85563185a8",
            "compositeImage": {
                "id": "cb018f90-165b-4b1f-b563-b023552a16ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04d90268-5401-4612-879c-ec652398e58e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "751db5f6-0448-4726-9707-36b8d21da08a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04d90268-5401-4612-879c-ec652398e58e",
                    "LayerId": "bf583b33-43f4-4c98-9716-1b9a7654c90e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "bf583b33-43f4-4c98-9716-1b9a7654c90e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8bac8438-fb4b-48d1-b264-8a85563185a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}