{
    "id": "b8bef6d1-70d6-45d3-b7b4-5e9234156d63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dude",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "287c7196-ef6f-4244-a111-fdf59a249b8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8bef6d1-70d6-45d3-b7b4-5e9234156d63",
            "compositeImage": {
                "id": "9376c8ef-3863-4bd8-8e99-103054ead887",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "287c7196-ef6f-4244-a111-fdf59a249b8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcf3a52b-6017-4602-a70b-71959d129eee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "287c7196-ef6f-4244-a111-fdf59a249b8e",
                    "LayerId": "9bf5ac8f-0dda-43bf-a8d4-fa39df0bb9ce"
                }
            ]
        },
        {
            "id": "a580e2c3-1020-4245-8872-6a5560e47131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8bef6d1-70d6-45d3-b7b4-5e9234156d63",
            "compositeImage": {
                "id": "772a5685-8f12-4e90-8bed-413ae6633779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a580e2c3-1020-4245-8872-6a5560e47131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c20b1b03-72cc-41a0-86f4-a837ff1d3623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a580e2c3-1020-4245-8872-6a5560e47131",
                    "LayerId": "9bf5ac8f-0dda-43bf-a8d4-fa39df0bb9ce"
                }
            ]
        },
        {
            "id": "c0d0d11b-9e24-4126-8703-836b65eb922c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8bef6d1-70d6-45d3-b7b4-5e9234156d63",
            "compositeImage": {
                "id": "8558a960-f582-4f97-bf42-3a5582c05e7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0d0d11b-9e24-4126-8703-836b65eb922c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "861689a8-f863-46fc-a8e5-0eafeb983fe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d0d11b-9e24-4126-8703-836b65eb922c",
                    "LayerId": "9bf5ac8f-0dda-43bf-a8d4-fa39df0bb9ce"
                }
            ]
        },
        {
            "id": "1bbe15d9-637e-46e7-852f-27d0a333ae77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8bef6d1-70d6-45d3-b7b4-5e9234156d63",
            "compositeImage": {
                "id": "51c8b238-fc04-4819-9d13-657da2a84005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bbe15d9-637e-46e7-852f-27d0a333ae77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed7bc273-490c-44e8-98a2-b6b20a35496f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bbe15d9-637e-46e7-852f-27d0a333ae77",
                    "LayerId": "9bf5ac8f-0dda-43bf-a8d4-fa39df0bb9ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "9bf5ac8f-0dda-43bf-a8d4-fa39df0bb9ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8bef6d1-70d6-45d3-b7b4-5e9234156d63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 5
}