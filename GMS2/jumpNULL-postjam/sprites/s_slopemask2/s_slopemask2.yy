{
    "id": "42dce8db-ad34-488e-9d64-aa3a29a0f8e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_slopemask2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8776e48-29a3-44b1-878e-849ba5c3761a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42dce8db-ad34-488e-9d64-aa3a29a0f8e1",
            "compositeImage": {
                "id": "2d21700b-74f0-4ce7-8de4-f59e6941cd6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8776e48-29a3-44b1-878e-849ba5c3761a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "813aa1a1-f7ab-4175-a0fb-b830b7873c13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8776e48-29a3-44b1-878e-849ba5c3761a",
                    "LayerId": "20964e82-db47-492d-beb1-6b9d4d97efb9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "20964e82-db47-492d-beb1-6b9d4d97efb9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42dce8db-ad34-488e-9d64-aa3a29a0f8e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}