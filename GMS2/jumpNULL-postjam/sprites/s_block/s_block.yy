{
    "id": "4f0fa2d9-5ed0-4554-87bb-8bad0fa51279",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afe30665-9aeb-4a22-8c2d-8256faa9eda5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f0fa2d9-5ed0-4554-87bb-8bad0fa51279",
            "compositeImage": {
                "id": "3099ed18-5c5d-430e-9676-ce0eb070a1a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afe30665-9aeb-4a22-8c2d-8256faa9eda5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aed5bff0-fb41-4629-8af8-899d447cab1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afe30665-9aeb-4a22-8c2d-8256faa9eda5",
                    "LayerId": "176bacc0-c2f9-4c81-9b20-113a72bc7e3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "176bacc0-c2f9-4c81-9b20-113a72bc7e3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f0fa2d9-5ed0-4554-87bb-8bad0fa51279",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}