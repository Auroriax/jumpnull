{
    "id": "167d0b07-b933-4c6e-aca1-d894d0ac0894",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_spacefg",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 143,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 92,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4778ef94-fd25-4748-a23f-3d2b9983255d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "167d0b07-b933-4c6e-aca1-d894d0ac0894",
            "compositeImage": {
                "id": "98bf3651-cb5a-4644-af44-11d273af9e91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4778ef94-fd25-4748-a23f-3d2b9983255d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c85b613-478f-4864-b64e-7ba0b880651d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4778ef94-fd25-4748-a23f-3d2b9983255d",
                    "LayerId": "a355f9fa-9c95-4d7b-96f9-a42eafde33e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "a355f9fa-9c95-4d7b-96f9-a42eafde33e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "167d0b07-b933-4c6e-aca1-d894d0ac0894",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}