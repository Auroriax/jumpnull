{
    "id": "bcd8a1fe-8481-4662-9eb4-db8a52b51d9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 237,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1adfe313-727c-4dcc-b48d-5ec9f5ebfc4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcd8a1fe-8481-4662-9eb4-db8a52b51d9a",
            "compositeImage": {
                "id": "3425976e-d074-4bca-b1cd-e6d1666e1f8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1adfe313-727c-4dcc-b48d-5ec9f5ebfc4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dd73324-c617-48ae-8b79-db7f437fc9f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1adfe313-727c-4dcc-b48d-5ec9f5ebfc4d",
                    "LayerId": "81f6ded6-f922-4b2b-99d9-d9f51a54f832"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "81f6ded6-f922-4b2b-99d9-d9f51a54f832",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcd8a1fe-8481-4662-9eb4-db8a52b51d9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 238,
    "xorig": -3,
    "yorig": 60
}