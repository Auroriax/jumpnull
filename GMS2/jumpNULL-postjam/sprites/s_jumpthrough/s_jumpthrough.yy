{
    "id": "ff5b9a51-8591-4a26-83e4-5dcc912b61e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_jumpthrough",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b203da19-eef6-442d-9264-52371ce5c1f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff5b9a51-8591-4a26-83e4-5dcc912b61e6",
            "compositeImage": {
                "id": "bcbd5681-3beb-41e5-8cfe-29828cc6499f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b203da19-eef6-442d-9264-52371ce5c1f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40e5537a-6cd7-444a-8ec9-43bc3ed8acd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b203da19-eef6-442d-9264-52371ce5c1f5",
                    "LayerId": "a6554163-fc8f-487a-b89b-50afd037bddc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "a6554163-fc8f-487a-b89b-50afd037bddc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff5b9a51-8591-4a26-83e4-5dcc912b61e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}