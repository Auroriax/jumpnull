{
    "id": "5ea54253-35e7-4435-9a44-899afc69eb92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_intro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e3ca89e-1e83-48b5-96f7-2b2dd98d9829",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ea54253-35e7-4435-9a44-899afc69eb92",
            "compositeImage": {
                "id": "b76fb8c2-e13b-43e8-9545-1fb2642f41c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e3ca89e-1e83-48b5-96f7-2b2dd98d9829",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60501b8a-d6cb-4c60-9efb-3ccae754d581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e3ca89e-1e83-48b5-96f7-2b2dd98d9829",
                    "LayerId": "aa05c7ac-cd83-40a0-bf99-9ae565aede59"
                }
            ]
        },
        {
            "id": "a5753d87-0a90-445d-b2b7-a4fc0cccaa94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ea54253-35e7-4435-9a44-899afc69eb92",
            "compositeImage": {
                "id": "56f007d9-0902-490b-8760-5db406434630",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5753d87-0a90-445d-b2b7-a4fc0cccaa94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e022416-5bd6-46f3-9703-a9f4352cd1d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5753d87-0a90-445d-b2b7-a4fc0cccaa94",
                    "LayerId": "aa05c7ac-cd83-40a0-bf99-9ae565aede59"
                }
            ]
        },
        {
            "id": "14d8a1ed-8023-4a3e-a2fd-bdeff7fff070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ea54253-35e7-4435-9a44-899afc69eb92",
            "compositeImage": {
                "id": "bf07652b-529b-4c21-a5c6-0972f89cf6ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14d8a1ed-8023-4a3e-a2fd-bdeff7fff070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2bcfe7b-33ef-4992-b766-39738f2eb5ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14d8a1ed-8023-4a3e-a2fd-bdeff7fff070",
                    "LayerId": "aa05c7ac-cd83-40a0-bf99-9ae565aede59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "aa05c7ac-cd83-40a0-bf99-9ae565aede59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ea54253-35e7-4435-9a44-899afc69eb92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}