{
    "id": "008ae45a-f8fb-4a4f-ac4f-241261ce37d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_slopemask3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f9df36b-e12a-4d00-9eec-7125717b91dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008ae45a-f8fb-4a4f-ac4f-241261ce37d0",
            "compositeImage": {
                "id": "9abb48e2-e7f2-4821-b0ef-41b3695397a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f9df36b-e12a-4d00-9eec-7125717b91dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1740003c-2e03-43eb-a187-de3010cdb448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f9df36b-e12a-4d00-9eec-7125717b91dc",
                    "LayerId": "4a7203d6-c99b-4778-8bd2-88b6b3fb0d10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "4a7203d6-c99b-4778-8bd2-88b6b3fb0d10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "008ae45a-f8fb-4a4f-ac4f-241261ce37d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}