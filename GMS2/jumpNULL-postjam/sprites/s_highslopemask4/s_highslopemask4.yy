{
    "id": "857d01e5-c10c-40c0-8b15-a00297d62cac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_highslopemask4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 4,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3541699-0a31-4244-a817-91ced4a50cf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "857d01e5-c10c-40c0-8b15-a00297d62cac",
            "compositeImage": {
                "id": "b9373d5f-0bff-4be0-9ed6-c40ae31a96fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3541699-0a31-4244-a817-91ced4a50cf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed4ef882-aff5-4ebe-bec4-1a3697360c65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3541699-0a31-4244-a817-91ced4a50cf3",
                    "LayerId": "47009e15-0781-4a7c-a7ed-e30037cfd468"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "47009e15-0781-4a7c-a7ed-e30037cfd468",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "857d01e5-c10c-40c0-8b15-a00297d62cac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}