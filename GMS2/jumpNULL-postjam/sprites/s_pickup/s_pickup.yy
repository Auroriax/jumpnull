{
    "id": "2472abb4-1244-46b8-9b15-9e784b0aa241",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pickup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef977c4b-b44b-4980-b08a-115d3e93be12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "compositeImage": {
                "id": "a37c7e38-0739-431f-a7cb-a87f81cbaa48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef977c4b-b44b-4980-b08a-115d3e93be12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48e6dd87-9407-4317-b2d5-4847d33251b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef977c4b-b44b-4980-b08a-115d3e93be12",
                    "LayerId": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e"
                }
            ]
        },
        {
            "id": "522764b7-b888-464d-be71-1c378f9a4ff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "compositeImage": {
                "id": "a2cfab6b-1984-4897-9d8c-c44686313075",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "522764b7-b888-464d-be71-1c378f9a4ff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e1105fe-0494-444a-ab8d-57c5e893ec33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "522764b7-b888-464d-be71-1c378f9a4ff6",
                    "LayerId": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e"
                }
            ]
        },
        {
            "id": "a0dc025e-1c4e-446b-ae31-4082c9c02386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "compositeImage": {
                "id": "f522c6de-444a-469b-be1e-1ee2eac97b9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0dc025e-1c4e-446b-ae31-4082c9c02386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8de0b0be-df8d-4055-80e7-7561d19a665c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0dc025e-1c4e-446b-ae31-4082c9c02386",
                    "LayerId": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e"
                }
            ]
        },
        {
            "id": "756834a7-2ce0-4f1b-942a-eb37d8f0b75a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "compositeImage": {
                "id": "4575329f-906e-44d2-ae70-99bf4ebfe55e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "756834a7-2ce0-4f1b-942a-eb37d8f0b75a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50c5faf3-001f-4d43-87ea-8d34cdb4023a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "756834a7-2ce0-4f1b-942a-eb37d8f0b75a",
                    "LayerId": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e"
                }
            ]
        },
        {
            "id": "05a080a0-c233-41ee-b952-97c4d604aa18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "compositeImage": {
                "id": "d4a0390e-f1c6-4a5e-b606-612c835d601d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05a080a0-c233-41ee-b952-97c4d604aa18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "343cda01-6cd5-4bed-b526-8865a9c0c49d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05a080a0-c233-41ee-b952-97c4d604aa18",
                    "LayerId": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e"
                }
            ]
        },
        {
            "id": "ff78b840-f3f8-4114-9d05-c20ccca5027d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "compositeImage": {
                "id": "f4440969-c1e5-496b-90fb-e975f0d9b691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff78b840-f3f8-4114-9d05-c20ccca5027d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94c970e9-6a11-4f60-8106-bd915cfed3e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff78b840-f3f8-4114-9d05-c20ccca5027d",
                    "LayerId": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e"
                }
            ]
        },
        {
            "id": "a07f120e-5eaf-4e36-aef9-c6bc1d22ca7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "compositeImage": {
                "id": "b264ddd4-af72-4b63-b047-06121a74276c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a07f120e-5eaf-4e36-aef9-c6bc1d22ca7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "365375b7-6804-4d1b-92f8-e74abecb2481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a07f120e-5eaf-4e36-aef9-c6bc1d22ca7b",
                    "LayerId": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e"
                }
            ]
        },
        {
            "id": "6c9fc832-7ab0-41f6-b28f-5a47a3754133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "compositeImage": {
                "id": "d8d2b39b-46cb-46cf-9217-a9557eac41d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c9fc832-7ab0-41f6-b28f-5a47a3754133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abed9c8b-cffa-4be8-9868-f7bdb6f507fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c9fc832-7ab0-41f6-b28f-5a47a3754133",
                    "LayerId": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e"
                }
            ]
        },
        {
            "id": "d08b476b-0fac-46e5-ac49-233c46b173fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "compositeImage": {
                "id": "369aad35-26bb-48ea-a081-74fe7992af27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d08b476b-0fac-46e5-ac49-233c46b173fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4980f3f5-6644-4b1d-9760-85a05bf17335",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d08b476b-0fac-46e5-ac49-233c46b173fe",
                    "LayerId": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e"
                }
            ]
        },
        {
            "id": "99f15e50-123c-49e9-8233-996fe7b0cbe2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "compositeImage": {
                "id": "89bf69c6-31d9-43c1-a953-c4d08bc94009",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f15e50-123c-49e9-8233-996fe7b0cbe2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e39311f-bba6-4229-8f83-ad94d14b0ad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f15e50-123c-49e9-8233-996fe7b0cbe2",
                    "LayerId": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e"
                }
            ]
        },
        {
            "id": "e01eacb9-85aa-490c-abd4-bc8f5377e009",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "compositeImage": {
                "id": "171caf12-1edc-44cf-937a-8876ced63097",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e01eacb9-85aa-490c-abd4-bc8f5377e009",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82b9b2d4-1d5d-4222-b33f-d0a4cb982c23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e01eacb9-85aa-490c-abd4-bc8f5377e009",
                    "LayerId": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e"
                }
            ]
        },
        {
            "id": "87d27e11-7df2-4c4a-b397-e473e387c157",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "compositeImage": {
                "id": "95598a4b-05c8-4567-a816-0fbd8d1e1a5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87d27e11-7df2-4c4a-b397-e473e387c157",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64798586-5d37-468c-954b-71a867a74e85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87d27e11-7df2-4c4a-b397-e473e387c157",
                    "LayerId": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e"
                }
            ]
        },
        {
            "id": "b5e9f579-2c5a-4bf7-bd7b-9c571c9721de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "compositeImage": {
                "id": "f43b7ccf-e7b2-4710-9db2-741ab6c2f8bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e9f579-2c5a-4bf7-bd7b-9c571c9721de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "343d46f5-904d-483e-9264-fc171ed364c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e9f579-2c5a-4bf7-bd7b-9c571c9721de",
                    "LayerId": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "062cdf3b-88eb-4ef6-b048-446aeb3bf02e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2472abb4-1244-46b8-9b15-9e784b0aa241",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}