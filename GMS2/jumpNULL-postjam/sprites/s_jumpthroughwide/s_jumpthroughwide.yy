{
    "id": "7a3c3db0-b8ac-4248-ba52-fc385e1a6ff0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_jumpthroughwide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86908d06-980d-4730-af09-296376fabf87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a3c3db0-b8ac-4248-ba52-fc385e1a6ff0",
            "compositeImage": {
                "id": "6802f1ba-064c-4deb-a704-a6f2e5434ec1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86908d06-980d-4730-af09-296376fabf87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48049f48-43cd-40f2-8275-681d19b0f80d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86908d06-980d-4730-af09-296376fabf87",
                    "LayerId": "3a1d3aba-22b4-4d3f-921d-6e4a2530c6ac"
                }
            ]
        },
        {
            "id": "376922b9-a9f3-40bd-a470-99e8a4f5e7a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a3c3db0-b8ac-4248-ba52-fc385e1a6ff0",
            "compositeImage": {
                "id": "df9e0612-97b8-4c09-b066-dc4044a458e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "376922b9-a9f3-40bd-a470-99e8a4f5e7a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b316c86-884d-4431-9982-366d7868b776",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "376922b9-a9f3-40bd-a470-99e8a4f5e7a5",
                    "LayerId": "3a1d3aba-22b4-4d3f-921d-6e4a2530c6ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "3a1d3aba-22b4-4d3f-921d-6e4a2530c6ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a3c3db0-b8ac-4248-ba52-fc385e1a6ff0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}