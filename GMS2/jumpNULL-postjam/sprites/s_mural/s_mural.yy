{
    "id": "52b183b2-1c14-42bb-b9b5-e6ee89014bd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_mural",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05204e76-15a4-4cdb-9386-5e00b72873f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52b183b2-1c14-42bb-b9b5-e6ee89014bd9",
            "compositeImage": {
                "id": "ca08fdd0-7683-423e-9c48-34589a1ee8bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05204e76-15a4-4cdb-9386-5e00b72873f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81b27870-0446-48da-b4f2-774196c07ed4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05204e76-15a4-4cdb-9386-5e00b72873f1",
                    "LayerId": "2630d229-6d59-48f5-92ba-512e6435e8bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2630d229-6d59-48f5-92ba-512e6435e8bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52b183b2-1c14-42bb-b9b5-e6ee89014bd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}