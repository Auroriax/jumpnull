{
    "id": "86105852-8ac0-494e-9ac0-e73d5189d756",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bannertrigger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8902cf0-b32e-495a-8b0b-feb3124b9f2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86105852-8ac0-494e-9ac0-e73d5189d756",
            "compositeImage": {
                "id": "4e11029b-ced0-475f-9598-842e54e6dcf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8902cf0-b32e-495a-8b0b-feb3124b9f2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dc5645a-d50f-465d-9660-7909ef4c2073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8902cf0-b32e-495a-8b0b-feb3124b9f2b",
                    "LayerId": "ac85c772-b298-4c9e-98f0-37a2c6710f0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ac85c772-b298-4c9e-98f0-37a2c6710f0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86105852-8ac0-494e-9ac0-e73d5189d756",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}