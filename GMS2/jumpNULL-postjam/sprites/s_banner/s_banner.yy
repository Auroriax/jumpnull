{
    "id": "7b82136c-45e4-4d3c-9201-749212772e9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_banner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 2,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e823dece-d979-4e48-af6e-29177532fff5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b82136c-45e4-4d3c-9201-749212772e9f",
            "compositeImage": {
                "id": "7b855296-b7c7-4617-8da2-c2c83718f3d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e823dece-d979-4e48-af6e-29177532fff5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc6f5bd-fd39-4bde-8f0e-aabafd3f08b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e823dece-d979-4e48-af6e-29177532fff5",
                    "LayerId": "10763505-18f3-48a5-ade0-8772f54dfd01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "10763505-18f3-48a5-ade0-8772f54dfd01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b82136c-45e4-4d3c-9201-749212772e9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}