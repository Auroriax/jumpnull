{
    "id": "73e26c9a-e0fd-438b-9c8a-6b61d57469e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_highslope",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 4,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "136b635b-87c1-4d09-aaf8-66bb3e046b93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73e26c9a-e0fd-438b-9c8a-6b61d57469e1",
            "compositeImage": {
                "id": "8e503313-4807-4af0-846b-4a6318ab98f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "136b635b-87c1-4d09-aaf8-66bb3e046b93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74e2c17b-630a-4093-878e-d05e0467f361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "136b635b-87c1-4d09-aaf8-66bb3e046b93",
                    "LayerId": "eeec0713-513c-442c-a31f-6675bb4f0190"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "eeec0713-513c-442c-a31f-6675bb4f0190",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73e26c9a-e0fd-438b-9c8a-6b61d57469e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}