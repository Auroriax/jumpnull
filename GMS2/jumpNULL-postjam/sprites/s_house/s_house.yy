{
    "id": "4f29b622-c8d4-4e43-898d-973a11e96f7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_house",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b965677b-1a7f-44fb-97e8-99114007ba3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f29b622-c8d4-4e43-898d-973a11e96f7d",
            "compositeImage": {
                "id": "ad6fc3b9-5603-486c-a767-491dbb60c737",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b965677b-1a7f-44fb-97e8-99114007ba3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45358780-71a6-421e-8480-9611f7587c6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b965677b-1a7f-44fb-97e8-99114007ba3b",
                    "LayerId": "5879bc23-6618-44ff-a911-710adffeaada"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "5879bc23-6618-44ff-a911-710adffeaada",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f29b622-c8d4-4e43-898d-973a11e96f7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}