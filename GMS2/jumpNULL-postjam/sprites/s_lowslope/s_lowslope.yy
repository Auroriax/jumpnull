{
    "id": "43d0395c-048b-4003-8a48-79d747a5d77a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_lowslope",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76df8692-b523-4228-9684-fba72600d500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43d0395c-048b-4003-8a48-79d747a5d77a",
            "compositeImage": {
                "id": "6f80679f-36ac-4345-bc5c-67b36edeccf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76df8692-b523-4228-9684-fba72600d500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "999203db-1553-4bd6-9940-3c95bde2e9a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76df8692-b523-4228-9684-fba72600d500",
                    "LayerId": "2fa0d17a-fb87-4dc4-81cf-31694b57b295"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "2fa0d17a-fb87-4dc4-81cf-31694b57b295",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43d0395c-048b-4003-8a48-79d747a5d77a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}