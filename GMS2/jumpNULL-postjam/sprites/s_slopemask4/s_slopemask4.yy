{
    "id": "334a592b-f366-4e3c-b4ac-22b403c431b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_slopemask4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "685114b2-1ae1-4b6b-9636-4ab902c475da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "334a592b-f366-4e3c-b4ac-22b403c431b2",
            "compositeImage": {
                "id": "7e2218d1-9752-4aae-bfac-2c474754ff2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "685114b2-1ae1-4b6b-9636-4ab902c475da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9acc7052-0e20-4b41-b967-efca0287676b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "685114b2-1ae1-4b6b-9636-4ab902c475da",
                    "LayerId": "7d504e5e-33d2-4fe8-871d-742225237889"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "7d504e5e-33d2-4fe8-871d-742225237889",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "334a592b-f366-4e3c-b4ac-22b403c431b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}