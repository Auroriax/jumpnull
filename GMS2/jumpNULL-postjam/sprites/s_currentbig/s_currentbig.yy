{
    "id": "c5cc02fb-6b53-4a13-bf91-da2d76c67ee9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_currentbig",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd35ea49-015c-4032-b537-6cf901dcc8d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5cc02fb-6b53-4a13-bf91-da2d76c67ee9",
            "compositeImage": {
                "id": "76252d6f-eebd-4c81-b164-17fbe90cee15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd35ea49-015c-4032-b537-6cf901dcc8d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9736c659-6d4d-45c9-836e-be9e87e188c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd35ea49-015c-4032-b537-6cf901dcc8d9",
                    "LayerId": "7eb4a52a-7170-4da9-826d-6928a5ca2a0a"
                }
            ]
        },
        {
            "id": "bbfb7a86-7973-45c5-96e8-e801f6699d65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5cc02fb-6b53-4a13-bf91-da2d76c67ee9",
            "compositeImage": {
                "id": "a0465ba9-422a-4d69-ac9f-7d162fea0ebd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbfb7a86-7973-45c5-96e8-e801f6699d65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "545cb645-89ed-4081-8812-34f21b2ddfe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbfb7a86-7973-45c5-96e8-e801f6699d65",
                    "LayerId": "7eb4a52a-7170-4da9-826d-6928a5ca2a0a"
                }
            ]
        },
        {
            "id": "94d8813e-8f78-42a4-837e-c091859a82a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5cc02fb-6b53-4a13-bf91-da2d76c67ee9",
            "compositeImage": {
                "id": "863b0822-75ce-4db5-b774-4beaf1eaaf9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94d8813e-8f78-42a4-837e-c091859a82a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9735e736-c13d-4453-9404-35bd7a2492cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94d8813e-8f78-42a4-837e-c091859a82a3",
                    "LayerId": "7eb4a52a-7170-4da9-826d-6928a5ca2a0a"
                }
            ]
        },
        {
            "id": "7537c048-c285-4d18-a50f-d86dc6f6e1e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5cc02fb-6b53-4a13-bf91-da2d76c67ee9",
            "compositeImage": {
                "id": "98cfcd11-09b3-46c4-867e-521269fb7e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7537c048-c285-4d18-a50f-d86dc6f6e1e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc29c9eb-1d42-4507-b05d-24643216dc65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7537c048-c285-4d18-a50f-d86dc6f6e1e3",
                    "LayerId": "7eb4a52a-7170-4da9-826d-6928a5ca2a0a"
                }
            ]
        },
        {
            "id": "95ae7009-4073-47bc-9910-567f1eb20a2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5cc02fb-6b53-4a13-bf91-da2d76c67ee9",
            "compositeImage": {
                "id": "84d21dc1-da20-4214-97b3-0e7ec30b6113",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95ae7009-4073-47bc-9910-567f1eb20a2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57299f34-3305-4760-8aec-e42749ce76c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95ae7009-4073-47bc-9910-567f1eb20a2a",
                    "LayerId": "7eb4a52a-7170-4da9-826d-6928a5ca2a0a"
                }
            ]
        },
        {
            "id": "66d684b9-efe7-483b-a5fe-414ac7f0f6c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5cc02fb-6b53-4a13-bf91-da2d76c67ee9",
            "compositeImage": {
                "id": "a28ff1f6-6437-4d46-bd2a-39bfbd9f3ab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66d684b9-efe7-483b-a5fe-414ac7f0f6c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "698d4c3a-7368-4866-bfd7-7df246dec15a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66d684b9-efe7-483b-a5fe-414ac7f0f6c5",
                    "LayerId": "7eb4a52a-7170-4da9-826d-6928a5ca2a0a"
                }
            ]
        },
        {
            "id": "703060f9-237a-411f-93ec-418e409e2c5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5cc02fb-6b53-4a13-bf91-da2d76c67ee9",
            "compositeImage": {
                "id": "5b0b5318-e1bc-435c-acc0-791aeeff5d5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "703060f9-237a-411f-93ec-418e409e2c5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6515eee9-b168-44c8-9c83-feaebc1ad6e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "703060f9-237a-411f-93ec-418e409e2c5e",
                    "LayerId": "7eb4a52a-7170-4da9-826d-6928a5ca2a0a"
                }
            ]
        },
        {
            "id": "e3600eab-85ed-43e5-bae3-4825bc3eacf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5cc02fb-6b53-4a13-bf91-da2d76c67ee9",
            "compositeImage": {
                "id": "e6f979b5-feba-44a7-971f-fb67c0e2cc53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3600eab-85ed-43e5-bae3-4825bc3eacf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7aa4ccef-1207-44d6-86cd-58b8c433a4e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3600eab-85ed-43e5-bae3-4825bc3eacf1",
                    "LayerId": "7eb4a52a-7170-4da9-826d-6928a5ca2a0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7eb4a52a-7170-4da9-826d-6928a5ca2a0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5cc02fb-6b53-4a13-bf91-da2d76c67ee9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 12,
    "yorig": 8
}