{
    "id": "8291771e-186a-4aad-b62a-8c944a1b33ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_highslope4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 4,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13f88af2-0637-4de2-84a4-e48874e2c5ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8291771e-186a-4aad-b62a-8c944a1b33ea",
            "compositeImage": {
                "id": "398e9901-6589-4ecf-9ccd-2e14bc03a571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13f88af2-0637-4de2-84a4-e48874e2c5ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9394f573-1f45-4aa2-95b4-924c6fca6724",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13f88af2-0637-4de2-84a4-e48874e2c5ca",
                    "LayerId": "bb05f8bb-9805-456b-96b7-393362c40ab3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "bb05f8bb-9805-456b-96b7-393362c40ab3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8291771e-186a-4aad-b62a-8c944a1b33ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}