{
    "id": "3f39a6ed-bc6d-4687-a5b2-311738eaab90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_mapoverworld",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 583,
    "bbox_left": 0,
    "bbox_right": 1127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d248302a-75f7-4bb9-a925-1602e77d9ff0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f39a6ed-bc6d-4687-a5b2-311738eaab90",
            "compositeImage": {
                "id": "254e4499-7ea0-42c1-b970-bd6f2279c46d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d248302a-75f7-4bb9-a925-1602e77d9ff0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a850a4d-6e28-432f-8d62-d543d26f40fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d248302a-75f7-4bb9-a925-1602e77d9ff0",
                    "LayerId": "832ec67a-f9ca-489c-a002-862dc5656b7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 584,
    "layers": [
        {
            "id": "832ec67a-f9ca-489c-a002-862dc5656b7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f39a6ed-bc6d-4687-a5b2-311738eaab90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1128,
    "xorig": 0,
    "yorig": 0
}