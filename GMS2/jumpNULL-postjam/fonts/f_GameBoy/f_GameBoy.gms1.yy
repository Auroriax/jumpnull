{
    "id": "69d03265-b3f5-486f-bd60-145bfb655a89",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_GameBoy",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Early GameBoy",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b30be9e1-0bc4-4243-acb1-f67580b93661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 63,
                "y": 55
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "44e61185-d334-4888-82fd-0d72b5e6e220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 8,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 24,
                "y": 67
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bd6d73e5-bbb4-467c-a19c-e9b99056971b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 4,
                "offset": 0,
                "shift": 8,
                "w": 5,
                "x": 29,
                "y": 67
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "54e65b25-95e5-44f3-b503-8bafe60553e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 4,
                "offset": 0,
                "shift": 8,
                "w": 5,
                "x": 50,
                "y": 67
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e0a26412-5ea1-49f2-83af-b54dc93a7d2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 4,
                "offset": 0,
                "shift": 8,
                "w": 5,
                "x": 43,
                "y": 67
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b1f5f733-8641-4600-ac98-b32eba7e97d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 4,
                "offset": 0,
                "shift": 8,
                "w": 5,
                "x": 57,
                "y": 67
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6e29e07d-d7cd-485e-abd4-9caec846b8fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 4,
                "offset": 0,
                "shift": 8,
                "w": 5,
                "x": 36,
                "y": 67
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3f2cb246-f26c-4ade-a3ee-7a255fb7aebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 4,
                "offset": 2,
                "shift": 8,
                "w": 2,
                "x": 84,
                "y": 67
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "41ca09e9-505c-43f0-9db8-113ca7bf31eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 9,
                "offset": -1,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 13
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b364cbf5-cfe1-4c1a-a898-8454e7e1509c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 5,
                "x": 49,
                "y": 55
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "11438a0a-935f-47bc-9b44-df5350fac2c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 5,
                "x": 69,
                "y": 55
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "bee72f35-c589-4ff5-ba96-81d35ba7c675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 5,
                "x": 56,
                "y": 55
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b2615a8a-b0e5-4b9c-ac65-61f1baff507a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 9,
                "offset": 1,
                "shift": 8,
                "w": 2,
                "x": 68,
                "y": 67
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "49c371b8-5aca-41ab-9d5b-775f9f82fb25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 6,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 18,
                "y": 67
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8205073d-544a-4738-b260-b72704f861e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 2,
                "x": 76,
                "y": 67
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "47ab51aa-f397-45c7-8edf-a9efd7991f59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 2,
                "x": 80,
                "y": 67
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "425ee98a-cc83-4ecf-9b49-53edf44cf704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 35
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "13ea65df-73f0-45a1-bc87-584ce4cbad82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 100,
                "y": 55
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8b4f107d-7198-4e4a-95e1-93f2c6882a5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 35
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0b0c8648-d52a-4d04-b691-66e9c2b7a987",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 25
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c37fa266-327e-4e00-9c06-d902d9fbf48c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "695268fe-f1cb-4df0-a9ff-87fad411ec6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 35
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b3d54e3c-cc2b-4465-b8ab-625c136ce9a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 25
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "87ef2bad-700d-46e0-9c30-68b1034eeb7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7370f475-b9d8-4826-b77e-9edab1c5f488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 45
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1e7114bf-b46d-401a-a299-be3cb93882e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 35
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6befa8e0-511d-4a04-a562-a91bd4e23e04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 2,
                "x": 72,
                "y": 67
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "1ecb8446-fba4-42d5-9e51-99d154f68fe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 9,
                "offset": 1,
                "shift": 8,
                "w": 2,
                "x": 64,
                "y": 67
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "7733579a-dc6d-4e92-aaa0-44ac44c063a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 10,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 13
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9b5a48d3-7f1a-4e79-a4f9-17e2d2315371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 10,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 13
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8fceed4e-1964-4f0b-a96a-b487b81b24f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 42,
                "y": 55
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2fcafb14-cf1f-4435-ad55-aa780277d8a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 9,
                "offset": -1,
                "shift": 8,
                "w": 6,
                "x": 37,
                "y": 13
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0f3d574d-caaf-4bef-8df2-9309e7f1c034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 9,
                "offset": -1,
                "shift": 8,
                "w": 6,
                "x": 29,
                "y": 13
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6cf08645-90e8-497b-8f90-771142a71631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 25
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "16ce0154-db23-47a3-8949-553b6045a1d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 25
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "276b3514-e8b4-4cfa-b6cc-6a020fc38e3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 25
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "923ff0ff-ea37-43c5-96b0-7050fdea5248",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 45
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "20d936c0-0d16-43d6-8d00-3f941c3fcf53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 55
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ecbd8951-110b-4191-903f-8133a609af13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 45
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f591da0f-66af-41cf-a366-93fddc6ec88b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 25
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d9725136-a1e1-422e-aa41-bc5081abc61e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 35
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "886f3346-43ac-4fb5-a23a-e189cd37487a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 82,
                "y": 55
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "079dc170-99bc-480e-9589-13b7b43ac9e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 25
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "44a77735-daab-4602-81f0-0f735c10453f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 35
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f8d6f5e8-d841-4b3c-b531-3ce30e159298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 45
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1db3d8da-5c18-4333-a930-c1fc4f027fc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 45
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a980f22d-85cd-49cf-bb86-f82875149d1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 45
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4200d34f-22ce-41b5-9bf3-aa632b510cd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 35
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "76e3301b-e5b8-4d41-b584-a57d78795fbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 35
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b1c605c4-5e35-46f2-a636-6a7a8db59a6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 45
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8d2c5f7e-cc9f-4288-bcb2-cd8b2e9eb804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 35
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b239bfaa-b9c8-44a0-b49b-a2deadc8fe86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 35
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "aca48be7-620e-4768-9d65-efee2041e1b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 35
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "76aeaa38-97d4-4baa-bd84-22e2e867ce50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 45
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "400c7197-5d11-4fed-8047-e397f3158279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 45
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9738b15c-c185-448b-ad78-dbeb3bd54b1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 45
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b43424b8-fcc7-4052-b87b-337da3c2673e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 45,
                "y": 13
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b19297fb-c5a8-4d90-b0ec-f57b13bb7231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 77,
                "y": 13
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "271c3bd8-17c5-4492-92b3-d1afb8f36a76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 45
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "676bbb06-cab3-481b-a409-9c56b4e8dd77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 9,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fc1c6617-9d40-46c2-9e98-f74566cc8465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 9,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e265af3d-65ad-427c-a05d-c7e35a72a974",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 5,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 55
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "076e73f4-cfd4-47c3-bc83-421830a5cc43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 5,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 67
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b83aedbb-7e8b-40d4-a0a0-710ba45d6ea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 5,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 67
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "57371255-0558-4e57-b267-5ea55ff43261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 5,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 55
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "e4363378-53c2-431e-b707-6b8ceb8f6086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 35
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6d6f56ce-21ea-44e3-92d6-1f34c85f0363",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 55
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ec223add-4111-4b77-b626-164953c1939b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 117,
                "y": 13
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7d51a227-f5ba-40e5-9788-639de129ff15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 55
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "454549a3-8489-43c0-bbe9-fdf5a6e3b979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 25
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0ba25301-5d0a-4a8b-94c6-4fc4ad54d03e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 69,
                "y": 13
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "78befadb-e36f-4e98-91e2-c2e4488deed8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 101,
                "y": 13
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "653451e5-a8ee-45f5-bedb-10c86cd045a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 93,
                "y": 13
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c6eb73a8-92f1-4e34-87f5-a85f1da9f7df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 94,
                "y": 55
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3bca6651-9887-4c66-8a91-a69cfe607526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 53,
                "y": 13
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c9c7b766-34f4-4eb7-8f39-46d05959affb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 55
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "62e23c78-83dc-446c-b546-5312d5a6f02d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 45
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "1666d9f1-4156-452b-9792-f1dc811d9dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 61,
                "y": 13
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e7f6350e-b41d-4664-a8cb-a03e133c8a37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8d788907-de7b-41b3-ad9f-dfd437085d8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 85,
                "y": 13
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "87346753-a466-44d7-a7de-ea27a434e098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 109,
                "y": 13
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "fa648660-3c18-4dec-a0b1-4c29d6bc8d88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 45
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9ca5d367-5f99-4d32-92a4-3d1bf254f69c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 45
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f6415aea-1c8c-47bc-bc15-d8bea17d5fae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 45
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "cdb661a5-d0ae-4112-868c-75b34fdd3715",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "cc3ed557-f91e-407f-8606-299099e0477f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 55
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "82a8ae43-35e2-4cb8-a584-220cadc300da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "336b8391-5f44-461a-a273-3c2467a206f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e26eec4c-ef4a-44f4-a890-0edf0560d4ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b752ec33-97a8-4b67-a271-bd95cb5790c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "18ae1845-0ad2-4d2e-9f00-0c81a8f9dd38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 35
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a47c8a3e-3226-40f1-af20-a819996b6dff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 35
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9a0b6a95-7ea8-4467-a866-e6e8163504d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 8,
                "offset": 0,
                "shift": 50,
                "w": 50,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9822f2ed-fdc3-4b57-aaa4-809c4521d090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 8,
                "offset": 0,
                "shift": 50,
                "w": 50,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1bdaa848-993d-47d7-b9e7-483e4473df9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 88,
                "y": 55
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "b3269cd5-3dae-4fac-85c7-80726ca435b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 76,
                "y": 55
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": true,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}