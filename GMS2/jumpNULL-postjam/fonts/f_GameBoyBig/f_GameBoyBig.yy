{
    "id": "e451b590-7ea4-410c-988a-fac6bdc83114",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_GameBoyBig",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Early GameBoy",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "69fe5da7-78d8-4005-82b4-173b4f05ed6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 113,
                "y": 193
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6e162328-7e01-4a91-a447-2590d3cc2a28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 6,
                "shift": 24,
                "w": 9,
                "x": 56,
                "y": 225
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3d133fe2-be75-4e72-9dc4-68dd7da35145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 12,
                "offset": 0,
                "shift": 24,
                "w": 15,
                "x": 67,
                "y": 225
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0217edf9-9d72-4280-99cb-f9319c36dd7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 12,
                "offset": 0,
                "shift": 24,
                "w": 15,
                "x": 118,
                "y": 225
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "eeae439f-2d42-4003-8e6c-126fa1d7296e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 12,
                "offset": 0,
                "shift": 24,
                "w": 15,
                "x": 101,
                "y": 225
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "79bc2bf2-128a-41ab-9418-46fc6f01410a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 12,
                "offset": 0,
                "shift": 24,
                "w": 15,
                "x": 135,
                "y": 225
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "343b0b2b-4930-4c57-9a91-a6230b0241ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 12,
                "offset": 0,
                "shift": 24,
                "w": 15,
                "x": 84,
                "y": 225
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "46c165af-20bf-4f48-a1ac-a1a459eedce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 12,
                "offset": 6,
                "shift": 24,
                "w": 6,
                "x": 192,
                "y": 225
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "610947ee-e0d3-41e0-964b-ee4611121555",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 27,
                "offset": -3,
                "shift": 24,
                "w": 21,
                "x": 2,
                "y": 60
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0c3d3e2c-9505-41ce-9f5f-59c513ce60e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 15,
                "x": 79,
                "y": 193
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4b416f7a-0d16-464a-a9ce-5c01826834d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 15,
                "x": 127,
                "y": 193
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c4f3e7d4-334a-4501-9825-90900dac3148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 15,
                "x": 96,
                "y": 193
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bd457ba2-9970-4acb-a16d-3c11c4157fd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 27,
                "offset": 3,
                "shift": 24,
                "w": 6,
                "x": 160,
                "y": 225
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "01da5e1c-1049-4722-9f04-973b5ab5ffa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 3,
                "shift": 24,
                "w": 12,
                "x": 42,
                "y": 225
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "040258fd-33e4-4689-a3bb-24720baf9075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 6,
                "x": 176,
                "y": 225
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "3e47371b-e9dc-4287-9096-984657635528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 6,
                "x": 184,
                "y": 225
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c6057c9b-c7ed-4600-ba0c-42157d01c2a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 62,
                "y": 141
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5523c128-879f-4dcf-9a21-2849043c8e51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": 3,
                "shift": 24,
                "w": 12,
                "x": 200,
                "y": 193
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3ffee985-4768-488b-bf43-077450e740c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 122,
                "y": 141
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8c75ad7e-b0a4-4c91-845f-b204add1bb6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 122,
                "y": 89
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2ca93c46-f6f4-4359-8038-12bf7a361941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 22,
                "y": 89
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5627ce84-ee98-4a7e-befa-09c3664f50c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 142,
                "y": 115
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5000a311-d120-4acd-969f-a86b9ec5d6f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 142,
                "y": 89
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ca5b1202-1836-46b2-be2c-cae7a3494405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 82,
                "y": 115
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4fbe4887-39c5-4ecd-ba00-3b7af1f8e351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 22,
                "y": 167
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "87406b70-ecd0-4cdc-8031-3050a8fbcda3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 82,
                "y": 141
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2e8e8e68-0855-43cd-9670-4099c7067c93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 3,
                "shift": 24,
                "w": 6,
                "x": 168,
                "y": 225
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ffc26767-cb89-4fc3-9e19-c1cbafb75b1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 27,
                "offset": 3,
                "shift": 24,
                "w": 6,
                "x": 152,
                "y": 225
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "312a5ea7-3b18-4cdf-bb45-2e85e3d5183c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": 0,
                "shift": 27,
                "w": 21,
                "x": 206,
                "y": 28
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "65777912-8246-4ca9-81e4-f8e2360155ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 0,
                "shift": 27,
                "w": 21,
                "x": 229,
                "y": 28
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7961516b-b1d2-47e0-8ff7-59abcb86f5aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 27,
                "offset": 3,
                "shift": 27,
                "w": 15,
                "x": 62,
                "y": 193
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4d21ca64-548c-431e-85cf-14f805e7f7ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 27,
                "offset": -3,
                "shift": 24,
                "w": 18,
                "x": 45,
                "y": 60
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8d85ae32-2db7-4c71-a2f8-78ef82641a2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 27,
                "offset": -3,
                "shift": 24,
                "w": 18,
                "x": 25,
                "y": 60
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "879ad59d-bdfd-4f9b-ab11-929adc19c421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 182,
                "y": 89
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3df2fbac-3d4d-4316-8e6b-bd1ee6d5f34f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 82,
                "y": 89
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9c6d90c7-628d-4448-a6fb-5932bb6a704d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 42,
                "y": 115
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0cfe81ca-1f85-4bd7-b5c4-0daeaf75fccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 102,
                "y": 167
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a1de6ade-5406-4d22-af95-16d878d4f631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 42,
                "y": 193
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9b557ea9-2a4d-4f3a-94f4-18e72905fd32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 162,
                "y": 167
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3bfb9269-4a67-4d90-8b94-7e7121ba7999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 102,
                "y": 89
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "52dc96fe-7809-4071-a188-f45634aca18d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 2,
                "y": 141
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3e07d75a-db44-4442-9a7d-6d32fed3fc97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 3,
                "shift": 24,
                "w": 12,
                "x": 158,
                "y": 193
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a8fc9042-3453-41f5-a5a1-b4d8b01d9190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 22,
                "y": 115
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ee3ee615-b6a5-455b-ab3b-9348489d0fd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 222,
                "y": 115
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3b5ef6fe-899b-44ea-bf4f-b27b4bcf46d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 62,
                "y": 167
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "38ab96f0-de1d-4166-aa17-79b6d394f118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 2,
                "y": 167
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "80e4efd0-33db-4af0-bf7b-24bf54bc811b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 42,
                "y": 167
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f47c9e9f-3b3e-4084-8080-a2f0e47ff694",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 22,
                "y": 141
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "209d3377-eaeb-49b8-b71a-a56e2954d9b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 182,
                "y": 115
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "4f724361-2d1f-42d9-a250-51278225c482",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 142,
                "y": 167
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5a63016e-a3bc-41ab-a29e-c13739a6f57b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 202,
                "y": 115
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e2e11429-83ab-4dcf-9f3b-9fdabc34a923",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 162,
                "y": 115
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a3918da2-ddbd-4787-a9d7-97df7a0c42fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 122,
                "y": 115
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "816f3fc0-f8cc-49f7-ab70-59c81d84ac61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 202,
                "y": 141
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "fedfc00e-e1e9-48e1-93f2-f3c02c1d2761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 82,
                "y": 167
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "cddc58b2-b1da-41c6-ab56-8b36c7baf32f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 182,
                "y": 141
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c11971e8-0c8d-4ade-b613-4d628f40d465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 65,
                "y": 60
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e63886b8-7116-41de-b873-a48caff7f524",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 145,
                "y": 60
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4749ed32-1e52-48da-9613-03af08733ec6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 122,
                "y": 167
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b6afb83a-d3e2-46a1-8893-55f13b05cd8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 27,
                "offset": -3,
                "shift": 24,
                "w": 24,
                "x": 180,
                "y": 28
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "192f3221-7135-4fde-804d-78d2a2eb2a3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 27,
                "offset": -3,
                "shift": 24,
                "w": 24,
                "x": 154,
                "y": 28
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "efe4bce6-ae61-4301-a97d-8967927050ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 234,
                "y": 193
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "be561b7c-0553-4ce3-9921-37578d203392",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 2,
                "y": 225
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "28f51142-7004-4d9d-8e30-e885df913402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 22,
                "y": 225
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ff62a5ae-28e7-484b-842e-1451a49cbdc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 214,
                "y": 193
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d2939ada-e86e-4f6e-9073-b87f6032c5d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 42,
                "y": 141
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "640f6b00-60b9-48aa-b79c-bcb0884a9ef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 222,
                "y": 167
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e28b2fad-8d58-4d3d-bad8-692bcf73975e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 2,
                "y": 89
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4d3e1fb7-b45c-4a24-bb4d-0a4625e11e11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 2,
                "y": 193
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b4a25b08-05b2-45b6-91e0-058cb6e11358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 62,
                "y": 115
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "09dc24f3-05d3-4602-a7c3-67b95dcc4502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 125,
                "y": 60
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fdc46218-e4ae-46d5-a5cf-2d7526e2d434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 205,
                "y": 60
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "2aad9fd2-a750-4611-81ef-5fae3480ccf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 185,
                "y": 60
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b27a7cab-0c43-4f0d-b3b8-1ca7cb803d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 3,
                "shift": 24,
                "w": 12,
                "x": 186,
                "y": 193
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "695eab41-1a57-430d-a502-4b8773a8646c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 85,
                "y": 60
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "53324089-e805-4f2e-9deb-23e63c260456",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 202,
                "y": 167
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "54c5bf9e-1e7f-488e-9e4b-2ca3b8ab125d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 142,
                "y": 141
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7316ecbd-6b51-4aac-98a5-f80aeada9d88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 105,
                "y": 60
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0bfdfa34-8c43-48b7-8417-42e95977490f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 202,
                "y": 89
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "27561b07-7ada-43b0-a267-7be84e52dfa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 165,
                "y": 60
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7d374446-58f0-48d4-a682-751d557187cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 225,
                "y": 60
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1b09bcfb-a48d-4369-b2f0-e64e9f8c474a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 162,
                "y": 141
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d633c8b4-b633-473f-8c44-5782eb8985e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 182,
                "y": 167
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "893807d9-385b-4633-a07b-e45842f36aa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 222,
                "y": 141
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a5cbe502-781b-405c-bd7b-e296b353c9ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 222,
                "y": 89
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f455c40a-c48d-4149-b410-1c61abecb706",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 22,
                "y": 193
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3979bc40-6b3b-4380-b7d9-10a69824e79d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 62,
                "y": 89
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "bf719027-aaa7-490e-8a4a-a65dc53b2249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 162,
                "y": 89
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d3794fe1-bc54-4bcb-8153-1401b306bd5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 42,
                "y": 89
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "76ed637e-5038-4b36-9856-b5f725a140e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 2,
                "y": 115
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "74636fa8-a039-4495-a82a-c0a29a3cfdd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 102,
                "y": 141
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "048c40f5-61be-4538-93fb-0f55c17bf421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 18,
                "x": 102,
                "y": 115
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3637d153-431f-434b-b77d-4ce0f26b6784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 0,
                "shift": 150,
                "w": 150,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "41d1705c-7e25-4a42-8f98-7ecaaceb1657",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 0,
                "shift": 150,
                "w": 150,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3a1e5392-d36f-45b0-8ff9-15f11ec56a2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 12,
                "x": 172,
                "y": 193
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "0cd7bde7-9fda-4265-9163-044a36c9614d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 12,
                "x": 144,
                "y": 193
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": true,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}