{
    "id": "a9188620-a624-4a5a-83ae-a1b53faf038b",
    "modelName": "GMLinuxOptions",
    "mvc": "1.0",
    "name": "Linux",
    "option_linux_allow_fullscreen": true,
    "option_linux_disable_sandbox": false,
    "option_linux_display_cursor": true,
    "option_linux_display_name": "jumpNULL",
    "option_linux_display_splash": false,
    "option_linux_enable_steam": false,
    "option_linux_homepage": "http:\/\/amcookie.weebly.com",
    "option_linux_icon": "${options_dir}\\linux\\icons\\icon64.png",
    "option_linux_interpolate_pixels": false,
    "option_linux_long_desc": "A metroidvania platformer made for the #gbjam . However, in this platformer, you cannot jump. You might think 'what's the fun in platforming, then?' Puzzle solving, exploration and just enjoying a more relaxed interpretation of the metroidvania genre. And you will be offered a pretty interesting choice should you make it to the end of the game...\\u000d\\u000a\\u000d\\u000aIn the Game Boy Jam, the goal was to make a game inspired by the original handheld, and to use it's resolution and a limited color palette. If you would like to experience the world without hand-holding, then you are free to do so. If you need just a little extra push to figure out what to do, you can use the sign posts placed near all interesting locations.",
    "option_linux_maintainer_email": "auroriax@gmail.com",
    "option_linux_resize_window": false,
    "option_linux_scale": 0,
    "option_linux_short_desc": "jumpNULL is a metroidvania where you can't jump.",
    "option_linux_splash_screen": "${options_dir}\\linux\\splash\\splash.png",
    "option_linux_start_fullscreen": false,
    "option_linux_sync": false,
    "option_linux_texture_page": "2048x2048",
    "option_linux_version": {
        "build": 0,
        "major": 1,
        "minor": 3,
        "revision": 0
    }
}