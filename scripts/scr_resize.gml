//Source: http://www.dummies.com/how-to/content/scaling-to-best-fit-in-gamemaker-studio.html

cur_width = max(1, display_get_width());
cur_height = max(1, display_get_height());

var resizex, resizey;

var ratio = cur_width / cur_height;
if cur_width < cur_height
    {
    resizex = min_height * ratio;
    resizey = min_height;
    }
else
    {
    resizex = min_width;
    resizey = min_width / ratio;
    }

surface_resize(application_surface, view_wview[0], view_hview[0]);
