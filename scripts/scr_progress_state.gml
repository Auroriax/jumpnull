///scr_progress_state(stateid [can only increment])

switch argument[0]
{
case 0: //Upgrade 1, slower falling
 o_citicen3.x = 540
 o_citicen3.y = 1136
 o_citicen3.d[0] = "YANA: Oh wow! Where'd you find those boots?"
 o_citicen3.d[1] = "At the altar? I was there yesterday..."
 o_citicen3.d[2] = "How did it pop up there?"
 o_citicen3.d[3] = "Did it came down with the object from space?"
 o_citicen3.d[4] = "You can pass the gap with the boots now! Go on!"
 break;
case 1: //Slope climb upgrade
 o_cameo.x = 716
 o_cameo.y = 668
 o_cameo.d[0] = "Hello! Lovely village you have here."
 o_cameo.d[1] = "I might plan my next big cave adventure in the area."
 o_cameo.d[2] = "You might not think it, but I'm a real treasure hunter."
 o_cameo.d[3] = "Judging on the loot you have, there's plenty to find!"
 o_cameo.d[4] = "/END";
 break;
case 2: //Lana Telepathy
 o_citicen2.x = 612
 o_citicen2.y = 652
 o_citicen2.d[0] = "CARL: Wow! A voice- in your MIND?"
 o_citicen2.d[1] = "That's incredible! What'd it say?"
 o_citicen2.d[2] = "The mountains, huh? That's left from the village!"
 o_citicen2.d[3] = "Just climb that steep hill left over there!"
 o_citicen2.d[4] = "Good luck!";
 break;
}
