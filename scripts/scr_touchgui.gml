if os_type != os_android
exit;

var scr, clr, pressed;
scr = application_get_position();
clr = make_colour_rgb(83,122,62)
devx = display_get_gui_width();
devy = display_get_gui_height();
pressed = false

draw_rectangle_colour(0,0,devx,scr[1],clr,clr,clr,clr,0)
draw_rectangle_colour(0,scr[3],devx,devy,clr,clr,clr,clr,0)

draw_set_color(make_colour_rgb(204,228,152))
if keyboard_check(vk_left) {pressed = 0} else {pressed = 1}
draw_rectangle(0,devy*0.8,devx*0.2,devy,pressed)
if keyboard_check(vk_right) {pressed = 0} else {pressed = 1}
draw_rectangle(devx*0.2,devy*0.8,devx*0.4,devy,pressed)
if keyboard_check(vk_enter) {pressed = 0} else {pressed = 1}
draw_rectangle(devx*0.45,devy*0.8,devx*0.65,devy,pressed)
if keyboard_check(vk_up) {pressed = 0} else {pressed = 1}
draw_rectangle(devx*0.7,devy*0.8,devx,devy*0.9,pressed)
if keyboard_check(vk_down) {pressed = 0} else {pressed = 1}
draw_rectangle(devx*0.7,devy*0.9,devx,devy,pressed)
if keyboard_check(ord('R')) {pressed = 0} else {pressed = 1}
draw_rectangle(scr[2]-72,scr[1]-76,scr[2],scr[1]-4,pressed)

draw_sprite(s_gui,3,devx*0.1,devy*0.95)
draw_sprite(s_gui,1,devx*0.3,devy*0.95)
draw_sprite(s_gui,4,devx*0.55,devy*0.95)
draw_sprite(s_gui,0,devx*0.95,devy*0.85)
draw_sprite(s_gui,2,devx*0.95,devy*0.95)
draw_sprite(s_gui,5,scr[2]-36,scr[1]-38)

draw_sprite(s_logo,0,0,scr[1]-10)

//draw_text_colour(0,scr[1]-10,"LED display with Surround Sound",c_gbdarkgray,c_gbdarkgray,c_gbdarkgray,c_gbdarkgray,1)
