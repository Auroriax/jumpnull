///save_new()

show_debug_message("Save NEW")

file_delete("jumpNULL.save")

ini_open("jumpNULL.save")

ini_write_real("State","SaveX",-1)
ini_write_real("State","SaveY",-1)
ini_write_real("State","UpgradeCount",0)
ini_write_real("State","Hat",0)
ini_write_real("State","SpokeToLena",false)
ini_write_real("State","Switch",false)

for(var i = 0; i != 15; i += 1)
{
 for(var j = 0; j != 15; j += 1)
 {
  ini_write_real("Minimap",string(i)+"_"+string(j),false)
 }
}

ini_close()
